<?php

use Bitrix\Main\Loader;
use Api\Main;
use Api\Tools;
use PHPUnit\Framework\TestCaseBitrix;

final class VendorsClassTest extends TestCaseBitrix
{
    public Api\Vendors $vendor;
    public Api\FieldsMap $mapper;

    protected function setUp(): void
    {
        Loader::includeModule('api');

        $shop_id = 'shop_test';

        $config = Main::$container->get('Api\Config');

        $this->mapper = Main::$container->get('Api\FieldsMap');

        $site_id = $config->getSiteByShopId($shop_id);
        $config->set('IBLOCK_ID', $config->options[$site_id . '__VENDORS_IBLOCK_ID']);

        $this->vendor = Main::$container->make('Api\Vendors', [
            'cfg' => $config
        ]);
    }

    public function testCanCreateVendorElement()
    {

        $vendor_item = [
            "NAME" => $this->randString(),
            "XML_ID" => $this->randGuid(),
        ];

        $res = $this->vendor->create($vendor_item);
        $this->assertTrue($res['status']);
    }

    public function testCantCreateVendorElement()
    {
        $vendor_item = [
            "NAME" => $this->randString(),
            "XML_ID" => $this->faker->randomNumber(),
        ];

        $res = $this->vendor->create($vendor_item);
        $this->assertFalse($res['status']);
    }

    protected function tearDown(): void
    {
        if (isset($this->vendor->iblock->element_id) && is_int($this->vendor->iblock->element_id)) {
            $this->vendor->iblock->deleteById($this->vendor->iblock->element_id);
        }
    }
}