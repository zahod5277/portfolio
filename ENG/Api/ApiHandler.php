<?php

namespace Api {

    use Api\Log;
    use Api\Main;
    use Api\Config;
    use Api\Iblocks;
    use Api\KafkaProducer;

    class ApiHandler
    {
        public Config $config;
        public Iblocks $iblock;
        public ResponseHandler $responseHandler;
        public FieldsMap $mapper;
        public array $last_query;
        public string $last_error;
        public KafkaProducer $producer;
        public Log $logger;
        public ?string $last_api_status;


        public function __construct(Config $cfg)
        {

            $this->config = $cfg;

            $this->mapper = Main::$container->get('\Api\FieldsMap');

            $this->responseHandler = Main::$container->get('\Api\ResponseHandler');

            $this->iblock = Main::$container->make('\Api\Iblocks', [
                'config' => $this->config
            ]);

            $this->producer = Main::$container->make('\Api\KafkaProducer', [
                'config' => [
                    'broker' => 'kafka:9092',
                    'topic' => 'answers'
                ]
            ]);

            $this->logger = Main::$container->make('\Api\Log', [
                'instance' => 'pdo'
            ]);

            $this->last_error = '';

        }


        /**
         * process all input data, search method & execute, return status
         * @param array $data
         * @return bool
         */
        public function process($data): bool
        {
            $this->last_query = $data;
            $method = $data['method'];
            $object = lcfirst($data['collection']);

            if (!method_exists($this, $method)) {
                $message = 'Метод ' . $method . ' не найден';
                Log::$handler->error($message);
                return false;
            }

            $response = [];
            $has_errors = false;

            if (empty($data['info'][$object])) {
                $result = $this->$method();
                return $this->response([], !$result);
            }

            foreach ($data['info'][$object] as $obj) {
                if (!is_array($obj))
                    return false;

                $mapped = $this->mapper->map($obj);
                $result = $this->$method($mapped);
                if ($result['status']) {
                    if (!empty($result['data'])) {
                        $response['info'] = $result['data'];
                    }
                } else {
                    $response['info'][] = [
                        'id' => $obj['id'],
                        'name' => $obj['name'],
                        'error' => $this->last_error,
                    ];
                    $has_errors = true;
                }
            }

            return $this->response($response, $has_errors);
        }

        /**
         * call response handler & send result message to Kafka
         * @param $data - data processing result
         * @param $has_errors
         * @return bool
         */
        public function response($data, $has_errors): bool
        {
            $response = $this->responseHandler->buildResponse($data, $has_errors, $this->last_query);
            $this->last_api_status = $response;
            return $this->producer->produceMessageWithKey(
                $response,
                $this->last_query['requestId']
            );
        }

        protected function success(array $data = null): array
        {
            return [
                'status' => true,
                'data' => $data
            ];
        }

        protected function error(): array
        {
            return [
                'status' => false,
                'data' => $this->last_error
            ];
        }

        protected function setResult(bool $result, array $data = null): array
        {
            return $result ? $this->success($data) : $this->error();
        }

    }
}