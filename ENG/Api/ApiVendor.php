<?php


namespace Api {

    use Bitrix\Main\Entity;
    use Bitrix\Main\Loader;
    use Bitrix\Main\Localization\Loc;
    use Bitrix\Highloadblock as HL;
    use CModule;
    use CIBlockElement;

    use Api\FieldsMap;
    use Api\ApiHandler;
    use Api\ResponseHandler;
    use Api\Config;
    use Api\KafkaProducer;
    use Api\Iblocks;
    use Api\Log;
    use Api\Main;

    class Vendors extends ApiHandler
    {
        /**
         * method GetVendor
         * @param $data
         * @return array
         */
        public function get($data): array
        {
            $response = [];

            $result = $this->iblock->getElements($data);
            if (!empty($result)) {
                foreach ($result as $item) {
                    $response[] = $this->mapper->remap($item);
                }
            } else {
                $response = $result;
            }

            return $this->setResult(true, $response);
        }

        /**
         * create a Vendor element
         * @param $data
         * @return array
         */
        public function create($data): array
        {
            $create_result = $this->iblock->create($data);
            if (!$create_result)
                $this->last_error = $this->iblock->last_error;
            return $this->setResult($create_result);
        }

        /**
         * Update Vendor Method
         * @param $data /a data that must be updated
         * @return array query status
         */
        public function update($data): array
        {
            $XML_ID = $data['XML_ID'];
            $element = $this->iblock->getElement($XML_ID);

            if (!empty($element && $element['ID'])) {
                $res = $this->iblock->updateElement($element['ID'], $data);
            } else {
                $this->last_error = 'Элемент не найден или неправильно задан GUID';
                $res = false;
            }

            return $this->setResult($res);
        }

        /**
         * delete a Vendor element
         * @param $data
         * @return array
         */
        public function delete($data): array
        {
            $del_res = $this->iblock->deleteByGUID($data['XML_ID']);
            return $this->setResult($del_res);
        }

}