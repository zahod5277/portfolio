<div class="row">
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <th>Статус</th>
            <th>Номер</th>
            <th>Дата</th>
            <th>Менеджер</th>
            <th>Получатель</th>
            <th>Доставка</th>
            <th>Сумма</th>
            <th>Действия</th>
            </thead>
            {foreach $orders as $item}
                <tr data-order-id="{$item['order.id']}">
                    <td>
                        <p class="crm-order-table__status">
                            <span class="crm-order-table__status-label"
                                  style="background-color: #{$item['status.color']};"></span>
                            <span class="crm-order-table__status-text">
                            {$item['status.name']}
                        </span>
                        </p>
                    </td>
                    <td>
                        <a data-order-item="{$item['order.id']}" href="#" data-toggle="tooltip" data-placement="top"
                           title="Карточка заказа"><b>{$item['order.num']}</b></a>
                    </td>
                    <td class="crm-order-table__tr--sm">
                        {$item['order.createdon']|date:'d/m/Y h:m'}
                    </td>
                    <td class="form-group">
                        {foreach $mgrs as $mgr}
                            {if $mgr['id'] == $item['manager.id']}
                                {$mgr['fullname']}
                            {/if}
                        {/foreach}
                    </td>
                    <td>
                        {$item['address.receiver']}
                    </td>
                    <td>
                        {$item['delivery.name']}
                    </td>
                    <td>
                        {$item['order.cost']|number : 0 : '.' : ''}₽
                    </td>
                    <td>
                        <button class="btn btn-success btn-sm" data-crm-order-excel
                                data-crm-order-id="{$item['order.id']}" data-toggle="tooltip" data-placement="top"
                                title="Экспорт заказа в EXCEL">
                            <i class="fas fa-file-excel"></i>
                        </button>

                        <button class="btn btn-primary btn-sm" data-order-item="{$item['order.id']}"
                                data-toggle="tooltip" data-placement="top" title="Карточка заказа">
                            <i class="fas fa-tag"></i>
                        </button>
                    </td>
                </tr>
            {/foreach}
        </table>
    </div>
</div>