{var $statuses = $_modx->runSnippet('@FILE:snippets/orderCRM/getStatuses.php')}
{var $manager = 'Не назначен'}
<div class="col-12 order__item" data-selectable="true">
    {foreach $orders as $item}
        {foreach $mgrs as $mgr}
            {if $mgr['id'] == $item['manager.id']}
                {var $manager = $mgr['fullname']}
            {/if}
        {/foreach}
        <h4 class="h4" data-selectable="true" data-order="{$item['order.num']}">Заказ #{$item['order.num']}</h4>
        <div class="mt-5 mb-2">
            <div class="row">
                <div class="col-12 col-md-6 mb-5">
                    <h5 class="alert-info p-2" data-selectable="true">Менеджер: {$manager}</h5>
                </div>
                <div class="col-12 col-md-6 mb-5">
                    <div class="mb-2 p-2 form-group" style="position: relative;top: -7px;">
                        <select data-crm-status class="form-control" data-order-id="{$item['order.id']}" name="status" id="status">
                            {foreach $statuses as $status}
                                {if $item['status.id'] == $status.id}
                                    <option selected="selected" value="{$status.id}">{$status.name}</option>
                                    {else}
                                    <option value="{$status.id}">{$status.name}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </div>
                </div>
            </div>

            {var $address = $item['address.index']~', г.'~$item['address.city']~', ул.'~$item['address.street']~', д.'~$item['address.building']~', кв./оф.'~$item['address.room']}
            {var $discount = $item['order.cost'] - $item['order.cart_cost'] - $item['order.delivery_cost']}
            <div class="table-responsive">
                <table class="table">
                    <tr data-selectable="true">
                        <td>
                            Сумма заказа: <b>
                                {$item['order.cart_cost']|number : 0 : '.' : ' '}
                                <span class="ld-sp-currency">₽</span>
                            </b>
                        </td>
                        <td>Дата: {$item['order.createdon']|date_format:"%d.%m.%Y"}</td>
                    </tr>
                    <tr data-selectable="true">
                        <td>Тип доставки: {$item['delivery.name']}</td>
                        <td>
                            Адрес доставки: {$address}
                        </td>
                    </tr>
                    <tr data-selectable="true">
                        <td>
                            Способ оплаты: {$item['payment.name']}
                            <br>
                            {if $discount > 0}
                                Скидка: {$discount}
                            {/if}
                        </td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <div class="table-responsive">
                <table class="table" data-selectable="true">
                    <tr data-selectable="true">
                        <td>Клиент: {$item['address.receiver']}</td>
                        <td></td>
                    </tr>
                    <tr data-selectable="true">
                        <td>e-mail: <a href="mailto:{$item['user.email']}">{$item['user.email']}</a></td>
                        <td></td>
                    </tr>
                    <tr data-selectable="true">
                        <td>Телефон: <a href="tel:{$item['address.phone']}">{$item['address.phone']}</a></td>
                        <td></td>
                    </tr>
                    {if $item['address.comment'] != ''}
                        <tr data-selectable="true">
                            <td colspan="2">Комментарий к заказу: {$item['address.comment']}</td>
                        </tr>
                    {/if}
                </table>
            </div>
            <h5 class="mt-5 mb-2" data-selectable="true">Состав заказа:</h5>
            <div class="mt-50 table-responsive">
                <table class="table ">
                    <tr data-selectable="true">
                        <td>Товар</td>
                        <td>Артикул</td>
                        <td>Опции</td>
                        <td>Цена</td>
                        <td>Количество</td>
                        <td>Сумма</td>
                    </tr>
                    {$_modx->getChunk('@FILE:chunks/orderCRM/orders.product.row.tpl',[
                        'products' => $products
                    ])}
                </table>
            </div>
        </div>
    {/foreach}
</div>
</div>