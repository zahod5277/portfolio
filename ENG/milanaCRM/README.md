# Micro-CRM on MODX Revo + miniShop2 based

## What this module can do

This module has developed in summer of 2020.

Functions:

* Managers can see order details, customer profile, order summ, discount
* Users roles (manager & administrator).
    - Manager only can see the info
    - Admin can add new managers
    - Admin can fix orders

* Orders/Order exports in Excel with custom period & filter (like customer name or email)


## Backend
Main class is  **[milanaCRM\core\components\milanacrm.class.php](https://bitbucket.org/zahod5277/portfolio/src/89de56f5eb4b5bbef10a2a78d4da9bda43f339a7/ENG/milanaCRM/milanaCRM/core/components/milanacrm/model/milanacrm.class.php)**

All CRM works with AJAX, so key method is **[processAJAX](https://bitbucket.org/zahod5277/portfolio/src/89de56f5eb4b5bbef10a2a78d4da9bda43f339a7/ENG/milanaCRM/milanaCRM/core/components/milanacrm/model/milanacrm.class.php)**

This is a little system, which doesn't need a complex route system so i do routing with crm_action param via switch/case.
If action has a method, then a suitable method executes, and return JSON to frontend, where JS class work with it.

## Interested things
 
- [getExcel](https://bitbucket.org/zahod5277/portfolio/src/89de56f5eb4b5bbef10a2a78d4da9bda43f339a7/ENG/milanaCRM/milanaCRM/assets/components/milanacrm/js/web/default.js#lines-36) method for PHP Office instance, which returns the Excel file

- Methods [exportOrder & exportOrders](https://bitbucket.org/zahod5277/portfolio/src/89de56f5eb4b5bbef10a2a78d4da9bda43f339a7/ENG/milanaCRM/milanaCRM/assets/components/milanacrm/js/web/default.js#lines-36) for different data in each other.

- [createXLSX](https://bitbucket.org/zahod5277/portfolio/src/89de56f5eb4b5bbef10a2a78d4da9bda43f339a7/ENG/milanaCRM/milanaCRM/assets/components/milanacrm/js/web/default.js#lines-36) method return a file. Look really great!
 
- [deleteFile](https://bitbucket.org/zahod5277/portfolio/src/89de56f5eb4b5bbef10a2a78d4da9bda43f339a7/ENG/milanaCRM/milanaCRM/assets/components/milanacrm/js/web/default.js#lines-36) method - removing file from server after admin is download this file for security.

- [Some hardcode](https://bitbucket.org/zahod5277/portfolio/src/89de56f5eb4b5bbef10a2a78d4da9bda43f339a7/ENG/milanaCRM/milanaCRM/assets/components/milanacrm/js/web/default.js#lines-36) :) 

## Frontend

jQuery. Yeah, shame on me.   

I use Fenom in php side - Smarty like template engine, which is really easy to install to MODX.
For example, you can see Fenom templates in **front\chunks\orderCRM\orders.outer.sudo.tpl**

JS Class  **milanaCRM** (**[milanaCRM\assets\components\milanacrm\js\web\default.js](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/milanaCRM/milanaCRM/assets/components/milanacrm/js/web/default.js)**)

**[ajax](https://bitbucket.org/zahod5277/portfolio/src/89de56f5eb4b5bbef10a2a78d4da9bda43f339a7/ENG/milanaCRM/milanaCRM/assets/components/milanacrm/js/web/default.js#lines-36)** method get a data, result HTML & name of callback function. Callbacks give an opportunity to show warnings or do some other things when query was executed.

## Design

Bootstrap 4, Font-awesome 5, Fancybox 3. 
 
 ![Риcунок 1](front1.png)
 
 ===============================
 
 ![Риcунок 1](front2.png)
 
I think it's really cool work.