<?php

return [
    'milanaCRMUserPolicyTemplate' => [
        'description' => 'milanaCRM policy template description.',
        'template_group' => 1,
        'permissions' => [
            'milanacrm_save' => [],
        ]
    ],
];