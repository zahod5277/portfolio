<?php

return [
    'manager_group' => [
        'xtype' => 'textfield',
        'value' => '',
        'area' => 'milanacrm_main',
    ],
    'manager_status_in' => [
        'xtype' => 'textfield',
        'value' => '2',
        'area' => 'milanacrm_main',
    ],
    'manager_exclude_statuses' => [
        'xtype' => 'textfield',
        'value' => '1',
        'area' => 'milanacrm_main',
    ],
];