<?php
/** @var xPDOTransport $transport */
/** @var array $options */
/** @var modX $modx */
if ($transport->xpdo) {
    $modx =& $transport->xpdo;

    $dev = MODX_BASE_PATH . 'Extras/milanaCRM/';
    /** @var xPDOCacheManager $cache */
    $cache = $modx->getCacheManager();
    if (file_exists($dev) && $cache) {
        if (!is_link($dev . 'assets/components/milanacrm')) {
            $cache->deleteTree(
                $dev . 'assets/components/milanacrm/',
                ['deleteTop' => true, 'skipDirs' => false, 'extensions' => []]
            );
            symlink(MODX_ASSETS_PATH . 'components/milanacrm/', $dev . 'assets/components/milanacrm');
        }
        if (!is_link($dev . 'core/components/milanacrm')) {
            $cache->deleteTree(
                $dev . 'core/components/milanacrm/',
                ['deleteTop' => true, 'skipDirs' => false, 'extensions' => []]
            );
            symlink(MODX_CORE_PATH . 'components/milanacrm/', $dev . 'core/components/milanacrm');
        }
    }
}

return true;