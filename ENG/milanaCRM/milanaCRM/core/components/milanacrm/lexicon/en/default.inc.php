<?php
include_once 'setting.inc.php';

$_lang['milanacrm'] = 'milanaCRM';
$_lang['milanacrm_menu_desc'] = 'A sample Extra to develop from.';
$_lang['milanacrm_intro_msg'] = 'You can select multiple items by holding Shift or Ctrl button.';

$_lang['milanacrm_items'] = 'Items';
$_lang['milanacrm_item_id'] = 'Id';
$_lang['milanacrm_item_name'] = 'Name';
$_lang['milanacrm_item_description'] = 'Description';
$_lang['milanacrm_item_active'] = 'Active';

$_lang['milanacrm_item_create'] = 'Create Item';
$_lang['milanacrm_item_update'] = 'Update Item';
$_lang['milanacrm_item_enable'] = 'Enable Item';
$_lang['milanacrm_items_enable'] = 'Enable Items';
$_lang['milanacrm_item_disable'] = 'Disable Item';
$_lang['milanacrm_items_disable'] = 'Disable Items';
$_lang['milanacrm_item_remove'] = 'Remove Item';
$_lang['milanacrm_items_remove'] = 'Remove Items';
$_lang['milanacrm_item_remove_confirm'] = 'Are you sure you want to remove this Item?';
$_lang['milanacrm_items_remove_confirm'] = 'Are you sure you want to remove this Items?';

$_lang['milanacrm_item_err_name'] = 'You must specify the name of Item.';
$_lang['milanacrm_item_err_ae'] = 'An Item already exists with that name.';
$_lang['milanacrm_item_err_nf'] = 'Item not found.';
$_lang['milanacrm_item_err_ns'] = 'Item not specified.';
$_lang['milanacrm_item_err_remove'] = 'An error occurred while trying to remove the Item.';
$_lang['milanacrm_item_err_save'] = 'An error occurred while trying to save the Item.';

$_lang['milanacrm_grid_search'] = 'Search';
$_lang['milanacrm_grid_actions'] = 'Actions';