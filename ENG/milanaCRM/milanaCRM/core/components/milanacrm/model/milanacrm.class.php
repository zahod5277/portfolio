<?php
require_once dirname(dirname(__FILE__)) . '/vendor/autoload.php';

class milanaCRM
{
    /** @var modX $modx */
    public $modx;

    /** @var miniShop2 $ms2 */
    public $ms2;

    public $Excel;

    /**
     * @param modX $modx
     * @param array $config
     */
    function __construct(modX &$modx, array $config = [])
    {
        $this->modx =& $modx;
        $this->isAjax = false;
        $this->error = false;
        $corePath = MODX_CORE_PATH . 'components/milanacrm/';
        $assetsUrl = MODX_ASSETS_URL . 'components/milanacrm/';
        $this->pdo = $this->modx->getService('pdoTools');
        $this->config = array_merge([
            'actionUrl' => $assetsUrl . 'action.php',
            'corePath' => $corePath,
            'modelPath' => $corePath . 'model/',
            'processorsPath' => $corePath . 'processors/',
            'connectorUrl' => $assetsUrl . 'connector.php',
            'assetsUrl' => $assetsUrl,
            'cssUrl' => $assetsUrl . 'css/',
            'jsUrl' => $assetsUrl . 'js/',
            'milanaJS' => $assetsUrl . 'js/web/default.js',
            'reportsUrl' => MODX_BASE_PATH . 'reports/',
            'managerGroup' => $this->modx->getOption('milanacrm_manager_group', 3),
        ], $config);

        if (!$this->ms2 = $this->modx->getService('minishop2')) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, '[milanaCRM] Requires installed miniShop2.');

            return false;
        }

        $this->ms2->initialize($ctx);

        $this->modx->addPackage('milanacrm', $this->config['modelPath']);
        $this->modx->lexicon->load('milanacrm:default');
    }

    public function loadJs($objectName = 'milanaCRM')
    {
        if ($js = trim($this->config['milanaJS'])) {
            if (preg_match('/\.js/i', $js)) {
                $this->modx->regClientScript(str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $js));
            }
        }
        $config = $this->modx->toJSON(array(
            'assetsUrl' => $this->config['assetsUrl'],
            'actionUrl' => str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $this->config['actionUrl'])
        ));
        $objectName = trim($objectName);
        $this->modx->regClientScript(
            "<script type=\"text/javascript\">{$objectName}.initialize({$config});</script>", true
        );
    }

    public function processAJAX($request)
    {
        $params = [];
        parse_str($request['params'], $params);
        switch ($request['crm_action']) {
            case 'filter':
                $result = $this->getOrders($params);
                break;
            case 'order_item':
                $result = $this->getOrderWithProduct($params);
                break;
            case 'change-manager':
                $result = $this->changeOrderManager($params);
                break;
            case 'change-status':
                $result = $this->changeOrderStatus($params);
                break;
            case 'order-excel':
                $result = $this->exportOrder($params);
                break;
            case 'orders-excel':
                $result = $this->exportOrders($params);
                break;
            case 'delete-file':
                $result = $this->deleteFile($params);
                break;
            default:
                $result = [];
                break;
        }

        if ($this->isAjax) {
            $output = json_encode([
                'status' => 'success',
                'data' => $result
            ]);
        } else {
            $output = $result;
        }

        return $output;
    }

    public function getManagersList($group_id)
    {
        if (!isset($group_id)) {
            $group_id = $this->config['managerGroup'];
        }
        $managers = $this->modx->getIterator('modUser', [
            'primary_group' => $group_id
        ]);

        foreach ($managers as $manager) {
            $mgrs[] = array_merge($manager->getOne('Profile')->toArray(), $manager->toArray());
        }

        return $mgrs;
    }

    public function getManager($id)
    {
        if ($user = $this->modx->getObject('modUser', $id)) {
            $profile = $user->getOne('Profile');
            $manager = array_merge($user->toArray(), $profile->toArray());
            return $manager;
        }
        return false;
    }

    public function changeOrderManager($params)
    {
        if (!$orderManager = $this->modx->getObject('milanaOrderManagerItem', [
            'order_id' => $params['order_id']
        ])) {
            $orderManager = $this->modx->newObject('milanaOrderManagerItem');
            $orderManager->set('order_id', $params['order_id']);
        }
        $orderManager->set('manager_id', $params['manager_id']);
        $orderManager->save();
        //TODO отправка сообщений менеджерам при их назначении
        $response = $this->modx->invokeEvent('milanaCRMSetManager', [
            'order_id' => $params['order_id'],
            'manager_id' => $params['manager_id']
        ]);

        return true;
    }

    public function changeOrderStatus($params)
    {
        $new_status = intval($params['status_id']);
        $order_id = intval($params['order_id']);
        return $this->ms2->changeOrderStatus($order_id, $new_status);
    }

    public function getOrders($params)
    {

        $where = $this->getWhereCondition($params);

        if (isset($params['limit']) && $params['limit'] != '') {
            $limit = intval($params['limit']);
        } else {
            $limit = 100;
        }

        $data = $this->pdo->runSnippet('@FILE:snippets/orderCRM/getOrdersCRM.php', [
            'where' => $where,
            'return_data' => 'data',
            'limit' => $limit,
            'tplAdmin' => '@FILE:chunks/orderCRM/orders.outer.sudo.tpl',
            'tplMgr' => '@FILE:chunks/orderCRM/orders.outer.tpl',
            'join' => [
                'orderManager' => [
                    'class' => 'milanaOrderManagerItem',
                    'on' => '`msOrder`.`id` = `orderManager`.`order_id`'
                ]

            ],
            'select_fields' => [
                'orderManager' => '`orderManager`.`manager_id` as `manager.id`'
            ]
        ]);

        return $data;
    }

    public function getOrderWithProduct($params)
    {
        $order_id = $params['order_id'];
        $data = $this->pdo->runSnippet('@FILE:snippets/orderCRM/getOrdersCRM.php', [
            'orderId' => $order_id,
            'includeProducts' => 1,
            'return_data' => 'data',
            'includeThumbs' => 'medium',
            'where' => [],
            'tplAdmin' => '@FILE:chunks/orderCRM/orders.modal.outer.tpl',
            'tplMgr' => '@FILE:chunks/orderCRM/orders.modal.outer.tpl',
            'join' => [
                'orderManager' => [
                    'class' => 'milanaOrderManagerItem',
                    'on' => '`msOrder`.`id` = `orderManager`.`order_id`'
                ]
            ],
            'select_fields' => [
                'orderManager' => '`orderManager`.`manager_id` as `manager.id`'
            ]
        ]);

        return $data;
    }

    public function getStatuses($is_sudo = false)
    {
        $statuses = $this->modx->getIterator('msOrderStatus');
        $exclude_statuses = [];
        $sts = [];
        foreach ($statuses as $status) {
            $st = $status->toArray();
            if (!$is_sudo) {
                $exclude_statuses = explode(',', $this->modx->getOption('milanacrm_manager_exclude_statuses'));
                $this->modx->log(1,'EXCL: '.print_r($exclude_statuses,1));
            }
            if (!in_array($st['id'], $exclude_statuses)){
                $sts[] = $st;
            }
        }
        return $sts;
    }

    protected function getWhereCondition($params)
    {
        if ($params['date_from'] != '' || $params['date_to'] != '') {
            $where[] = $this->getDateWhereCondition($params['date_from'], $params['date_to']);
        }

        if ($params['status'] != '') {
            $status_id = intval($params['status']);
            if ($status_id > 0) {
                $where[] = [
                    '`Status`.`id` = ' . $status_id
                ];
            }
        }

        if ($params['mgrs'] != '') {
            $mgr = intval($params['mgrs']);
            if ($mgr > 0) {
                $where[] = [
                    '`orderManager`.`manager_id` = ' . $mgr
                ];
            }
        }

        if ($params['other'] != '') {
            $where[] = $this->getLikeCondition($params['other']);
        }

        return $where;

    }

    public function getDateWhereCondition($d_from, $d_to)
    {
        $where = [];
        if (isset($d_from) && $d_from != '') {
            $date_from = $d_from;
            $dateF = new DateTime($date_from);
            $from = $dateF->format('Y-m-d');
        }

        if (isset($d_to) && $d_to != '') {
            $date_to = $d_to;
            $dateT = new DateTime($date_to);
            $to = $dateT->format('Y-m-d');
        }

        if (isset($from) && isset($to)) {
            $where[] = [
                '`msOrder`.`createdon` BETWEEN "' . $from .
                '" AND  "' . $to . '"'];
        }

        return $where;
    }

    public function getLikeCondition($str)
    {
        $fields = [
            '`msOrder`.`id`',
            '`msOrder`.`num`',
            '`msOrder`.`comment`',
            '`Address`.`receiver`',
            '`Address`.`phone`',
            '`Address`.`city`',
            '`Address`.`region`',
            '`User`.`email`'
        ];

        $str = htmlspecialchars($str);
        $prepared_conditions = [];
        foreach ($fields as $field) {
            $prepared_conditions[] = "{$field} LIKE '%{$str}%'";
        }

        $like = implode(' OR ', $prepared_conditions);
        return $like;
    }

    private function log($to_log)
    {
        $this->modx->log(1, print_r($to_log, 1));
        return true;
    }

    public function exportOrder($params)
    {
        $order_id = $params['order_id'];

        $order_products = $this->modx->getIterator('msOrderProduct', [
            'order_id' => $order_id
        ]);

        //@TODO сделать интерактивную шапку и выбор полей вообще
        $data[0] = [
            'Артикул',
            'Количество',
            'Размер',
            'Цвет',
            'Цена за единицу',
            'Сумма'
        ];

        foreach ($order_products as $order_product) {
            $op = $order_product->toArray();
            if ($product = $this->modx->getObject('msProductData', $op['product_id'])) {
                $data[] = [
                    $product->get('article'),
                    $op['count'],
                    $op['options']['size'],
                    $op['options']['color'],
                    $op['price'],
                    $op['cost'],
                ];
            }
        }

        //да простит меня бог за такой способ именования и вообще за всю эту строчку
        $filename = $this->config['reportsUrl'] . '/order-export-' . $order_id . '.xlsx';
        $excel = $this->getExcel();
        $excel->createXLSX($data, $filename);
        if (is_file($filename)) {
            //prepare filename to download - create link like https://site-url/file.xlsx
            $file = str_replace(MODX_BASE_PATH, $this->modx->getOption('site_url'), $filename);
            return $file;
        } else {
            return false;
        }
    }

    public function exportOrders($params)
    {
        $where = $this->getWhereCondition($params);

        //$this->log($where);

        $orders = $this->pdo->runSnippet('@FILE:snippets/orderCRM/getOrdersCRM.php', [
            'where' => $where,
            'return_data' => 'data',
            'toArray' => 1,
            'tplAdmin' => '@FILE:chunks/orderCRM/orders.outer.sudo.tpl',
            'tplMgr' => '@FILE:chunks/orderCRM/orders.outer.tpl',
            'join' => [
                'orderManager' => [
                    'class' => 'milanaOrderManagerItem',
                    'on' => '`msOrder`.`id` = `orderManager`.`order_id`'
                ]

            ],
            'select_fields' => [
                'orderManager' => '`orderManager`.`manager_id` as `manager.id`'
            ]
        ]);

        $data[0] = [
            'Номер заказа',
            'Дата создания',
            'Менеджер',
            'Статус',
            'Клиент',
            'Способ доставки',
            'Стоимость заказа',
            'Стоимость корзины',
        ];
        foreach ($orders['orders'] as $order) {
            $manager = $this->getManager($order['manager.id']);
            $data[] = [
                $order['order.num'],
                $order['order.createdon'],
                $manager['fullname'],
                $order['status.name'],
                $order['address.receiver'],
                $order['delivery.name'],
                $order['order.cost'],
                $order['order.cart_cost'],
            ];
        }
        //да простит меня бог за такой способ именования и вообще за всю эту строчку
        $filename = $this->config['reportsUrl'] . '/orders-export.xlsx';
        $excel = $this->getExcel();
        $excel->createXLSX($data, $filename);
        if (is_file($filename)) {
            //prepare filename to download - create link https://site-url/file.xlsx
            $file = str_replace(MODX_BASE_PATH, $this->modx->getOption('site_url'), $filename);
            return $file;
        } else {
            return false;
        }
    }

    public function deleteFile($params){
        $path = str_replace($this->modx->getOption('site_url'),MODX_BASE_PATH, $params['file']);
        if (is_file($path)){
            $this->log(microtime(true));
            sleep(2);
            $this->log(microtime(true));
        };
        return unlink($path);
    }

    public function getExcel()
    {
        $className = 'milanaCRMExcel';
        if (!class_exists($className)) {
            require_once dirname(__FILE__) . '/milanacrm/excel/milanacrmexcel.class.php';
        }

        try {
            return new $className($this->modx);
        } catch (Exception $e) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, 'Error create Excel class ' . $className . '  ' . $e->getMessage());
        }
        return null;
    }

}