milanaCRM.page.Home = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        components: [{
            xtype: 'milanacrm-panel-home',
            renderTo: 'milanacrm-panel-home-div'
        }]
    });
    milanaCRM.page.Home.superclass.constructor.call(this, config);
};
Ext.extend(milanaCRM.page.Home, MODx.Component);
Ext.reg('milanacrm-page-home', milanaCRM.page.Home);