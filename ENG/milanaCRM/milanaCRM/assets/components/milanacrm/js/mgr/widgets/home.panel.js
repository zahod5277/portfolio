milanaCRM.panel.Home = function (config) {
    config = config || {};
    Ext.apply(config, {
        baseCls: 'modx-formpanel',
        layout: 'anchor',
        /*
         stateful: true,
         stateId: 'milanacrm-panel-home',
         stateEvents: ['tabchange'],
         getState:function() {return {activeTab:this.items.indexOf(this.getActiveTab())};},
         */
        hideMode: 'offsets',
        items: [{
            html: '<h2>' + _('milanacrm') + '</h2>',
            cls: '',
            style: {margin: '15px 0'}
        }, {
            xtype: 'modx-tabs',
            defaults: {border: false, autoHeight: true},
            border: true,
            hideMode: 'offsets',
            items: [{
                title: _('milanacrm_items'),
                layout: 'anchor',
                items: [{
                    html: _('milanacrm_intro_msg'),
                    cls: 'panel-desc',
                }, {
                    xtype: 'milanacrm-grid-items',
                    cls: 'main-wrapper',
                }]
            }]
        }]
    });
    milanaCRM.panel.Home.superclass.constructor.call(this, config);
};
Ext.extend(milanaCRM.panel.Home, MODx.Panel);
Ext.reg('milanacrm-panel-home', milanaCRM.panel.Home);
