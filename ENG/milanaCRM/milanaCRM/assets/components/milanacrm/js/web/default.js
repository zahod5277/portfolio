var milanaCRM = {
    options: {
        'tooltip': '[data-toggle="tooltip"]',
        'filter_form': '#filter_form',
        'filter_submit_btn': '[data-crm-filter-submit]',
        'order_item': '[data-order-item]',
        'manager_select': '[data-crm-manager]',
        'status_select': '[data-crm-status]',
        'order_excel_btn': '[data-crm-order-excel]',
        'orders_excel_btn': '[data-crm-orders-excel]',
    },
    initialize: function (cfg) {
        var $this = this;
        $this.config = cfg;
        $this.tooltips($this.options);
        $this.filter($this.options, $this.config);
        $this.getOrderInfo($this.options, $this.config);
        $this.changeManager($this.options, $this.config);
        $this.changeStatus($this.options, $this.config);
        $this.resetFilter($this.options);
        $this.orderToExcel($this.options, $this.config);
        $this.ordersToExcel($this.options, $this.config);
    },
    tooltips: function (options) {
        $(options.tooltip).tooltip();
    },
    resetFilter: function (options) {
        $('[type="reset"]', options.filter_form).on('click', function () {
            $(options.filter_form).trigger("reset");
            $('select', options.filter_form).each(function () {
                $(this).prop('selectedIndex', 0);
            });
            $(options.filter_form).trigger('submit');
        });
    },
    ajax: function (config, action, params, result_html_id, callback) {
        $.post(
            config.actionUrl, {
                crm_action: action,
                params: params
            },
            function (resp) {
                resp = JSON.parse(resp);
                if (resp.status != 'error') {
                    if (result_html_id != '') {
                        $(result_html_id).html(resp.data);
                    }
                } else {
                    miniShop2.Message.error(resp.data);
                }
                callback(result_html_id, resp);
            });
    },
    getOrderInfo: function (options, config) {
        $('body').on('click', options.order_item, function (e) {
            e.preventDefault();
            let api = 'order_item',
                data_object = {
                    'order_id': $(this).data('order-item')
                },
                params = milanaCRM.serializeObject(data_object);
            milanaCRM.ajax(config, api, params, '[data-crm-modal]', function () {
                $.fancybox.open({
                    src: '[data-crm-modal]',
                    type: 'inline',
                    touch: false,
                    opts: {
                        afterShow: function (instance, current) {
                        }
                    }
                });
            });
        });
    },
    filter: function (options, config) {
        $('body').on('submit', options.filter_form, function (e) {
            e.preventDefault();
            let api = 'filter',
                params = $(this).serialize();
            milanaCRM.ajax(config, api, params, '[data-crm-orders]', function () {
                milanaCRM.tooltips(options);
            });
        });
    },
    changeManager: function (options, config) {
        $('body').on('change', options.manager_select, function (e) {
            e.preventDefault();
            let api = 'change-manager',
                data_object = {
                    'order_id': $(this).parents('tr').data('order-id'),
                    'manager_id': $(this).val()
                },
                params = milanaCRM.serializeObject(data_object);
            milanaCRM.ajax(config, api, params, '', function (options, response) {
                if (response.status != 'error') {
                    miniShop2.Message.success(response.data);
                    $(milanaCRM.options.filter_submit_btn).trigger('click');
                    milanaCRM.tooltips(options);
                } else {
                    miniShop2.Message.error(response.data);
                }
            });
        });
    },
    changeStatus: function (options, config) {
        $('body').on('change', options.status_select, function (e) {
            e.preventDefault();
            let api = 'change-status',
                data_object = {
                    'order_id': $(this).data('order-id'),
                    'status_id': $(this).val()
                },
                params = milanaCRM.serializeObject(data_object);
            milanaCRM.ajax(config, api, params, '', function (options, response) {
                if (response.status != 'error') {
                    miniShop2.Message.success(response.data);
                    milanaCRM.tooltips(options);
                } else {
                    miniShop2.Message.error(response.data);
                }
            });
        });
    },
    orderToExcel: function (options, config) {
        $(options.order_excel_btn).on('click', function () {
            let api = 'order-excel',
                data_object = {
                    'order_id': $(this).data('crm-order-id')
                },
                result_html_id = '',
                params = milanaCRM.serializeObject(data_object);
            milanaCRM.ajax(config, api, params, result_html_id, function (result_html_id, response) {
                if (response.status != 'error') {
                    let url = response.data;
                    milanaCRM.downloadAndDelete(options, config, response.data, function(url){
                        let api = 'delete-file',
                            data_object = {
                                'file': url
                            },
                            params = milanaCRM.serializeObject(data_object);
                        milanaCRM.ajax(config, api, params, result_html_id, function () {});
                    });
                } else {
                    miniShop2.Message.error('Невозможно скачать файл');
                }
            });
        });
    },
    ordersToExcel: function (options, config) {
        $(options.orders_excel_btn).on('click', function () {
            let api = 'orders-excel',
                result_html_id = '',
                params = $(options.filter_form).serialize();
            milanaCRM.ajax(config, api, params, result_html_id, function (result_html_id, response) {
                if (response.status != 'error') {
                    let url = response.data;
                    milanaCRM.downloadAndDelete(options, config, response.data, function(url){
                        let api = 'delete-file',
                            data_object = {
                                'file': url
                            },
                            params = milanaCRM.serializeObject(data_object);
                        milanaCRM.ajax(config, api, params, result_html_id, function () {});
                    });
                } else {
                    miniShop2.Message.error('Невозможно скачать файл');
                }
            });
        });
    },
    downloadAndDelete: function(options, config, url, callback){
        window.open(url, '_blank');
        setTimeout(callback(url), 2000);
    },
    serializeObject: function (obj) {
        var s = "";
        for (var key in obj) {
            if (s != "") {
                s += "&";
            }
            s += (key + "=" + encodeURIComponent(obj[key]));
        }
        return s;
    }
};