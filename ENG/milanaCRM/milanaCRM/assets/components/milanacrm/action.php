<?php

define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';
$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');
// Switch context if need

$milana = $modx->getService('milanaCRM', 'milanaCRM', MODX_CORE_PATH . 'components/milanacrm/model/');
if (!$milana) {
    return 'Could not load MilanaCRM class!';
}

$milana->isAjax = true;

echo $milana->processAJAX($_POST);

@session_write_close();