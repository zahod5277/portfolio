<?php

namespace Module;

use Bitrix\Main\Entity;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Highloadblock as HL;

class Multiprop
{
    static function GetValuesFromArr($data)
    {
        $values = [];
        foreach ($data as $key => $value) {
            foreach ($value['SUB_VALUES'] as $sub_keys => $sub_values) {
                switch ($sub_values['PROPERTY_TYPE']) {
                    case 'F':
                        $property_value = \CFile::GetPath($sub_values['VALUE']);
                        break;
                    case 'S':
                        $property_value = self::getStringValue($sub_values);
                        break;
                    default:
                        $property_value = $sub_values['VALUE'];
                }
                $values[$key][$sub_keys] = $property_value;
            }
        }
        return $values;
    }


    // @TODO сделать второй метод, MultiProp::GetValuesById($arResult['ID']);

    //код такого метода (условный)
    //    $res = CIBlockElement::GetList(Array(), ['ID' => $id], false, Array("nPageSize"=>50), $arSelect);
    //    while($ob = $res->GetNextElement()){
    //        $arFields = $ob->GetFields();
    //        print_r($arFields);
    //        $arProps = $ob->GetProperties();
    //        print_r($arProps);
    //    }


    static function getStringValue($value)
    {
        switch ($value['USER_TYPE']) {
            case 'HTML';
                $property_value = $value['~VALUE']['TEXT'];
                break;
            case 'directory':
                if (isset($value['USER_TYPE_SETTINGS']['TABLE_NAME'])) {
                    $property_value = self::getHighloadValue($value);
                }
                break;
            case 'MediaLibIblockProperty':
                $property_value = self::getMediaValues($value);
                break;
            default:
                $property_value = $value['VALUE'];
                break;
        }
        return $property_value;
    }

    static function getHighloadValue($value)
    {
        $hldata = array_pop(HL\HighloadBlockTable::getList(['filter' => ['TABLE_NAME' => $value['USER_TYPE_SETTINGS']['TABLE_NAME']]])->fetchAll());
        $entity_class = HL\HighloadBlockTable::compileEntity($hldata)->getDataClass();
        $res = $entity_class::getList(['select' => ['*'], 'order' => ['ID' => 'ASC'], 'filter' => ['UF_XML_ID' => $value["VALUE"]]])->fetchAll();
        return !empty($res) ? $res[0]['UF_DESCRIPTION'] : '';
    }

    static function getMediaValues($value)
    {
        $res = [];
        $medialibrary = \CMedialibItem::GetList([
            'arCollections' => [$value['VALUE']],
        ]);

        if (!empty($medialibrary)) {
            foreach ($medialibrary as $media) {
                $res[] = $media['PATH'];
            }
        }

        return $res;
    }
}