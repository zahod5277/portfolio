# Multifield class handler for Bitrix CMF

### Task
Create interface for adding product to site. Look the example on screen.png

### Analysis

We need a component which can work with different types of fields in one set. 

https://marketplace.1c-bitrix.ru/solutions/simai.complexprop/#tab-install-link

This component allows to group other properties in one set and use it multiply times.
But this component can't work with files library and can take just one file in set. 

We need to install component for the gallery

https://marketplace.1c-bitrix.ru/solutions/fgsoft.propmedia/
Now we can use Bitrix media library as page property

Now we should render this values on frontend. 
We have different types of properties:

- File (just one file, like Hero banner)
- String (title, description, some other text)
- Array (key-value)
- Media library for gallery   

### Solution
Files in Bitrix stores as ID.
We need to use a special method GetPath() to get a real path, not ID

```php
CFile::GetPath();
```

String depends of type editor (HTML, textarea, etc). So string value can be like that
```php
$arResult['PROPERTIES']['PROP_NAME']['VALUE'];
```
or like that (for to render text with HTML tags)
```php
$arResult['PROPERTIES']['PROP_NAME']['~VALUE']['TEXT'];
``` 

In Bitrix you can get array fields only if you know the instance of class, so first you must to find this instance.
After that you need to use GetList() method.

Media library has entities "Album" and "Files" 
If you need to show all files belongs to album, you need to know album ID and get all files from it. 


**All this things show us one simple idea - we need a special handler to rule this fields. 
We need a single class, which can process this properties and returns final values which we can use on site front.
In additional we can say it will be easy to improve and extend that class**

#### you can see the solution in "module/lib/multiprop.php" 


