'use strict';

const request = require('superagent');
const http = require('http');
const fs = require('fs');

module.exports = function(type) {
  return new Promise( (resolve, reject) => {

    request
      .get('https://api.randomuser.me/')
      .end( (err, res) => {
        if (err)
          reject(err);

        let data = JSON.parse(res.text).results[0];
		//console.log(data);
		data;
        if (type === 'simple') {
          resolve({
            firstName: data.name.first,
            lastName: data.name.last,
            email: data.email,
            username: data.login.username,
            password: data.login.password
          });
        } else {
          resolve(data);
        }

      });
  });
};
