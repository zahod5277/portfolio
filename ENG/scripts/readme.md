# This is a little set of different scripts done by me for the past years.

- [randomUser](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/scripts/randomUser/) - Node.js script for auto-generate user avatar via some service. I wrote it when eat my breakfast. Really! You can check time code on screenshots.
- [ajaxCalc](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/scripts/ajaxCalc.js) - jQuery plugin i wrote in 2018. It calculate service cost via AJAX by multiply params on CBSCG.ru.
- [amoPipelines](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/scripts/amoPipeline.js) - Module for amoCRM. This module allows choose what data will be hidden for managers (sales summ for example).  
- [groups.php](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/scripts/groups.php) - My ugly script for Bitrix24 CRM. It's ask list of current buildings which still developing from compnay ERP and returns coordinates & public info. It was written in august 2020. 
    * You can check how it works in [https://mechtaevo.ru/stroyka/](https://mechtaevo.ru/stroyka/). Just check a map!    
