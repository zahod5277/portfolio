define(['jquery'], function ($) {
    var CustomWidget = function () {
        var self = this;
        var system = self.system();

        var userEmail = system.amouser;

        this.callbacks = {
            render: function () {
                return true;
            },
            init: function () {

                jsonArr = JSON.parse(self.params.crater);
                var hideIDS = [];
                if (jsonArr) {
                    for (item in jsonArr) {
                        if (jsonArr[item].email == userEmail) {
                            var hideID = jsonArr[item].id;
                            hideIDS.push(jsonArr[item].id);

                            if (hideIDS) {
                                var pipes = true;
                                for (var k = 0; k < hideIDS.length; k++) {
                                    if (AMOCRM.data.current_view.pipeline_id == hideIDS[k]) {
                                        $('.pipeline__body').html('<h2>Эта воронка скрыта. Пожалуйста, перейдите в свою воронку</h2>');
                                    }
                                }
                            }


                            $('[data-entity=leads]').on('mouseenter', function () {
                                setTimeout(function () {
                                    if (hideIDS) {
                                        for (var i = 0; i < hideIDS.length; i++) {
                                            $('[data-id=' + hideIDS[i] + ']').hide();
                                        }
                                    }
                                    $('[data-id=' + hideID + ']').hide();
                                }, 200);
                            });
                            if (AMOCRM.data.current_entity == 'dashboard') {
                                $('[data-id=' + hideID + ']').hide();
                            }
                        }
                    }
                } else {
                    console.log('нет настроек');
                }
                return true;
            },
            bind_actions: function () {
                console.log('bind_actions');
                return true;
            },
            settings: function () {
                console.log('settings');
                return true;
            },
            onSave: function () {
                //alert('click');
                return true;
            },
            destroy: function () {

            },
            contacts: {
                //select contacts in list and clicked on widget name
                selected: function () {
                    console.log('contacts');
                }
            },
            leads: {
                //select leads in list and clicked on widget name
                //console.log('leads');
                selected: function () {
                    //console.log('leads');
                    console.log('leads select');
                }
            },
            tasks: {
                //select taks in list and clicked on widget name
                selected: function () {
                    console.log('tasks');
                }
            }
        };
        return this;
    };

    return CustomWidget;
});