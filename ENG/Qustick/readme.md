# [Qustick.ru](https://qustick.ru) (link is work)

## My own pet-project.
## Marketplace for musicians like Reverb.com

### Date of creation: couple hours (not everyday) after work in summer-winter 2021 ;)

First thing you need to know - this is a MVP.
I was wanted to check the idea of russian music marketplace. In case this project will increase it will be redesigned on Vue & Laravel.   
It works on my favourite MODX Revo now, because it fast & simple to start.  
 
Just main module "Qustick"  stores in this repo. It's a set of classes which extends MODX standard functions.

There a list:

* Micro-CRM. I was re-used code from my previous work [MilanaCRM](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/milanaCRM/). By the way it prove i can write a code with good abstraction level, my code can be used many times in different tasks.
* [QustickMessages](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Qustick/model/qustickmessages.class.php). Very simple message module, without sockets & realtime. Basically it's just a comment page, when just 2 users can leave comments to each other.
* [SEO addresses generator for products](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Qustick/utils/aliasgenerator.class.php).     
* [Dadata API handler](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Qustick/utils/dadata.class.php)
    * Can detect city by IP
    * Can suggest locality by first symbols of locality name (autocomplete)  
