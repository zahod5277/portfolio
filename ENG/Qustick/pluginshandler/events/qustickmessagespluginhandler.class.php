<?php
if (!class_exists('qustickMultiPluginHandler')) {
    require_once MODX_CORE_PATH . 'components/qustick/pluginshandler/qustickmultipluginhandler.class.php';
}

/**
 * Class qustickMessagesPluginHandler
 */
class qustickMessagesPluginHandler extends qustickMultiPluginHandler
{
    /*
     * @return bool
     */
    public function OnCommentSave()
    {
        $comment = $this->object->toArray();
        $this->modx->log(1, 'COMMENT ARR: '.print_r($comment,1));
        if (!is_int($comment['thread'])){
            return false;
        }

        $thread = $this->modx->getObject('TicketThread', $comment['thread']);
        if ($comment['createdby'] != $thread->get('createdby') || strstr($thread->get('name'),'product-comment-') != false){
            return true;
        }

        $this->message_handler = $this->modx->getService('qustickMessages', 'qustickMessages', MODX_CORE_PATH . 'components/qustick/model/');
        if (!$this->message_handler) {
            return 'false';
        }

        $user_email = $this->message_handler->getUserMessageCompanionByThread($comment);

        if ($author_profile = $this->modx->getObject('modUserProfile', [
            'email' => $comment['email']
        ])) {
         $author_name = $author_profile->get('fullname');
         $message = 'Вам пришло новое сообщение на сайте Кустик  от пользователя '.$author_name.'<br>';
        } else {
            $message = 'Вам пришло новое сообщение на сайте Кустик <br>';
        }

        $message .= 'Чтобы прочитать сообщение, пожалуйста, перейдите по <a href="https://qustick.ru/messages/">ссылке</a>';
        $this->ms2->sendEmail($user_email, 'Новое сообщение на Кустике', $message);
        return true;
    }


}