<?php
if (!class_exists('qustickMultiPluginHandler')) {
    require_once MODX_CORE_PATH . 'components/qustick/pluginshandler/qustickmultipluginhandler.class.php';
}

/**
 * Class qustickProductPluginHandler
 */
class qustickProductPluginHandler extends qustickMultiPluginHandler
{
    private $alias_generator;

    public function __construct(modX $modx, &$scriptProperties = [])
    {
        if (!class_exists('aliasGenerator')) {
            require_once MODX_CORE_PATH . 'components/qustick/utils/aliasgenerator.class.php';
        }
        $this->alias_generator = new \Qustick\Utils\aliasGenerator($modx);
        parent::__construct($modx, $scriptProperties);
    }

    public function OnLoadWebDocument()
    {

        if ($this->modx->resource->class_key == 'msProduct'){
            $product_options = [];
            $options = $this->modx->getIterator('msProductOption', [
               'product_id' => $this->modx->resource->id
            ]);
            foreach ($options as $option){
                $product_options[$option->get('key')] = $option->get('value');
            }
            $this->modx->resource->set('product_options', $product_options);
        }
        return true;
    }

    public function OnDocFormSave(){

        if ($this->resource->class_key == 'msProduct' && $this->mode == 'new'){
            $this->alias_generator->generateAlias($this->id);
        }
        return true;
    }
}