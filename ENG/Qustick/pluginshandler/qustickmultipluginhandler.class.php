<?php
if (!class_exists('qustickPluginHandler')) {
    require_once MODX_CORE_PATH.'components/qustick/pluginshandler/pluginhandler.class.php';
}

/**
 * Class qustickMultiPluginHandler
 */
class qustickMultiPluginHandler extends qustickPluginHandler
{
    public function run()
    {
        $eventName = $this->modx->event->name;
        if (method_exists($this, $eventName)) {
            $this->$eventName();
        }
    }
}