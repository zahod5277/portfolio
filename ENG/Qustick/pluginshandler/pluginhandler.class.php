<?php

abstract class qustickPluginHandler{
    /** @var modX $modx */
    public $modx;
    /** @var pdoTools $pdoTools */
    public $pdoTools;
    /** @var miniShop2 $ms2 */
    public $ms2;
    /** @var array $scriptProperties */
    protected $scriptProperties;
    /** @var bool $isAjax */
    public $isAjax;
    /** @var array $config */
    public $config;
    /**
     * @param modX $modx
     * @param array $config
     */
    public function __construct(modX $modx, &$scriptProperties = [])
    {
        $this->modx = &$modx;
        $this->scriptProperties = &$scriptProperties;

        $this->isAjax = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';

        $this->config = [
            'prefix' => $this->modx->getOption(xPDO::OPT_TABLE_PREFIX),
        ];

        //@todo fix!
        //if scriptProperties contain a object, ms2 will be crush on init. Need to fix this
        $do_not_load_ms2 = false;
        foreach ($this->scriptProperties as $key => $property) {
//            $this->modx->log(1, 'Добавляем в propetrties: '.$key);
//            $this->modx->log(1, 'Ее тип: '.gettype($property));
            $this->$key = $property;
            if (gettype($property) == 'object'){
                $do_not_load_ms2 = true;
            }
        }

        //$this->modx->log(1, 'CTX: ' . $this->modx->context->get('key'));
        if (!$do_not_load_ms2 && $this->ms2 = $this->modx->getService('miniShop2')) {
            $this->ms2->initialize($this->modx->context->get('key'), $this->scriptProperties);
        }

        if ($this->pdoTools = $this->modx->getService('pdoFetch')) {
            $this->pdoTools->setConfig($this->config);
        }
    }

    abstract public function run();
}