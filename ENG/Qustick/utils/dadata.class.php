<?php

class qustickDadata
{
    /** @var modX $modx */
    public $modx;
    /** @var bool $isAjax */
    public $isAjax;
    /** @var array $config */
    public $config;

    /**
     * @param modX $modx
     * @param array $config
     */
    public function __construct(modX $modx)
    {
        $this->modx = &$modx;
        $this->isAjax = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';

        $this->ip = $_SERVER['SERVER_ADDR'];

        $this->config = [
            'api_url' => $this->modx->getOption('dadata_api_url'),
            'api_key' => $this->modx->getOption('dadata_api_key'),
        ];
    }

    private function request($url, $params = [])
    {
        $headers = [
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Token ' . $this->config['api_key']
        ];

        $req_url = $this->config['api_url'] . $url;

        $ch = curl_init($req_url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);

        if (!empty($params)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        }

        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, 1);
    }

    public function getCityByIP($ip = '')
    {
        $url = 'iplocate/address';
        if (empty($ip)) {
            $ip = $url . '?ip=' . $_SERVER['REMOTE_ADDR'];
        }
        $result = $this->request($url);

        return $result;
    }

    public function suggest($search)
    {
        $url = 'suggest/address';
        $request = [
            'query' => $search,
            'count' => 20
        ];
        $result = $this->request($url, $request);
        $cities = [];

        //@todo - сделал какое-то говно в плане хранения уже занесённых в автокомплит городов, надо придумать способ
        // фильтровать это дело получше, а то хуйня получается какая-то
        $cities_kladrs = [];

        if (!empty($result['suggestions'])) {
            foreach ($result['suggestions'] as $res) {
                if (mb_stripos($res['data']['city'], $search) === 0) {
                    if (!in_array($res['data']['city_kladr_id'],$cities_kladrs)) {
                        $cities_kladrs[] = $res['data']['city_kladr_id'];
                        $cities[] = $res;
                    }
                }
            }
        }

        if (empty($cities)) {
            return [
                'status' => 'error',
                'data' => 'Не найдено ни одного населённого пункта в нашем списке'
            ];
        }

        return $cities;
    }
}