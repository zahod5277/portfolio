<?php
//require_once dirname(dirname(__FILE__)) . '/vendor/autoload.php';

class qustickCRM
{
    /** @var modX $modx */
    public $modx;

    /** @var miniShop2 $ms2 */
    public $ms2;

    /** @var pdoTools $pdo */
    public $pdo;

    public $Excel;

    /**
     * @param modX $modx
     * @param array $config
     */
    function __construct(modX &$modx, array $config = [])
    {
        $this->modx =& $modx;
        $this->isAjax = false;
        $this->error = false;
        $corePath = MODX_CORE_PATH . 'components/qustick/';
        $assetsUrl = MODX_ASSETS_URL . 'components/qustick/';
        $this->pdo = $this->modx->getService('pdoTools');
        $this->config = array_merge([
            'actionUrl' => $assetsUrl . 'rest.php',
            'corePath' => $corePath,
            'modelPath' => $corePath . 'model/',
            'assetsUrl' => $assetsUrl,
            'cssUrl' => $assetsUrl . 'css/',
            'jsUrl' => $assetsUrl . 'js/',
            'qustickCRMjs' => $assetsUrl . 'js/qustickCRM.js',
            'reportsUrl' => MODX_BASE_PATH . 'reports/',
        ], $config);

        if (!$this->ms2 = $this->modx->getService('minishop2')) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, '[qustickCRM] Requires installed miniShop2.');

            return false;
        }

        $this->ms2->initialize('web');

        $this->modx->addPackage('qustickcrm', $this->config['modelPath']);
    }

    public function loadJs($objectName = 'qustickCRM')
    {
        if ($js = trim($this->config['qustickCRMjs'])) {
            if (preg_match('/\.js/i', $js)) {
                $this->modx->regClientScript(str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $js));
            }
        }
        $config = $this->modx->toJSON(array(
            'assetsUrl' => $this->config['assetsUrl'],
            'actionUrl' => str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $this->config['actionUrl'])
        ));
        $objectName = trim($objectName);
        $this->modx->regClientScript(
            "<script type=\"text/javascript\">{$objectName}.initialize({$config});</script>", true
        );
    }

    public function changeOrderStatus($order_id, $status_id)
    {
        return $this->ms2->changeOrderStatus($order_id, $status_id);
    }

    public function getOrders($params)
    {

        $where = $this->getWhereCondition($params);

        if (isset($params['limit']) && $params['limit'] != '') {
            $limit = intval($params['limit']);
        } else {
            $limit = 100;
        }

        $data = $this->pdo->runSnippet('@FILE:snippets/office/CRM/getOrdersCRM.php', [
            'where' => $where,
            'return_data' => 'data',
            'limit' => $limit,
            'tpl' => '@FILE:chunks/office/CRM/orders.outer.tpl',
        ]);

        return $this->success($data);
    }

    /*
     * @param int $order_id
     * @return
     */
    public function getOrder($order_id = 0)
    {
        if ($order = $this->modx->getObject('msOrder', $order_id)){
            if ($order->get('seller_id') == $this->modx->user->id){
                $tpl = '@FILE:chunks/office/CRM/orders.modal.outer.sudo.tpl';
            } else {
                $tpl = '@FILE:chunks/office/CRM/orders.modal.outer.tpl';
            }
            $data = $this->pdo->runSnippet('@FILE:snippets/office/CRM/getOrdersCRM.php', [
                'orderId' => $order_id,
                'includeProducts' => 1,
                'return_data' => 'data',
                'includeThumbs' => 'small',
                'where' => [],
                'tpl' => $tpl
            ]);
            return $this->success($data);
        }
        return $this->error('Заказ не найден');
    }

    public function getStatuses($is_sudo = false)
    {
        $statuses = $this->modx->getIterator('msOrderStatus');
        $exclude_statuses = [];
        $sts = [];
        foreach ($statuses as $status) {
            $st = $status->toArray();
            if (!$is_sudo) {
                $exclude_statuses = explode(',', $this->modx->getOption('qustickcrm_manager_exclude_statuses'));
                //$this->modx->log(1,'EXCL: '.print_r($exclude_statuses,1));
            }
            if (!in_array($st['id'], $exclude_statuses)){
                $sts[] = $st;
            }
        }
        return $sts;
    }

    protected function getWhereCondition($params)
    {
        if ($params['date_from'] != '' || $params['date_to'] != '') {
            $where[] = $this->getDateWhereCondition($params['date_from'], $params['date_to']);
        }

        if ($params['status'] != '') {
            $status_id = intval($params['status']);
            if ($status_id > 0) {
                $where[] = [
                    '`Status`.`id` = ' . $status_id
                ];
            }
        }

        if ($params['mgrs'] != '') {
            $mgr = intval($params['mgrs']);
            if ($mgr > 0) {
                $where[] = [
                    '`orderManager`.`manager_id` = ' . $mgr
                ];
            }
        }

        if ($params['other'] != '') {
            $where[] = $this->getLikeCondition($params['other']);
        }

        //$this->log('WHERE: '.print_r($where,1));
        return $where;

    }

    protected function getDateWhereCondition($d_from, $d_to)
    {
        $where = [];
        if (isset($d_from) && $d_from != '') {
            $date_from = $d_from;
            $dateF = new DateTime($date_from);
            $from = $dateF->format('Y-m-d');
        }

        if (isset($d_to) && $d_to != '') {
            $date_to = $d_to;
            $dateT = new DateTime($date_to);
            $to = $dateT->format('Y-m-d');
        } else {
            $to = date('Y-m-d');
        }

        if (isset($from) && isset($to)) {
            $where[] = [
                '`msOrder`.`createdon` BETWEEN "' . $from .
                '" AND  "' . $to . '"'];
        }

        return $where;
    }

    protected function getLikeCondition($str)
    {
        $fields = [
            '`msOrder`.`id`',
            '`msOrder`.`num`',
            '`msOrder`.`comment`',
            '`Address`.`receiver`',
            '`Address`.`phone`',
            '`Address`.`city`',
            '`Address`.`region`',
            '`User`.`email`'
        ];

        $str = htmlspecialchars($str);
        $prepared_conditions = [];
        foreach ($fields as $field) {
            $prepared_conditions[] = "{$field} LIKE '%{$str}%'";
        }

        $like = implode(' OR ', $prepared_conditions);
        return $like;
    }

    private function log($to_log)
    {
        $this->modx->log(1, print_r($to_log, 1));
        return true;
    }

    public function deleteFile($params){
        $path = str_replace($this->modx->getOption('site_url'),MODX_BASE_PATH, $params['file']);
        if (is_file($path)){
            $this->log(microtime(true));
            sleep(2);
            $this->log(microtime(true));
        };
        return unlink($path);
    }

    private function success($data){
        return [
            'status' => 'success',
            'data' => $data
        ];
    }
    private function error($data){
        return [
            'status' => 'error',
            'data' => $data
        ];
    }

}