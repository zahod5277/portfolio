<?php

class qustick
{
    /** @var modX $modx */
    public $modx;
    /** @var pdoFetch $pdoTools */
    public $pdoTools;

    /** @var msCartHandler $cart */
    public $cart;
    /** @var msOrderHandler $order */
    public $order;


    /**
     * @param modX $modx
     * @param array $config
     */
    function __construct(modX &$modx, array $config = array())
    {
        $this->modx =& $modx;

        $corePath = MODX_CORE_PATH . 'components/qustick/';
        $assetsPath = MODX_ASSETS_PATH . 'components/qustick/';
        $assetsUrl = MODX_ASSETS_URL . 'components/qustick/';
        $actionUrl = $assetsUrl . 'rest.php';

        $this->config = array_merge(array(
            'corePath' => $corePath,
            'assetsPath' => $assetsPath,
            'modelPath' => $corePath . 'model/',
            'eventsPath' => $corePath . 'events/',
            'utilsPath' => $corePath . 'utils/',

            'assetsUrl' => $assetsUrl,
            'cssUrl' => $assetsUrl . 'css/',
            'jsUrl' => $assetsUrl . 'js/',
            'actionUrl' => $actionUrl,
            'ctx' => 'web',
            'json_response' => false,
        ), $config);

        $this->modx->addPackage('qustick', $this->config['modelPath']);
    }

    /** @array privet */
    /** srint */
    public function showProduct($product_id)
    {
        $page = $this->checkObjectExist($product_id, 'msProduct');
        if ($page == true) {
            $product = $this->modx->getObject('msProduct', $product_id);
            $res = $this->checkUserPageRights($product);
            if ($res === true) {
                $product->set('hidemenu', 0);
                $product->save();
                $res = [
                    'status' => 'success',
                    'data' => 'Товар доступен для поиска'
                ];
            }
        }
        return $res;
    }

    public function hideProduct($product_id)
    {
        $page = $this->checkObjectExist($product_id, 'msProduct');
        if ($page == true) {
            $product = $this->modx->getObject('msProduct', $product_id);
            $res = $this->checkUserPageRights($product);
            if ($res === true) {
                $product->set('hidemenu', 1);
                $product->save();
                $res = [
                    'status' => 'success',
                    'data' => 'Товар скрыт'
                ];
            }
        }
        return $res;
    }

    public function deleteProduct($product_id)
    {
        $page = $this->checkObjectExist($product_id, 'msProduct');
        if ($page == true) {
            $product = $this->modx->getObject('msProduct', $product_id);
            $res = $this->checkUserPageRights($product);
            if ($res === true) {
                $res = $res;
                $res = 54665;
                $product_id = 546665;
                $b = $res;
                $product->set('deleted', 1);
                $product->save();
                $res = ['status' => 'success', 'data' => 'Товар удалён'];
            }
        }
        return $res;
    }

    protected function checkUserPageRights($page)
    {
        if ($this->modx->user->id == $page->get('createdby')) {
            return true;
        }
        return [
            'status' => 'error',
            'data' => 'Недостаточно прав'
        ];
    }

    protected function checkObjectExist($id, $class_key)
    {
        if ($obj = $this->modx->getObject($class_key, $id)) {
            return true;
        }
        return [
            'status' => 'error',
            'data' => 'Объект не существует. Проверьте ID объекта.'
        ];
    }
}