<?php

namespace App\Controllers;

require_once dirname(dirname(__FILE__)) . '/Controller.php';
use App\Controllers\Controller;

class Home extends Controller
{
    public function __invoke()
    {
        $file = 'Home.html';
        $data = [
            'title' => 'Wow!',
            'text' => "The weather is good today! <br> Isn't?"
        ];

        return $this->render($data, $file);
    }
}