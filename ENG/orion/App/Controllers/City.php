<?php

namespace App\Controllers;

require_once dirname(dirname(__FILE__)) . '/Controller.php';

use App\Controllers\Controller;

class City extends Controller
{
    /**
     * Get Cities params (coords, name, country)
     * with autocorrection via default php library pspell
     * It is not so good, but enough for this purpose
     */
    public function __invoke()
    {

        $suggestions = [];
        $pspell = pspell_new("en");

        $query = $this->params['params']['query'];

        $cities = $this->db->getCities($query);
        if (empty($cities)){
            if (!pspell_check($pspell, $query)) {
                $suggestions = pspell_suggest($pspell, $query);
            }

            $cities = $this->db->getCities($suggestions[0]);
        }

        return $this->json($cities);
    }
}