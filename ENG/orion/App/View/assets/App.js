class App{
    constructor() {
        this.options = {
            suggest_city_selector: '[data-js-action="suggest-city"]',
            weather_html_container: '[data-js-action="show-weather"]'
        }
        this.suggestCity();
    }

    suggestCity(){
        const input = document.querySelector(this.options.suggest_city_selector);
        input.addEventListener("keyup", (event) => {
            let weather_container = document.querySelector(this.options.weather_html_container);
            weather_container.innerHTML = '';

            if (input.value.length >= 3){
                let api = 'suggest_city',
                    params = {
                        query: input.value
                    };
                this.ajax(api, params).then(cities => {
                    let cities_array = JSON.parse(cities);
                    cities_array.forEach( city => {
                        this.getWeather(city);
                    })
                });
            }
        });
    }

    getWeather(city){
        let api = 'get_weather',
            params = {
                long: city[2],
                lat: city[3],
                city: city[1],
                country: city[4]
            };
        this.ajax(api, params).then(html => {
            this.setHtml(this.options.weather_html_container, html)
        });
    }

    ajax(api, params) {
        let data = {
            api: api,
            params: params
        }

        let result = fetch('/index.php', {
            method: 'POST',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify(data)
        })
        .then((response) => {
            return response.text();
        })


        return result;
    }

    setHtml(selector, html){
        let input = document.querySelector(selector);
        input.innerHTML += html;
    }
}

new App();