<?php

namespace App;

class Config
{
    static private array $config = [
        'db_host' => 'localhost',
        'db_username' => 'zahod5277_orion',
        'db_password' => '0k*GGjQn',
        'db_name' => 'zahod5277_orion'
    ];

    public function get(string $key)
    {
        return self::$config[$key];
    }

    public function set($key, $value):void
    {
        self::$config[$key] = $value;
    }

    public function toArray(): array
    {
        return self::$config;
    }
}