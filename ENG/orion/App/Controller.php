<?php

namespace App\Controllers;

use App\Config;
use App\DB;

require_once 'DB.php';
require_once 'Config.php';

class Controller
{
    public DB $db;
    protected Config $config;
    private string $template_path;
    public array $params;

    public function __construct($params)
    {
        $this->template_path = 'App/View/';
        $this->config = new Config();
        $this->db = new DB($this->config->toArray());
        $this->params = $params;
    }

    /**
     * Render array into HTML chunk like TWIG or Blade (but much dumb)
     * @param array $data
     * @param string $file
     * @return void
     */
    public function render(array $data, string $file):void
    {
        $html = '';

        $template = $this->template_path.$file;

        if (is_file($template)){
            $raw = file_get_contents($template);
            $html = $raw;

            foreach ($data as $tag => $value){
                $html = str_replace("{{{$tag}}}", $value, $html);
            }
        }

        echo $html;
    }

    /**
     * render array into json for AJAX
     * @param array $data
     * @return void
     */
    public function json(array $data):void
    {
        echo json_encode($data);
    }
}
