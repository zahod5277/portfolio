<?php

namespace App;

use \mysqli;

class DB
{
    public mysqli $db;

    public function __construct($options)
    {
        $db = new mysqli(
            $options['db_host'],
            $options['db_username'],
            $options['db_password'],
            $options['db_name'],
        );

        mysqli_set_charset($db, 'UTF8');

        $this->db = $db;
    }

    /**
     * LIMIT 3 is for better predictability and perfomance
     * @param string $city_name
     * @return array
     */
    public function getCities(string $city_name): array
    {
        $query = "SELECT * FROM `big_cities` WHERE `name` LIKE '{$city_name}%' LIMIT 3";
        return $this->db->query($query)->fetch_all();
    }

    /**
     * Execute any query
     * @param string $query
     * @return void
     */
    public function query(string $query): void
    {
        $this->db->query($query);
    }
}