<?php

require_once 'App/App.php';

$app = new \App\App();

$post_data = json_decode(file_get_contents("php://input"), true) ?: [];

$req = array_merge($_REQUEST, $post_data);

$app->handleRequest($req);