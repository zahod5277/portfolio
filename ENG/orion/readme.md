### TEST CASE

### Task

Create a service that displays the current weather by city, with autocomplete and input error correction. For example, if a user enters "Lomdon," the service will still display the weather for London, and so on.
I cannot share the full details of the task due to management restrictions, so this is just a general overview.
The main requirement was to not use any pre-built or third-party solutions.

## How to install

+ Clone repo
+ Import **big_cities.sql** into your database
+ Set config DB connection in App/Config.php
+ Visit index.php with your browser

### Solution Explanation

In this solution, I didn’t use any third-party libraries at all.
Everything was written exclusively in native PHP 7.4 and JavaScript.

For data validation, I used the standard PHP pspell library. While it struggles with city names, it correctly recognizes London and Moscow! :)

The total time to solve the problem was 11 hours.

![time tracking](time.jpg "Time tracking from PHP Storm").

Of these, 2 hours were spent on planning the architecture and trying to make an adequate templateizer

Two hours were spent working with the database. My version of MySQL and phpMyAdmin for some reason could not import csv from the terms of the task, I had to
I had to write a code to import into the database, which did it line by line. Plus initially I used another database, but it turned out to have a lot of garbage values, so two hours were spent on customizing the database.

I spent another two hours trying to turn this system into a fancy framework, but I got tired and rolled it back to a new one