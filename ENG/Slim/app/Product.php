<?php

namespace app;

class Product
{
	
	public static function prepare($product, $getBundles = true)
	{
		global $modx;

		$excludes = array(
			'alias',
			'cacheable',
			'class_key',
			'contentType',
			'content_dispo',
			'content_type',
			'context_key',
			'createdby',
			'createdon',
			'deleted',
			'deletedby',
			'deletedon',
			'donthit',
			'editedby',
			'editedon',
			'hide_children_in_tree',
			'hidemenu',
			'id',
			'isfolder',
			'link_attributes',
			'menuindex',
			'menutitle',
			'parent',
			'privatemgr',
			'privateweb',
			'properties',
			'pub_date',
			'published',
			'publishedby',
			'publishedon',
			'richtext',
			'searchable',
			'show_in_tree',
			'source',
			'template',
			'type',
			'unpub_date',
			'uri',
			'uri_override',
			'vendor.id',
			'vendor.resource',
			'vendor.country',
			'vendor.logo',
			'vendor.address',
			'vendor.phone',
			'vendor.fax',
			'vendor.email',
			'vendor.description',
			'vendor.properties',
		);
		

		$output = array_merge(
			$product->toArray(),
			array(
				'remains' => $modx->runSnippet('getRemains', array('return' => 'data', 'id' => $product->id)),
				'product_productcode' => $product->getTVValue(8),
				'product_complect' => $product->getTVValue(10),
				'product_special_features' => $product->getTVValue(3),
				'product_tech_spec' => $product->getTVValue(4),
				//'remains' => json_decode($modx->runSnippet('getRemains', array('return' => 'json', 'id' => $product->id)))
			)
		);
		if ($getBundles){
		    $output['Related_Product'] = [];
    		$q = $modx->newQuery('msProduct')
    		    ->leftJoin('msProductBundle', 'Bundle', 'Bundle.bid = msProduct.id')
        		->where([
        		    'Bundle.rid'   =>  $product->id
        	    ]);
    	    foreach ($modx->getIterator('msProduct', $q) as $bundle){
    	        $output['Related_Product'][$bundle->getTVValue(8)] = static::prepare($bundle, false);
    	    }
		}

		$output = array_diff_key($output, array_flip($excludes));

		return $output;
	}


	public static function get($ctx = '', $postData = []) 
	{
		global $modx;

		$code = isset($postData['code']) ? trim($postData['code']) : '';
		if (empty($code)){
			throw new \Exception("Product not found", 404);
		}
		$code = array_map('trim', explode(',', $code));

		$c = $modx->newQuery('msProduct')
			->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id')
			->innerJoin('modTemplateVarResource', 'Code', array('msProduct.id = Code.contentid', 'Code.tmplvarid' => 8))
			->where(array(
				'Code.value:IN' => $code,
				'msProduct.deleted' => 0,
				'msProduct.published' => 1,
				'msProduct.context_key' => $ctx,
			))
			//->limit(1)
			;
		$results = [];
		foreach ($modx->getIterator('msProduct', $c) as $product){
			$results[] = static::prepare($product);
		}
		if (count($code) > 1){
		    return [
		        'results'   =>  $results,
	        ];
		}
		else{
		    return [
		        'object'   =>  $results[0],
	        ];
		}
	}


	public static function all($ctx = '', $postData = [])
	{
		global $modx;

		$limit = intval(isset($postData['limit']) ? $postData['limit'] : 0);
		$offset = intval(isset($postData['offset']) ? $postData['offset'] : 0);

		if (empty($limit)) $limit = 20;
		elseif ($limit > 50) $limit = 50;

		$sortby = isset($postData['sortby']) ? $postData['sortby'] : '';
		if (!in_array($sortby, array('pagetitle', 'longtitle', 'article', 'price', 'old_price', 'weight', 'barcode', 'product_productcode'))){
			$sortby = 'product_productcode';
		}
		$sortdir = isset($postData['sortdir']) ? $postData['sortdir'] : '';
		if (!in_array($sortdir, array('asc', 'desc'))){
			$sortdir = 'asc';
		}

		switch(true){
			case ($sortby == 'product_productcode'):
				$sortby = 'Code.value';
				break;
			case (in_array($sortby, array_keys($modx->getFields('modResource')))):
				$sortby = "msProduct.{$sortby}";
				break;
			case (in_array($sortby, array_keys($modx->getFields('msProductData')))):
				$sortby = "Data.{$sortby}";
				break;
			default:
				$sortby = "Code.value";
				break;
		}

		$data = array(
			'results' => array(),
			'total' => 0,
			'limit' => $limit,
			'offset' => $offset,
		);

		$criteria = array(
			'msProduct.deleted' => 0,
			'msProduct.published' => 1,
			'msProduct.context_key' => $ctx,
		);
		
		$code = isset($postData['code']) ? $postData['code'] : '';
		if ($code){
		    $code = array_diff(explode(',', $code), ['']);
	        $criteria['Code.value:IN'] = $code;
		}

		$q = $modx->newQuery('msProduct')
			->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id')
			->innerJoin('modTemplateVarResource', 'Code', array('msProduct.id = Code.contentid', 'Code.tmplvarid' => 8))
			->where($criteria)
			->sortby($sortby, $sortdir)
			->groupby('msProduct.id');

		$data['total'] = $modx->getCount('msProduct', $q);

		if ($data['total'] > 0){
			$q->limit($limit, $offset);
			foreach ($modx->getIterator('msProduct', $q) as $product){
				$data['results'][] = static::prepare($product);
			}
		}
		
		return $data;
	}
}