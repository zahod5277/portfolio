<?php

namespace app;

class AccessByToken
{
	public static function check($host, $token)
	{
		global $modx;

		if (!$dhToken = $modx->getObject('dhApiToken', array('token' => $token, 'active' => 1))){
			throw new \Exception("Failed token", 403);
		}

		$modx->switchContext($dhToken->context);

		return $dhToken;
	}

}