<?php

namespace app;

class AccessByLogin
{
	public static function check($host, $token = null)
	{
		global $modx;

		if (!$dhToken = $modx->getObject('dhApiLogin', array('username' => $_SERVER['PHP_AUTH_USER'], 'password' => $_SERVER['PHP_AUTH_PW'], 'active' => 1))){
			throw new \Exception("Failed Authorization", 403);
		}
		
		$modx->switchContext($dhToken->context);

		return $dhToken;
	}

}