@extends('layouts.master')

@section('content')
    <div class="portfolio-page">
        <section class="page-heading">
            <div class="container-fluid">
                <div class="row">
                    <div class="banner-content">
                        <h2>{{$data['name']}}</h2>
                        <span><a href="/">Главная</a> / <a href="/news">Блог</a> / {{$data['name']}}</span>
                    </div>
                </div>
            </div>
        </section>

{{--        @dump($data)--}}

        <section class="blog-entries">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="blog-item single-post">
                                    @if(isset($data['video_url']))
                                        <div class="thumb" style="text-align: center;">
                                            <iframe src="{{$data['video_url']}}" class="videoframe"  frameborder="0" allowfullscreen></iframe>
                                            <img src="./Wellond - Multipurpose HTML Template_files/assets/gallery/1.jpg" alt="">
                                        </div>
                                    @else
                                        <div class="thumb" style="text-align: center;">
                                            @if(isset($data['image']))
                                                <img src="{{Croppa::url('/store_images/'.$data['image'], 750, 360,array('pad'))}}" alt="" class="img-responsive" style="margin: 0 auto;">
                                            @else
                                                <img src="/theme/gadget/images/not_image.png" alt="" class="img-responsive" style="margin: 0 auto;">
                                            @endif
                                        </div>
                                    @endif
                                    <h4>{{$data['name']}}</h4>
                                    <ul class="more-info">

                                        <li><a><i class="fa fa-calendar"></i>{{$data['created_at']}}</a></li>

                                    </ul>
                                    <p style="word-break:Normal;">{{$data['content']}}</p>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
