@extends('layouts.master')

@section('content')
    <div class="container" style="background: white;">

        <div class="container newss">
            <span><a href="/news" class="beauty_button">Все новости</a></span>
            <span><a href="/onlynews" class="beauty_button">Новостные статьи</a></span>
            <span><a href="/video" class="beauty_button">Видео</a></span>
        </div>


        @foreach($products->where('fields', null) as $item)



            <div class="block_with_news">
                <div class='content'>

                    <div class="col-xs-12 col-sm-4">
                        <div style="position:relative;height:0;padding-bottom:56.21%">
                            @if(isset($item->image))
                                <a href="/news/{{$item->id}}"><img
                                            src="{{Croppa::url('/store_images/'.$item->image, 210, 118,array('pad'))}}"
                                            style="position:absolute;width:100%;height:100%;left:0" width="641"
                                            height="360"></a>
                            @else
                                <a href="/news/{{$item->id}}"><img src="/theme/gadget/images/not_image.png"
                                                                   style="position:absolute;width:100%;height:100%;left:0"
                                                                   max-width="641" max-height="360"></a>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-8" style="text-align: left; padding-left: 20px;">
                        <a href="/news/{{$item->id}}" style="color: #333333 !important; text-decoration:none;"><h3
                                    style="font-size:20px; ">{{$item->name}}</h3></a><br>
                        <p class="content-text-news">{{$item->content}}</p><br>
                        <a href="/news/{{$item->id}}" class="button_info">Подробнее</a>
                    </div>
                </div>
            </div>



        @endforeach
    </div>

    <div class='form_otziv container' style="height: 800px;">
        <div class='content'>
            <div></div>

            <div class='form'>
                <div class='header' style="padding-top: 20px;">
                    Ваша мечта станет реальностью<br></span>
                </div>
                <div class='text' style="padding-top: 18px;">
                    Отправьте заявку на обучение</br>и мы свяжемся с Вами!
                </div>
                <div style='width: 100%;float:left;color:red' id='output'></div>
                <form method="POST" action="{{ url('/') }}">
                    <div class='input'>
                        <input type='text' value="{{ old('name') }}" placeholder='Ваше имя' required name='name'
                               id='name'/>
                    </div>
                    <div class='input'>
                        <input type='text' class='past_of' required value="{{ old('email') }}" placeholder='&nbsp;email'
                               name='email' id='email'/>
                        <input type='text' class='past_of required' value="{{ old('phone') }}"
                               placeholder='&nbsp;Ваш телефон' name='phone' id='phone'
                        />
                    </div>
                    <div class='input'>
                        <input type='text' value="{{ old('skype') }}" placeholder='Индивидуально, в группе, по Skype'
                               name='skype' id='desc'/>
                    </div>

                    <div class='input'>
                        <input type='text' value="{{ old('time') }}" placeholder='Утром, днем, вечером' name='time'
                               id='desc2'/>
                    </div>
                    <input type="hidden" name="blog" value="Из блога" class="inputs">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" class="inputs">
                    <div class='input'>
                        <input type='submit' value='Отправить заявку'/>
                        @include('flash')
                    </div>
                    <!--
                <div class='content' style='font-size: 10px;'> Оставляя заявку, вы принимаете соглашение <br/> "Об
                    обработке персональных данных"*.
                </div>
                -->
                </form>
            </div>

            <BR>
                <BR>

                    <div style="text-align: center;">
                        <a href="https://vk.com/club10557972"><img style="margin-top: 25px;"
                                                                   src="/theme/gadget/images/vk.png"
                                                                   class="vk_images"><a>
                                <div>

                                    <BR><BR>


                                </div>
                    </div>
                    <div class='footer'>
                        <div class='content' styule="margin-top: -2px;">
                            <div class='logo_block blocks_in_footer'>
                                <div class='logo'>
                                    СТУДИЯ РОНДО
                                </div>
                                <div class='logo_bottom'>
                                    УРОКИ ИГРЫ НА ГИТАРЕ
                                </div>
                            </div>
                            <div class='center blocks_in_footer' style="    line-height: 29px; margin-top: 10px;">
                                расскажите о нас друзьям
                                <script type="text/javascript">(function () {
                                        if (window.pluso) if (typeof window.pluso.start == "function") return;
                                        if (window.ifpluso == undefined) {
                                            window.ifpluso = 1;
                                            var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                                            s.type = 'text/javascript';
                                            s.charset = 'UTF-8';
                                            s.async = true;
                                            s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
                                            var h = d[g]('body')[0];
                                            h.appendChild(s);
                                        }
                                    })();</script>
                                <div class="pluso" data-background="#ebebeb"
                                     data-options="medium,square,line,horizontal,nocounter,theme=04"
                                     data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir,email,print"></div>

                            </div>
                            <div style="margin-top: -7px;" class='phone_block blocks_in_footer'>
                                <div class='phone' style="font-size: 29px;">
                                    8 (965) 765-25-00
                                </div>
                                <div class='time' style="font-size: 18px;">
                                    Пн - Пт: 13:00-21:00<br>
                                    Сб: 11:00-21:00<br>
                                    Воскр: 12:00-18:00
                                </div>

                            </div>
                        </div>


                    </div>

        </div>

    </div>
@endsection
