@extends('layouts.master')

@section('content')
<div class="portfolio-page">
    <section class="page-heading">
    <div class="container-fluid">
        <div class="row">
            <div class="banner-content">
                <h2>Видео</h2>
                <span><a href="/">Главная</a> / Видео</span>
            </div>
        </div>
    </div>
    </section>


    <section class="blog-entries">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                    @foreach($products->whereNotNull('video_url',500) as $item)
                        <div class="blog-item random-post">
                            <div class="row">
                            @if(isset($item->video_url))
                                <div class="col-md-5">
                                    <div class="thumb" style="text-align: center;">


                                  <iframe src="{{$item->video_url}}" class="videoframe videoframesmall"  frameborder="0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            @else
                                <div class="col-md-5">
                                    <div class="thumb" style="text-align: center;">
                                    @if(isset($item->image))
                                        <a href="/news/{{$item->id}}"><img src="{{Croppa::url('/store_images/'.$item->image, 457, 295,array('pad'))}}" alt=""></a>
                                    @else
                                        <a href="/news/{{$item->id}}"><img src="/theme/gadget/images/not_image.png" alt=""></a>
                                    @endif
                                    </div>
                                </div>
                            @endif
                                <div class="col-md-7">
                                    <div class="right-content">
                                        @if(isset($item->video_url))
                                            <div class="category">
                                                <a><span>Видео</span></a>
                                            </div>
                                        @endif
                                        <a href="/news/{{$item->id}}"><h4>{{$item->name}}</h4></a>
                                        <p style="overflow:hidden;">{{$item->content}}</p>
                                        <ul class="more-info">
                                            <li><a><i class="fa fa-calendar"></i>{{$item->created_at}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>



                </div>

                {{$products->links()}}

            </div>

        </div>
    </div>
    </section>
</div>
@endsection
