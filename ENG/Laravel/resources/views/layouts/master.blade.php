<!DOCTYPE html>
<html lang="ru">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon" href="/theme/gadget/images/WMm1zhPKoXQ.jpg" type="image/png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    {!! SEO::generate() !!}
{{--    <meta name="keywords" content="курсы,гитара,школа игры на гитаре"/>--}}
{{--    <meta name="description" content="Студия РОНДО школа игры на гитаре"/>--}}
{{--    <title>Уроки игры на гитаре для детей и взрослых</title>--}}
    <meta name="yandex-verification" content="fce1f1e45910601f"/>


    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lobster|Open+Sans+Condensed:300" rel="stylesheet">

    <link rel="stylesheet" href="/theme/gadget/css/bootstrap.css?v=2">
    <link rel="stylesheet" href="/theme/gadget/css/animate.css">
    <link rel="stylesheet" href="/theme/gadget/css/jquery-ui.css">
    <link rel="stylesheet" href="/theme/gadget/css/font-awesome.min.css">
    <link rel="stylesheet" href="/theme/gadget/css/light-box.css">
    <link rel="stylesheet" href="/theme/gadget/css/wellond.css?v=16">
    <link rel="stylesheet" href="/theme/gadget/css/blenda-script.css">
    <link rel="stylesheet" href="/theme/gadget/rs-plugin/css/settings.css">

    <style type="text/css">


    </style>


    <!-- Yandex.Metrika counter -->
    <script type="text/javascript"> (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter33827604 = new Ya.Metrika({
                        id: 33827604,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true,
                        trackHash: true
                    });
                } catch (e) {
                }
            });
            var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
                n.parentNode.insertBefore(s, n);
            };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";
            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks"); </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/33827604" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript> <!-- /Yandex.Metrika counter -->

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <![endif]-->

</head>

<body>

<div id="preloader" style="display: none;">
    <div id="status" style="display: none;">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
</div>


<div class="site-main" id="top">
    <div class="site-header" id="sTop">
        <div class="main-header">
            <div class="container-fluid">
                <div id="menu-wrapper">
                    <nav class="navbar navbar-inverse">
                        <div class="navbar-header">
                            <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse"
                                    data-target="#main-nav">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a href="/"><img class="hidden-xs" src="/img/logo2.png"
                                 style="float: left;width: 160px;margin-top: 13px;margin-right: -87px;"></a>
                            <span class="navbar-brand"><a href="/" style="color:white" class="hover-underline">Студия Рондо</a><br class="hidden-xs"><span class="hidden-xs">Уроки игры на гитаре</span><br><span
                                        style="font-size: 20px;"><a href="tel:+79657652500" style="color:white" class="hover-underline">8 (965) 765-25-00</a> Работаем ежедневно</span></span>
                            <div id="main-nav" class="collapse navbar-collapse">
                                <ul class="menu-first nav navbar-nav">
                                    <li class=""><a href="{{$main_page}}">Главная</a></li>
                                    <li class=""><a href="{{$news_page}}">Новости</a></li>
                                    <li><a href="{{$about_us}}">О нас</a></li>
                                    <li><a href="/news">Видео</a></li>
                                    <li><a href="{{$price}}">Цены</a></li>
                                    <li><a href="/gift">Подарочный сертификат</a></li>
                                    <li><a href="{{$contacts}}">Контакты</a></li>

                                </ul>
                            </div>
                            <!-- /.main-menu -->
                        </div>
                    </nav>
                </div>
                <!-- /#menu-wrapper -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.main-header -->
    </div>
    <!-- /.site-header -->
</div>
<!-- /.site-main -->


@section('content')

@show

<section class="contact-us {{ $hiddenForms }}">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="section-heading">
                    <h2 id="contact2" style="color: white;">Пусть Ваша мечта станет реальностью!</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <form method="POST" action="{{ url('/') }}">
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset>
                                <input name="name" value="{{ old('name') }}" type="text" class="form-control"
                                       placeholder="Ваше имя" required>
                            </fieldset>
                        </div>

                        <div class="col-md-6">
                            <fieldset>
                                <input name="phone" value="{{ old('phone') }}" type="text" class="form-control"
                                       placeholder="Ваш телефон" required>
                            </fieldset>
                        </div>
                        <div class="col-md-12">
                            <fieldset>
                                <input name="skype" value="{{ old('skype') }}" type="text" class="form-control"
                                       placeholder="Индивидуально, в группе, по Skype">
                            </fieldset>
                        </div>
                    <!--
                            <div class="col-md-12">
                                <fieldset>
                                    <input name="time" value="{{ old('time') }}" type="text" class="form-control"  placeholder="Утром, днем, вечером" >
                                </fieldset>
                            </div>
                            -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" class="inputs">
                        <div class="col-md-12">
                            <button class="white-button" id="submit-contact" type='submit'><span>Отправить заявку</span><span
                                        class="ease-in"></span></button>
                            @include('flash')
                        </div>
                    </div>
                </form>
                <br><br><br><br><br><br><br><br><br>
            </div>
            <div class="col-md-4">
                <a id="contact-us"></a>
                <div id="map">
                    <script type="text/javascript" charset="utf-8" async
                            src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Arlv_vRVk95MPh_SARj9XTWsQPHyUGxNi&amp;width=100%25&amp;height=292&amp;lang=ru_RU&amp;scroll=true"></script>
                </div>
            </div>
        </div>

    </div>
</section>


<div class="contact-info">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="info-item">
                    <div class="icon">
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                    </div>
                    <div class="down-content">
                        <h6>Время работы:</h6>
                        <ul>
                            <li>
                                <a>Пн - Пт: 13:00-21:00<br>
                                    Сб: 11:00-21:00<br>
                                    Воскр: 12:00-18:00</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="info-item">
                    <div class="icon">
                        <a href="tel:+79657652500"><i class="fa fa-phone"></i></a>
                    </div>
                    <div class="down-content">
                        <h6>Телефон:</h6>
                        <ul>
                            <li><a href="tel:+79657652500">8 (965) 765-25-00</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="info-item">
                    <div class="icon">
                        <a target="_blank"
                           href="https://yandex.ru/maps/2/saint-petersburg/?mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fll%3D30.378%252C60.046%26spn%3D0.001%252C0.001%26text%3D%25D0%25A0%25D0%25BE%25D1%2581%25D1%2581%25D0%25B8%25D1%258F%252C%2520%25D0%25A1%25D0%25B0%25D0%25BD%25D0%25BA%25D1%2582-%25D0%259F%25D0%25B5%25D1%2582%25D0%25B5%25D1%2580%25D0%25B1%25D1%2583%25D1%2580%25D0%25B3%252C%2520%25D0%25BF%25D1%2580%25D0%25BE%25D1%2581%25D0%25BF%25D0%25B5%25D0%25BA%25D1%2582%2520%25D0%259A%25D1%2583%25D0%25BB%25D1%258C%25D1%2582%25D1%2583%25D1%2580%25D1%258B%252C%252021%25D0%25BA1%2520&ll=30.377528%2C60.046014&z=16"><i
                                    class="fa fa-map-marker"></i></a>
                    </div>
                    <div class="down-content">
                        <h6>Адрес:</h6>
                        <ul>
                            <li><a target="_blank"
                                   href="https://yandex.ru/maps/2/saint-petersburg/?mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fll%3D30.378%252C60.046%26spn%3D0.001%252C0.001%26text%3D%25D0%25A0%25D0%25BE%25D1%2581%25D1%2581%25D0%25B8%25D1%258F%252C%2520%25D0%25A1%25D0%25B0%25D0%25BD%25D0%25BA%25D1%2582-%25D0%259F%25D0%25B5%25D1%2582%25D0%25B5%25D1%2580%25D0%25B1%25D1%2583%25D1%2580%25D0%25B3%252C%2520%25D0%25BF%25D1%2580%25D0%25BE%25D1%2581%25D0%25BF%25D0%25B5%25D0%25BA%25D1%2582%2520%25D0%259A%25D1%2583%25D0%25BB%25D1%258C%25D1%2582%25D1%2583%25D1%2580%25D1%258B%252C%252021%25D0%25BA1%2520&ll=30.377528%2C60.046014&z=16">Санкт-Петербург
                                    ст.м. «Гражданский пр.», «Озерки», «Пр.Просвещения»,
                                    «Девяткино», «Парнас» пр. Культуры д.21 к.1. Работаем ежедневно</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<footer>
    <div class="container">
        <div class="col-sm-6 text-left">
            <p>Copyright 2018 <em>Студия Рондо</em>. Все права защищены.</p>
            <small>* Скидка предоставляется при полном отсутствии пропусков занятий, пропуск не засчитывается только в
                случае болезни при наличии справки от врача.</small>
        </div>
        @if(!isset($hide_bottom_vk))
        <div class="col-sm-6" style="padding-top: 20px;
            padding-bottom: 20px; text-align: right;">
            <script type="text/javascript" src="//vk.com/js/api/openapi.js?154"></script>
            <!-- VK Widget -->
            <div id="bottom-vk" style=""></div>
            <script type="text/javascript">
                VK.Widgets.Group("bottom-vk", {mode: 1}, 10557972);
            </script>
        </div>
        @endif
    </div>
</footer>


<div class="back-to-top">
    <a href="#top">
        <i class="fa fa-angle-up"></i>
    </a>
</div>


<script src="/theme/gadget/js/jquery-1.11.1.min.js"></script>
<script src="/theme/gadget/js/bootstrap.min.js"></script>

<script src="/theme/gadget/js/owl.carousel.min.js"></script>
<script src="/theme/gadget/js/lightbox.js"></script>
<script src="/theme/gadget/js/isotope.js"></script>
<script src="/theme/gadget/js/imagesLoad.js"></script>
<script src="/theme/gadget/js/custom.js?v=2"></script>


</body>
<script>
    $(function () {
        var location = window.location.href;
        var cur_url = '/' + location.split('/').pop();

        $('.menu-first li').each(function () {
            var link = $(this).find('a').attr('href');
            if (cur_url == link) {
                $(this).addClass('active');

            }
        });

        $(".menu-first li").click(function () {
            $('#main-nav').collapse('hide');
        });

    });

</script>
<!--
<script type="text/javascript" src="http://new.evrasia-ex.ru/assets/js/jquery-1.7.2.js"></script>
-->
<!--
<script src="http://new.evrasia-ex.ru/assets/js/snowflakes.js"></script>
<script type="text/javascript">
    let aaSnowConfig = {snowflakes: "100"};
</script>
<style>
    #snowflakesCanvas {
        position: fixed;
        top: 0px;
        height: 100%;
        pointer-events: none;
    }
</style>
-->
</html>

