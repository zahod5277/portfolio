# Supporting of little personal site on Laravel

Summer 2021

### Tasks
1) Need to create a detail news page, which can be reachable on links like sitename.com/news/{$id}

2) All the sites pages have the same titles & descriptions. All seo tags must be unique.

### Solution

This is my method of getting each news detail
```php
public function news_item(Request $request, $id)
    {
        if (!$news_item = Product::find($id)){
            abort(404);
        }
        $model['data'] = $news_item->getAttributes();
        SEO::setTitle($model['data']['name']);
        if (!empty($model['data']['description'])){
            SEO::setDescription($model['data']['description']);
        } else {
            SEO::setDescription($model['data']['name']);
        }

        $main_page = '/#sTop';
        $about_us = '/#services';
        $podarok = '/gift';
        $price = '/#pricing';
        $contacts = '#contact-us';
        $news_page = 'vknews';
        $hiddenForms = '';

        return view("news_item", ['data' => $model['data']], compact('main_page', 'news_page', 'about_us', 'podarok', 'price', 'contacts', 'hiddenForms'));
    }
```

Add a route

```php
Route::get('/news/{id}', 'App\Http\Controllers\GadgetController@news_item');
```

It works!

For SEO tags "SEO Module For Laravel" was used 

```php
// app/Http/Controllers/GadgetController
use SEO;
```
```php
SEO::setTitle($title, false);
SEO::setDescription($description);
```

```php
return App::make('App\Http\Controllers\GadgetController')->controllerMethodName($title, $description);
```

Layouts (master.blade.php)

```php
{!! SEO::generate() !!}
```

**This task was done in 2 hours** 