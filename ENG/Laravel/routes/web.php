<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#Route::get('/', function () {  return view('welcome'); });
Route::get('/', function() {
    $title = 'Студия РОНДО школа игры на гитаре';
    $description = 'Студия РОНДО школа игры на гитаре';
    return App::make('App\Http\Controllers\GadgetController')->index($title, $description);
});
Route::post('/', 'App\Http\Controllers\GadgetController@message');


//Route::get('/news', 'App\Http\Controllers\GadgetController@news');
Route::get('/news', function(Request $request) {
    $input = $request->all();
    $title = 'Видео наших учеников - уроки игры на гитаре Студия Рондо';
    $description = 'Посмотрите успехи наших учеников, которых они достигли всего за несколько занятий!';
    return App::make('App\Http\Controllers\GadgetController')->news($request, $title, $description, $input);
});

Route::get('/vknews', function(Request $request) {
    $title = 'Новости нашей школы - уроки игры на гитаре Студия Рондо';
    $description = 'Новости и события из жизни школы игры на гитаре Рондо';
    return App::make('App\Http\Controllers\GadgetController')->vknews($request, $title, $description);
});

Route::get('/video', function(Request $request) {
    $title = 'Видео достижений наших учеников - уроки игры на гитаре Студия Рондо';
    $description = 'Видео-портфолио учеников, проходящих обучение игре на гитаре в студии Рондо';
    return App::make('App\Http\Controllers\GadgetController')->video($request, $title, $description);
});

//Route::get(, );
Route::get('/gallery', function() {
    $title = 'Фотографии занятий, сертификаты и награды школы игры на гитаре Студия Рондо';
    $description = 'Здесь представлены некоторые моменты из жизни школы Рондо Студия';
    return App::make('App\Http\Controllers\GadgetController')->gallery($title, $description);
});

//обрабатывается автоматически по описанию видео
Route::get('/news/{id}', 'App\Http\Controllers\GadgetController@news_item');

Route::get('/gift', function() {
    $title = 'Подарочный cертификат обучению игре на гитаре купить в подарок';
    $description = 'Порадуйте любимых и близких - подарите им сертификат на обучение игре на гитаре в студии Рондо!';
    return App::make('App\Http\Controllers\GadgetController')->ensemble($title, $description);
});

Route::get('/ensemble', function() {
    $title = 'Игра в ансамбле (группе, рок-группе) с наставником в СПб';
    $description = 'Игра в группе, ансамбле с наставником - это просто. Запишитесь на занятия в школу Рондо!';
    return App::make('App\Http\Controllers\GadgetController')->gift($title, $description);
});

Route::get('/contacts', function() {
    $title = 'Школа игры на гитаре в СПб контакты, адрес - студия Ронда';
    $description = 'Наши контакты 8 (965) 765-25-00, пр-кт. Культуры 21 к.1';
    return App::make('App\Http\Controllers\GadgetController')->contacts($title, $description);
});

//служебные маршруты, не обрабатываются
Route::get('/spasibo', 'App\Http\Controllers\GadgetController@spasibo');
Route::get('/solotony', ['uses'=>'App\Http\Controllers\FrontController@solotony', 'as'=>'solotony']);
Route::get('/cmsmagazinec4639f3b000df051ff472e1a11bfb147.txt', ['uses'=>'App\Http\Controllers\FrontController@cmsmagazine', 'as'=>'cmsmagazine']);
//Route::get('/sitemap.xml', ['uses'=>'App\Http\Controllers\FrontController@sitemap', 'as'=>'sitemap']);
Route::get('/yandex_.html', ['uses'=>'App\Http\Controllers\FrontController@yandex_webmaster', 'as'=>'yandex_webmaster']);
Route::get('/yandex_6790860f24bfeae3.html', ['uses'=>'App\Http\Controllers\FrontController@yandex_connect', 'as'=>'yandex_connect']);
Route::get('/google.html', ['uses'=>'App\Http\Controllers\FrontController@google_search_console', 'as'=>'google_search_console']);
Route::get('/clear-cache', 'App\Http\Controllers\GadgetController@clear_cache');