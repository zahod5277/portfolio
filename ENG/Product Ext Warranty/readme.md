# Extended warranty for device on MODX & 1С 

Date of creation - october 2020

### Task
Company presents new service "extended warranty". Customer has a right to repair his device after base warranty period is over.
This service can be bought in basket on online store as a separate item in cart. 

"Extended warranty" is not a real product, it's just a row in database, contains device IMEI-code and order number.

#### Restrictions
1) "Extended warranty" service must be activated in 40 days since purchase date or this service will be canceled without refund.
2) Personal customer data must be encrypted.
 

#### Activation
Activation process can be described like entering device IMEI-code in special field in account page on company web site.
Device IMEI-code is unknown in moment of purchase, it will shown after 2 days sicne purchase date, when device will be packed to delivery.
Therefore, customer should receive device IMEI-code after purchase by e-mail or SMS message.
This message must contain IMEI-code, Order num and special link to activation.  
Developer must predict some conflict situations, like when customer buys 2 same devices with only one "extended warranty service". 

### Analysis
This module is a API exchange between MODX web-site and Company ERP based on russian software named "1C Commerce". 

List of methods:
* Create order with "extended warranty" service in database
* Send this data to register on 1C
* Get a device IMEI when it will be known.
* Send email to customer with IMEI & activation link.
* Activation on web-site (check imei & order num, set flag "activated" in database)
* Send to 1C this activation status (success or error)
* Error handler
* Encrypt/decrypt data package

### Solution
 
You can see my solution in [**superwarranty/model/superwarranty.class.php**](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Product%20Ext%20Warranty/superwarranty/model/superwarranty.class.php)


 Interested things:
 * I have choosed JWT as an API message standard. JWT can be encrypted, have a clear structure, easy to create & read.  
 * AES-128 as a encrypted method (link)
 * CRON-script - secondnotification.php. It fires once per day and check all orders where warranty still not active. Notification email to all this orders will be sent.  
 * Encrypt/decrypt described like a public function because i often use in outside of project for my own purposes. In good way it will be "private" of course.  