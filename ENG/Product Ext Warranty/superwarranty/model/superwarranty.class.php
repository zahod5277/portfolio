<?php

class superWarranty
{
    /** @var modX $modx */
    public $modx;
    public $pdo;
    public $miniShop2;

    /**
     * @param modX $modx
     * @param array $config
     */
    function __construct(modX &$modx, array $config = [])
    {
        $this->modx = &$modx;
        $corePath = MODX_CORE_PATH . 'components/superwarranty/';
        $assetsUrl = MODX_ASSETS_URL . 'components/superwarranty/';

        $this->config = array_merge([
            'corePath' => $corePath,
            'modelPath' => $corePath . 'model/',
            'processorsPath' => $corePath . 'processors/',
            'actionUrl' => $assetsUrl . 'action.php',
            'connectorUrl' => $assetsUrl . 'connector.php',
            'assetsUrl' => $assetsUrl,
            'cssUrl' => $assetsUrl . 'css/',
            'jsUrl' => $assetsUrl . 'js/',
            'scriptJS' => $assetsUrl . 'js/web/default.js',
            'url' => $this->modx->getOption('superwarranty_url'),
            'secret' => $this->modx->getOption('superwarranty_secret'),
            'iv' => $this->modx->getOption('superwarranty_iv'),
            'alg' => 'aes-256-cbc',
            'shop_id' => $this->modx->getOption('superwarranty_shop_id'),
            'imei_email_tpl' => $this->modx->getOption('superwarranty_warranty_activation_imei_email'),
            'activation_success_email_tpl' => $this->modx->getOption('superwarranty_activation_success_email'),
            'activation_error_email_tpl' => $this->modx->getOption('superwarranty_activation_error_email'),
        ], $config);
        $this->pdo = $this->modx->getService('pdoTools');
        $this->miniShop2 = $this->modx->getService('miniShop2');
        $this->modx->addPackage('superwarranty', $this->config['modelPath']);
        $this->modx->lexicon->load('superwarranty:default');
    }


    public function loadJs($objectName = 'superWarranty')
    {
        if ($js = trim($this->config['scriptJS'])) {
            if (preg_match('/\.js/i', $js)) {
                $this->modx->regClientScript(str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $js));
            }
        }
        $config = $this->modx->toJSON(array(
            'assetsUrl' => $this->config['assetsUrl'],
            'actionUrl' => str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $this->config['actionUrl'])
        ));
        $objectName = trim($objectName);
        $this->modx->regClientScript(
            "<script type=\"text/javascript\">{$objectName}.initialize({$config});</script>", true
        );
    }

    //receive data from remote 1C server
    public function receive($request)
    {
        if ($this->checkSign($request)) {
            $from_json = json_decode(base64_decode($request), true);
            $data = json_decode($this->decrypt($from_json), 1);
        } else {
            return ['status' => 'error', 'message' => 'message is incorrect!', 'data' => $request];
        }
        switch ($data['method']) {
            case 'sendIMEI':
                $res = $this->fillImei($data['orders'][0]);
                break;
            case 'linkWarrantyIMEI':
                $this->setWarrantyActive($data);
                $res = $this->notificateUser($data);
            default:
                break;
        }
        return json_encode($res);
    }

    //receive AJAX data from my own site
    public function receiveAJAX($request)
    {
        $params = [];
        parse_str($request['params'], $params);
        switch ($request['method']) {
            case 'activateWarranty':
                $res = $this->activateIMEI($params);
                break;
            default:
                break;
        }
        return json_encode($res);
    }

    public function sendWarranties($order_id, $method = '')
    {
        $this->clearLog();
        $payload = $this->collectPayload($order_id);
        if (empty($payload)) {
            return false;
        }
        $payload['method'] = $method ? $method : 'VerifyOrder';
        $request_status = $this->request($payload);

        if ($request_status != 0) {
            $this->beautyBLog('Request error! cURL error code: ' . $request_status);
            return false;
        } else {
            $this->beautyBLog('Request success!!! cURL code is 0, that means OK');
            return true;
        }

    }

    public function activateIMEI($data)
    {
        $guid = $data['guid'];
        $imei = $data['imei'];

        if ($warranty = $this->modx->getObject('superWarrantyItem', [
            'GUID' => $guid
        ])) {
            if ($imei_item = $this->modx->getObject('superWarrantyImei', [
                'imei' => $imei
            ])) {
                if ($imei_item->get('product_id') == $warranty->get('product_id')) {
                    if ($warranty->get('active_1c') != 1) {
                        if ($warranty->get('blocked') == 1) {
                            $res = [
                                'status' => 'error',
                                'message' => 'Указанный код гарантии заблокирован. Истёк срок регистрации. Обратитесь в службу поддержки'
                            ];
                            return $res;
                        }
                        $warranty->fromArray([
                            'active' => 1,
                            'activation_date' => date('Y-m-d\TH:i:s') . 'Z',
                            'imei_id' => $imei_item->get('id')
                        ]);
                        $warranty->save();
                        $status = $this->sendWarranties($warranty->get('order_id'), 'linkWarrantyIMEI');
                        if ($status) {
                            $res = [
                                'status' => 'success',
                                'message' => 'Указанный IMEI отправлен в систему регистрации гарантий. Вы получите подтверждение по почте'
                            ];
                        } else {
                            $res = [
                                'status' => 'error',
                                'message' => 'Неожиданная ошибка. Обратитесь к службе поддержке сайта'
                            ];
                        }
                    }
                } else {
                    $res = [
                        'status' => 'error',
                        'message' => 'GUID гарантии указан неверно. Проверьте правильность ввода'
                    ];
                }
                return $res;
            }
        }
    }

    public function fillImei($data)
    {
        $order_id = intval(str_replace(' ', '', $data['ID']));
        $order = $this->modx->getObject('msOrder', $order_id);
        $this->modx->switchContext($order->get('context'));
        $positions = $data['positions'];
        $imei_arr = [];
        foreach ($positions as $position) {
            foreach ($position['properties'][0]['imei'] as $imei) {
                if (!$product_imei_link = $this->modx->getObject('superWarrantyImei', [
                    'imei' => $imei
                ])) {
                    if ($product = $this->modx->getObject('msProductData', [
                        'article' => $position['product_sku_code']
                    ])) {
                        $where = [
                            'order_id' => $order_id,
                            'product_id' => $product->get('id'),
                            'imei' => $imei
                        ];
                        $product_imei_link = $this->modx->newObject('superWarrantyImei', $where);
                        $product_imei_link->save();
                        $imei_arr[] = [
                            'link_id' => $product_imei_link->get('id'),
                            'imei' => $imei
                        ];
                    }
                }
            }
        }
        if (!empty($imei_arr)) {
            $this->sendUserEmail($order_id);
        }
        return $imei_arr;
    }

    public function setWarrantyActive($data)
    {
        $status = $data['status'];
        $warranties = $data['data']['order']['positions'];
        foreach ($warranties as $warranty) {
            $warranty_item = $this->modx->getObject('superWarrantyItem', [
                'guid' => $warranty['warranty'][0]['GUID']
            ]);
            if ($warranty['warranty'][0]['properties'] == true && $status == 'false') {
                $warranty_item->set('blocked', 1);
                $warranty_item->save();
            } elseif ($warranty['warranty'][0]['properties'] == true && $status == 'true') {
                $warranty_item->set('active_1c', 1);
                $warranty_item->save();
            }
        }
        return true;
    }

    //Повторная отправка IMEI кода с напоминанием о том, что нужно активировать гарантию
    public function secondNotification()
    {
        $interval = '-1 month';
        $from = date('Y-m-d', strtotime($interval));

        $query = $this->modx->newQuery('msOrder');
        $query->where(
            array(
                'createdon BETWEEN "' . date('Y-m-d', strtotime("-1 month")) .
                '" AND  "'.date('Y-m-d', strtotime("-2 week")).'"'
            ));
        if ($query->prepare() && $query->stmt->execute()) {
            $all_orders_period = $query->stmt->fetchAll(PDO::FETCH_ASSOC);
        }

        $count = 0;
        foreach ($all_orders_period as $o){
            $imeis =  $this->modx->getIterator('superWarrantyImei',[
                'order_id' => $o['msOrder_id']
            ]);
            foreach ($imeis as $imei){
                if (!$warranty_active = $this->modx->getObject('superWarrantyItem',[
                    'imei_id' => $imei->get('id')
                ])){
                    $orders[] = $o['msOrder_id'];
                }
            }
        }

        $orders = array_unique($orders);

        $chunk = $this->config['imei_second_email_tpl'];
        foreach ($orders as $order){
            $this->sendUserEmail($order,$chunk);
        }

    }

    public function notificateUser($data)
    {
        $this->modx->switchContext($data['data']['order']['context']);
        $user = $this->modx->getObject('modUserProfile', $data['data']['order']['user_id']);
        if ($data['status'] == 'true') {
            $chunk = $this->pdo->getChunk($this->config['activation_success_email_tpl']);
            $subject = 'Услуга «Продление гарантии» активирована!';
        } else {
            $chunk = $this->pdo->getChunk($this->config['activation_error_email_tpl']);
            $subject = 'Ошибка активации услуги «Продление гарантии»';
        }
        $this->miniShop2->sendEmail($user->get('email'), $subject, $chunk);
        return true;
    }

    public function sendUserEmail($order_id)
    {
        $links = [];
        $warranty_items = $this->modx->getIterator('superWarrantyItem', [
            'order_id' => $order_id
        ]);
        foreach ($warranty_items as $warranty_item) {
            if ($warranty_item_imei = $this->modx->getObject('superWarrantyImei', [
                'order_id' => $order_id,
                'product_id' => $warranty_item->get('product_id')
            ])) {
                if ($product = $this->modx->getObject('modResource', $warranty_item->get('product_id'))) {
                    $links[] = [
                        'guid' => $warranty_item->get('guid'),
                        'name' => $product->get('pagetitle')
                    ];
                }
            }
        }

        $order = $this->modx->getObject('msOrder', $order_id);
        $user = $this->modx->getObject('modUserProfile', $order->get('user_id'));
        $this->modx->switchContext($order->get('context'));
        $chunk = $this->pdo->getChunk($this->config['imei_email_tpl'], [
            'links' => $links,
            'order' => $order->toArray(),
        ]);

        $this->miniShop2->sendEmail($user->get('email'), 'Активация услуги «Продление гарантии»', $chunk);
        return true;
    }

    public function request($payload)
    {
        $header = [
            'alg' => 'HS256',
            'typ' => 'JWT',
        ];

        $encoded = [
            'header' => base64_encode(json_encode($header)),
            'payload' => json_encode($payload, JSON_UNESCAPED_UNICODE),
        ];

        $message = $encoded;
        $post_data = json_encode($message, JSON_UNESCAPED_UNICODE);
        $ch = curl_init($this->config['url']);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

//         Execute the POST request
        $result = curl_exec($ch);
        $status = curl_errno($ch);

        // Close cURL resource
        curl_close($ch);
        return $status;
    }

    public function buildJWTSignature($data)
    {
        $source_signature = ($data['header']) . '.' . ($data['payload']);
        $signature = base64_encode(mb_strtoupper(hash_hmac('sha256', $source_signature, $this->config['secret'])));
        return $signature;
    }

    public function checkSign($input)
    {
        $input = json_decode(base64_decode($input), true);

        $source_signature = ($input['header']) . '.' . ($input['payload']);
        $signature = base64_encode(mb_strtoupper(hash_hmac('sha256', $source_signature, $this->config['secret'])));

        if ($signature === $input['signature']) {
            return true;
        } else {
            return false;
        }
    }

    //AES-128 Encryption
    public function encrypt($data)
    {
        $encryption_key = base64_decode($this->config['secret']);
        $iv = base64_decode($this->config['iv']);

        $encrypted = openssl_encrypt($data, $this->config['alg'], $encryption_key, 0, $iv);
        return $encrypted;
    }

    //AES-128 Decryption
    public function decrypt($data)
    {
        $encryption_key = base64_decode($this->config['secret']);
        $iv = base64_decode($this->config['iv']);
        $payload = $data['payload'];

        $decrypted = openssl_decrypt($payload, $this->config['alg'], $encryption_key, 0, $iv);
        return $decrypted;
    }

    /**
     * @param $order_id int
     * @return array
     */
    public function collectPayload($order_id)
    {
        $order = $this->modx->getObject('msOrder', $order_id);
        $user_data = array_merge($order->getOne('UserProfile')->toArray(), $order->getOne('User')->toArray());

        $order_products = $order->getMany('Products');

        $adress = $order->getOne('Address');
        $a = $adress->toArray();

        $reciever = explode(' ', $a['receiver']);
        $summands_w = round($order->cost * 0.2);
        $o = [
            'id' => "{$order->id}",
            'id_guid' => sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535)),
            'NumberOrder' => "{$order->id}",
            'customerFirstName' => $reciever[0],
            'customerLastName' => $reciever[1],
            'summasnds' => "{$order->cost}",
            'summands' => "{$summands_w}"
        ];

        unset($o['properties']);

        $order_items = [];
        foreach ($order_products as $order_product) {
            $order_product_options = $order_product->get('options');
            if (isset($order_product_options['AppleCare'])) {
                $warranty_product = $order_product->getOne('Product');
                $wp_arr = $warranty_product->toArray();

                if ($warranty = $this->modx->getObject('msProductData', $order_product_options['AppleCare'])) {

                    $warranty_items = $this->modx->getIterator('superWarrantyItem', [
                        'warranty_id' => $warranty->get('id'),
                        'order_id' => $order_id
                    ]);

                    foreach ($warranty_items as $wi) {
                        $ndsAppCare = round($warranty->get('price') * 0.2);
                        $ndsAppCares = round($warranty->get('price') * 0.2);
                        $summands = round(($order_product->get('price') * 0.2));
                        $warranty_arr = [
                            'name' => $warranty_product->get('pagetitle'),
                            'code_device' => $warranty_product->get('article'),
                            'deviceId' => $wp_arr['serial_number.value'] ? $wp_arr['serial_number.value'] : 'none',
                            'rate_NDSAppCare' => "20"
                        ];

                        $warranties[] = $warranty_arr;

                    }
                }
                $order_items = $warranties;
            }
        }
        $o['devices'] = $order_items;
        $data = $o;

        return $data;
    }

    private function beautyBLog($msg)
    {
        $page = $this->modx->getObject('modResource', 7);
        $page_content = $page->get('content');
        $log_item = date('d.m.Y H:i:s') . ' ' . $msg . ' <hr> ';
        $page->set('content', $page_content . $log_item);
        return $page->save();

    }

    private function clearLog()
    {
        $page = $this->modx->getObject('modResource', 7);
        $page->set('content', '');
        return $page->save();
    }

}