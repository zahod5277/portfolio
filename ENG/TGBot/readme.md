## TG Bot for 3UI Vpn

### My personal pet project, created while recovering from a broken leg

**--> [THE CODE](https://bitbucket.org/zahod5277/vpnbot/src/master/) <--**

Stack
+ PHP 8

+ Nutgram

+ Slim 4

+ PHPUnit

+ MySQL

+ Docker

The code is pretty shitty, because it never should be published, but you can check the tests. I like to write a tests.

I was really interested in writing my own telegram bot, plus since March 2022 I have been running various VPN
servers to provide my homies with internet access outside Cheburnet.

At some point I decided that I could try to monetize it. I found Nutgram, found a [ready-made Docker image for a VPN server](https://github.com/MHSanaei/3x-ui), discovered
that it has a very tasty API and got to work.

I didn't try to make the code clean and beautiful - the database queries are compiled very stupidly, the API methods can also be optimized,
all this was done for fun and to test the hypothesis of whether it is possible to make money on this.

In general, of course, it is possible. But it is very difficult to maintain a VPN server without having any idea how to work with networks in Linux.
In order to launch this product and make it work well in the Russian Federation with minimal speed losses, I had to:

+ Rent two VPSs from DigitalOcean. One for the TG bot, the second for X-UI itself 
+ Register domains and get Let's Encrypt for them using CertBot 
+ Forward X-UI domains and ports through CloudFlare 
+ Use a third-party utility to listen to the IP of the server with X-UI for suitable sites that can be used to disguise my VPN 
+ Configure X-UI itself 
+ Launch nginx on a VPS with PHP to receive web hooks from Telegram and make a web interface for my VPN 
+ Configure Slim4 to work in test/prod modes and send requests to the desired panel (dev/test), receive responses and give them to the user in Telegram 
+ Configure the daemon to work in web polling mode, because cart banned webhooks due to frequent requests, use Supervisord 
+ Then I redid the webhooks, added the IP to the firewall whitelist, it seemed to work 
+ Plus writing instructions for users, support, answering questions and other bullshit. 
+ In short, this is a full-time job and it's quite a hassle to make some easy money here if you don't devote your entire working day to it.
