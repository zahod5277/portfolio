# Dmitry Redkin. PHP-developer.
## Team-lead, Php senior, Fullstack developer

## Hard Skills 

* PHP 5/7/8, PHPUnit, Codeception, Slim, Bitrix, MODX, MySQL, Laravel
* Kafka, Docker, Linux (CentOS, Ubuntu)
* React, TypeScript
* HTML+JS+CSS, Gulp, Pug, SCSS
* Node.js
* GIT, Gitlab, Bitbucket, Gerrit
* PHPStorm
* Team leading, tech writing, task & time management

## Achievements
* I've been creating awesome web products since 2015.
* I've worked in remote teams since 2016.
* I’ve built over 100 websites, optimized thousands of lines of code, and saved money for clients worldwide.
* I’ve received recognition from companies like Nokia, Apple, and NVIDIA.
* I led a development team that created an impressive PHP data sync app.
* I can communicate effectively with business stakeholders.

[My personal web site](http://phpslave.ru/)  

Check my experience below

#### 2024

**My own pet-project - Telegram Bot for X-UI VPN**

[LINK](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/TGBot/)

**Test case for Sterc (Netherlands) on Fullstack Developer (VUE + PHP)**

[LINK](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Sterc/)

### 2023

**Orion Senior Software Engineer**

[TEST CASE](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/orion/)

* Passed the test case and received a job offer.
* All other work is protected by NDA.
* Main product: A U.S. TV-oriented system developed using PHP and React. 

### 2022

**diHouseExchange** 

[DEMO SOURCE](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Api/)

REST API between 1С ERP and 1C Bitrix (russian ERP & CMS systems). 

Stack: PHP 7.4, Slim 4, Bitrix, PHPUnit, Docker, Kafka

Role: architect, team lead, lead developer.

The whole project was projected by myself. I`ve developed JSON API & class structure, configured Kafka, teached all my team write a tests and use a Docker.
I have written a tech doc, and presented my project to business. 

**miEco**

1С Bitrix Core optimization for rapid web-site developing.

Stack: PHP 7.4, 1C Bitrix, JavaScript, Gulp, SCSS, PUG

Role: architect, team lead, developer.

I invented some technical tricks which can assist to develop a complex e-commerce site in 3-5 day. 

What exactly has been done:
* Installed TWIG as a template engine instead HTML+PHP spaghetti. Easy to read - easy to write! 
* Developed Gulp project with strong structure, also the code style has been written. Easy to write - easy to support!
* Developed special module "Rich Content": visual block editor with custom HTML-chunks. All you need is to fill TWIG-template & connect it in module. Then you can mix & create any blocks which was installed.    

### 2021

**Start a team lead career**

- I have optimized workspaces: buy PHPStorm, also configured TEST & PROD servers on CentOS и Ubuntu.
- Configured a special Docker project for test servers [take a look on my BitrixDock here. With russian manual inside](https://github.com/dihouse-redkin/bitrixdock)
- Installed Gitlab on company cloud & configured it. Also i have written a GIT manual for all team members & made a presentation "GIT - what the fuck is this?"  
- Written a tech rules & code styles about front-end & back-end developing
- Job interview with Senior, Middle и Junior developers
- Task management 
- A lot of talks in Zoom, Bitrix, Cisco Webex, Skype, etc. Sometimes in English (but not so good)

### 2020 - 2017

### MODX

- [Arny Praht](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Arny%20Praht/). 
    * Awesome [e-commerce site](https://arnypraht.com/) with annual turnover over 3 000 000 $. 
    * Check readme. 
- [Product Ext Warranty](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Product%20Ext%20Warranty/) - my custom module which allow to buy digital services with real devices. 
    MODX API + 1C УТ API + JWT + AES 128
- [Qustick.ru](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Qustick/) - My personal pet-project, marketplace for musicians.     
- [milanaCRM](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/milanaCRM/) - micro-CRM which was ordered by clothes factory from Moscow

### 1C Bitrix

- [Official russian web site of Nokia Mobile](https://mobileshop.nokia.ru/)  
- [BitrixMultiprop](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/BitrixMultiprop/) - another module for 1C Bitrix, which allow to work with multiply fields of infoblocks.

### Laravel

- [Laravel](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Laravel/) - my little experience with Laravel, mini web-site [Rguitar.ru](https://rguitar.ru/)

### Slim

- [Slim](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Slim/) - REST API for Nokia & NVIDIA on SLIM 2. This API returns orders info, products info, etc. 


### PHP NATIVE

- [sknt-test-case](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/sknt-test-case/) - Test job case for one of the most largest Russian telecom company "Skynet" (yeah, like Terminator, it's true). This task was successfully done with work offer after.  
- [smetter-test-case](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/smetter-test-case/) - Test job case for Smetter.ru (successfully done). 

### Other

- [scripts](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/scripts/) - some fine scripts on JS, Node.js, jQuery, PHP

### Info

- [docs/skills.md](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/my_skills.md) - My skills
 



> P.S. Holy shit, i'm so skilled!