# sknt-test-case
## Test case for Middle PHP-developer vacancy in "Skynet" - lead telecom company in Russia 

### Successfully done with job offer in september 03 2019

Task link (you can translate it with Google Translate in browser): (https://www.sknt.ru/job/frontend/)[https://www.sknt.ru/job/frontend/]

**What this single page can do**

- Route queries, include ajax ([**core/Controller.php**](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/sknt-test-case/core/Controller.php))

- Template engine Fenom. ([core/Templates/*](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/sknt-test-case/core/Templates/))

- JSON handler class ([core/Json_parser.php](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/sknt-test-case/core/Json_parser.php))

- BEM methodology for HTML & Gulp for build the app.

- App class, which can work with many states [**app/scripts/app.js**](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/sknt-test-case/app/scripts/app.js)


**Just put files to php env and go to index.php in browser.** 
