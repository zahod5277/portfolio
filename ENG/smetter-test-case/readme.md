# Test case for Smetter.ru (successfully done)

### Date of creation january 23 2020

## Task
Simple plane seat booking module with pure PHP.
It has to be done as single page app.
This page must contain:

1.    Departure city, destination city, flight date.

2.    Plane seat schema with booked seats

Restrictions:

1.    Bootstrap 3/4.

2.    PHP frameworks is forbidden!

3.    FULL OOP. 

## Solution
I was developed my own micro-framework with PSR standards.

Framework has a:
- Composer Autoload
- MVC - model, view, controller + route and MySQLi handler class.
- Models & controllers can be extended
- Routing system is very simple, but i think it's enough for this task.
- View works with my favourite template engine Fenom. All Views stores in **app/templates/**

### Interested things
All this code was written by me. This code doesn't contains any copy/paste from anywhere.

Model have just 2 methods **getCollection** and **getObject**. Supports **JOIN** and **WHERE**.

All controllers can be extended as you want. No limits. 

Frontend works with Gulp - SCSS, JS compile to bundle.

All frontend ruled by App class: data validation, send/receive AJAX.

### How to install this miracle?
Copy to PHP env. Start point - index.php, of course.
I used to start it on Homestead from Laravel.
Next step is import database dump to MySQL and set config in **app/App.php**. 

Working demo stand here:
[http://zahod5277.beget.tech/](http://zahod5277.beget.tech/)  
