# HARD Skills

## PHP 5.3,5.6,7.x  
## I can work with a new code and support a lot of legacy
## I can read the code of any quailty include shitcode
## PHP NATIVE 5.3 - 7.4, MODX, BITRIX, OPENCART, WORDPRESS, LARAVEL (a little bit)


* OOP, procedure programming, see the difference between
* Databases (Mysqli, PDO, XPDO, Eloquent, Bitrix ORM)
* PHPUnit
* API projecting & developing, work with 1С API, SOAP, REST, JWT, XML, JSON
* CMS/CMF: Bitrix, MODX Evo/Revo, Opencart, Wordpress, etc
* Laravel/Slim (a little bit)
* MVC
* Code refactoring
* I see the difference between shitcode, a normal code & code which written by god
* I REALLY LIKE TEMPLATE ENGINES (Fenom, Blade, TWIG)

## Databases & MySQL
* ER
* DB normalization  / denormalization  
* Simple queries
* Easy join queries (LEFT, RIGHT, INNER)
* Subqueries
* Base theory of DB,  datatypes, indexes, join's, methods of DB optimizing

## Linux
###
* Ubuntu, Mint, CentOS
* I can configure server on CentOS 7 (but please don't ask about it)
* Can configure Gitlab, Wiki, Bitrix24 or any solution on your server with manual
* SSH (scripts, cron, mc, etc) 
* BASH


## JavaScript
1. jQuery (like a god)
2. Simple native JS without jQuery
3. Classes, arrow functions, base theory of ES2015. Need more practice. 
3. Node.js (I can build a little simple server on Node)

## HTML + CSS
* BEM methodology
* Mobile first, pixel perfect, no js, keyframe animation
* SCSS, PUG

## Other skills

1. VCS (Gitflow, Pull Requests, cherry pick, etc)
2. GIT (fetch, pull, push, commit, add, status, reflog, etc)
3. PHPStorm (so mad about it)
4. Gulp, WEBPACK
5. NPM, Composer

# SOFT Skills

* I can google like hell, search info on Stackoverflow, GitHub и YouTube
* Figma, Zeplin
* Excel (no problem with any MS Office product)
* Any task tracker. Trello, Planfix, Bitrix24, YouTrack, Asana, Jira
* Any other softwate. Video editing (i have youtube channel), image editing, graphic design, audio editing, math calc with Matlab, 3D modelling
* Responsibility is my primary advantage. I am always online in worktime. My tasks are always under control, i can lead meetings everyday and do reports.
* I can admit my own mistakes
* I can prove my point with links, examples, references
* I have experience on meetings with techlead of russian banks, Nokia, Apple, NVIDIA.
* I can present a product and defend it in front of bussiness
* My english you can read right now. I almost didn't used Google Translate when i wrote it.   
* I crazy about studying and can do it very fast.


# What kind of tasks i can do

* Rule a little team - task control, time control, code review 
* Creating a new bussiness product on company existing technology stack 
* Supporting existing products
* Base CI/CD
* Writing tests
* Building new API methods
* A lot more...