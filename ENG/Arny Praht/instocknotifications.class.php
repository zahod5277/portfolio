<?php

class inStockNotifications {

    /** @var modX $modx */
    public $modx;

    /**
     * @param modX $modx
     * @param array $config
     */
    function __construct(modX &$modx, array $config = []) {
        $this->modx = & $modx;
        $corePath = MODX_CORE_PATH . 'components/instocknotifications/';
        $assetsUrl = MODX_ASSETS_URL . 'components/instocknotifications/';

        $this->config = array_merge([
            'corePath' => $corePath,
            'modelPath' => $corePath . 'model/',
            'processorsPath' => $corePath . 'processors/',
            'connectorUrl' => $assetsUrl . 'connector.php',
            'assetsUrl' => $assetsUrl,
            'cssUrl' => $assetsUrl . 'css/',
            'jsUrl' => $assetsUrl . 'js/',
            'stockField' => $this->modx->getOption('instocknotifications_StockCountField'),
            'stockClass' => $this->modx->getOption('instocknotifications_StockCountClass'),
            'syncMSOP' => $this->modx->getOption('instocknotifications_SyncMsOptionsPrice')
                ], $config);

        $this->modx->addPackage('instocknotifications', $this->config['modelPath']);
        $this->modx->lexicon->load('instocknotifications:default');
    }

    public function process() {
        $pdo = $this->modx->getService('pdoTools');

        $q = $this->modx->newQuery('inStockNotificationsItem');
        $q->select('DISTINCT inStockNotificationsItem.product_id');
        $q->where([
            'active:=' => 1,
        ]);
        $q->prepare();
        $sql = $q->toSQL();
        //$this->modx->log(1,print_r($sql,1));
        $q->stmt->execute();
        $result = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        $ids = array();
        foreach ($result as $res) {
            $ids[] = $res['product_id'];
        }
        switch ($this->config['stockClass']) {
            case 'modResource':
                $products = $this->getRemainsByResourceField($ids);
                break;
            case 'modTemplateVar':
                $products = $this->getRemainsByTV($ids);
                break;
            case 'msProductOption':
                $products = $this->getRemainsByOptions($ids);
                break;
            case 'msProductData':
                $products = $this->getRemainsByProductData($ids);
                break;
        }

        //$this->modx->log(xPDO::LOG_LEVEL_ERROR,print_r($products,1));

        if ($this->config['syncMSOP']) {
            $syncProducts = $this->getModificationsRemains($ids);
            $products = array_merge($products, $syncProducts);
        }

        foreach ($products as $product) {
            $this->modx->log(xPDO::LOG_LEVEL_ERROR, print_r($product, 1));
            $notificationItems = $this->modx->getIterator('inStockNotificationsItem', array('product_id' => $product['id']));
            foreach ($notificationItems as $notificationItem){
                $email = $notificationItem->get('email');
                $subject = $this->modx->getOption('instocknotifications_NotificationEmailSubject', $this->modx->lexicon('instocknotifications_NotificationEmailSubject'));
                $mailTemplate = $this->modx->getOption('instocknotifications_NotificationEmailTpl', 'tpl.InStockNotifications.email.instock');
                $message = $pdo->getChunk($mailTemplate, $product);
                //$this->modx->log(xPDO::LOG_LEVEL_ERROR, 'MESSAGE: '. print_r($message, 1));
                $sended = date('Y-m-d H:i:s');
                $notificationItem->set('sended',$sended);
                $notificationItem->set('active',0);
                $notificationItem->save();
                $this->sendEmail($message, $subject, $email);
                //Задержка на всякий случай. С классом-счетчиком заморачиваться не стал
                sleep(5);
            }
        }
    }

    public function getRemainsByProductData($ids) {
        $query = $this->modx->newQuery('msProduct');
        $query->select(array(
            'msProduct' => $this->modx->getSelectColumns('msProduct', 'msProduct'),
            'msProductData' => $this->modx->getSelectColumns('msProductData', 'msProductData')
        ));
        $query->leftJoin('msProductData', 'msProductData', 'msProductData.id = msProduct.id');
        $query->where(array(
            'msProductData.id:IN' => $ids,
            'msProductData.' . $this->config['stockField'] . ':!=' => 0
        ));
        $query->prepare();
        $query->stmt->execute();
        $result = $query->stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getRemainsByResourceField($ids) {
        $query = $this->modx->newQuery('msProduct');
        $query->select(array(
            'msProduct' => $this->modx->getSelectColumns('msProduct', 'msProduct')
        ));
        $query->where(array(
            'msProduct.id:IN' => $ids,
            'msProduct.' . $this->config['stockField'] . ':!=' => 0
        ));
//        'msProduct.'.$this->config['stockField'].':!=' => 0
        $query->prepare();
        $query->stmt->execute();
        $result = $query->stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getModificationsRemains($ids) {
        $query = $this->modx->newQuery('msProduct');
        $query->select(array(
            'msProduct' => $this->modx->getSelectColumns('msProduct', 'msProduct'),
            'msProductData' => $this->modx->getSelectColumns('msopModification', 'msopModification')
        ));
        $query->leftJoin('msopModification', 'msopModification', 'msopModification.rid = msProduct.id');
        $query->where(array(
            'msopModification.rid:IN' => $ids,
            'msopModification.count:>' => 0
        ));
        $query->prepare();
        $query->stmt->execute();
        $result = $query->stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getRemainsByTV($ids) {
        $query = $this->modx->newQuery('msProduct');
        $tv = $this->modx->getObject('modTemplateVar', array('name' => $this->config['stockField']));
        $query->select(array(
            'msProduct' => $this->modx->getSelectColumns('msProduct', 'msProduct'),
            'modTemplateVarResource' => $this->modx->getSelectColumns('modTemplateVarResource', 'modTemplateVarResource'),
        ));
        $query->leftJoin('modTemplateVarResource', 'modTemplateVarResource', 'modTemplateVarResource.contentid = msProduct.id');
        $query->where(array(
            'msProduct.id:IN' => $ids,
            'modTemplateVarResource.id:=' => $tv->get('id'),
            'modTemplateVarResource.value:>' => 0,
        ));
        $query->prepare();
        $query->stmt->execute();
        $result = $query->stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getRemainsByOptions($ids) {
        $query = $this->modx->newQuery('msProduct');
        $query->select(array(
            'msProduct' => $this->modx->getSelectColumns('msProduct', 'msProduct'),
            'msProductOption' => $this->modx->getSelectColumns('msProductOption', 'msProductOption'),
            'msProductFiles' => 'msProductFiles.url',
            'msProductData' => $this->modx->getSelectColumns('msProductData', 'msProductData')
        ));
        $query->leftJoin('msProductOption', 'msProductOption', 'msProductOption.product_id = msProduct.id');
        $query->leftJoin('msProductFile', 'msProductFiles', 'msProductFiles.product_id = msProduct.id');
        $query->leftJoin('msProductData', 'msProductData', 'msProductData.id = msProduct.id');
        $query->where(array(
            'msProductOption.product_id:IN' => $ids,
            'msProductOption.key:=' => $this->config['stockField'],
            'msProductOption.value:>' => 0,
            'msProductFiles.rank:=' => 0,
            'msProductFiles.parent:=' => 0
        ));
        $query->prepare();
        //$query->toSQL();
        //$this->modx->log(xPDO::LOG_LEVEL_ERROR,print_r($query->toSQL(),1));
        $query->stmt->execute();
        $result = $query->stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function sendEmail($message, $subject, $email) {
        $this->modx->getService('mail', 'mail.modPHPMailer');
        if (!$this->modx->mail)
            return false;

        $mailFrom = !empty($this->modx->getOption('instocknotifications_NotificationEmailSender')) ? $this->modx->getOption('instocknotifications_NotificationEmailSender') : $this->modx->getOption('emailsender');

        $mailFromName = !empty($this->modx->getOption('instocknotifications_NotificationEmailFrom')) ? $this->modx->getOption('instocknotifications_NotificationEmailFrom') : $this->modx->getOption('site_name');

        $this->modx->mail->set(modMail::MAIL_BODY, $message);
        $this->modx->mail->set(modMail::MAIL_FROM, $mailFrom);
        $this->modx->mail->set(modMail::MAIL_FROM_NAME, $mailFromName);
        $this->modx->mail->set(modMail::MAIL_SUBJECT, $subject);
        $this->modx->mail->address('to', $email);

        $this->modx->mail->setHTML(true);
        if (!$this->modx->mail->send()) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, 'An error occurred while trying to send the email: '
                    . $this->modx->mail->mailer->ErrorInfo);
        } else {
            $this->modx->mail->reset();
            return true;
        }
    }

}
