<?php

$scriptProperties = [];

$pdoFetch = $modx->getService('pdoFetch');
$pdoFetch->setConfig($scriptProperties);


$class = 'msOrder';
if (empty($where)){
    $where = [
        'msOrder.user_id' => $modx->user->get('id')
    ];
}

if (isset($orderId)){
    $where['msOrder.id'] = $orderId;
}

// Fields to select
$select = [
    'Status' => '`Status`.`name` as `status.name`, `Status`.`color` as `status.color`, `Status`.`id` as `status.id`',
    'Order' => $modx->getSelectColumns('msOrder', 'msOrder', 'order.'),
    'Payment' => '`Payment`.`name` as `payment.name`',
    'Delivery' => '`Delivery`.`name` as `delivery.name`',
    'Address' => $modx->getSelectColumns('msOrderAddress', 'Address', 'address.'),
];

// Joining tables
$leftJoin = [
    'Status' => [
        'class' => 'msOrderStatus',
        'on' => ' `msOrder`.`status` = `Status`.`id`'
    ],
    'Payment' => [
        'class' =>  'msPayment',
        'on' => '`msOrder`.`payment` = `Payment`.`id`',
    ],
    'Delivery' => [
        'class' => 'msDelivery',
        'on' => '`msOrder`.`delivery` = `Delivery`.`id`',
    ],
    'Address' => [
        'class' => 'msOrderAddress',
        'on' => '`msOrder`.`user_id` = `Address`.`user_id`'
    ],
];

if ((!empty($includeBonuses)) && ($includeBonuses == 1)) {
    $cl = $modx->getService('cloudloyalty', 'cloudLoyalty', MODX_CORE_PATH . 'components/cloudloyalty/model/');
    $leftJoin['Bonus'] = [
        'class' => 'cloudLoyaltyItem',
        'on' => '`msOrder`.`num` = `Bonus`.`order_id`'
    ];

    $select['Bonus'] = '`Bonus`.`off_bonuses` as `bonus.off_bonuses`,`Bonus`.`credited_bonuses` as `bonus.credited`';
}

$orderParam = array(
    'class' => $class,
    'where' => json_encode($where),
    'leftJoin' => json_encode($leftJoin),
    'select' => json_encode($select),
    'sortby' => 'createdon',
    'sortdir' => 'DESC',
    'groupby' => '`msOrder`.`id`',
    'return' => 'data',
    'limit' => $limit ? $limit : 100,
    'nestedChunkPrefix' => 'orders_',
);

// Merge all properties and run!
$pdoFetch->setConfig(array_merge($orderParam, $scriptProperties));
$pdoFetch->addTime('Query parameters are prepared.');
$order = $pdoFetch->run();

$products = [];
if ((!empty($includeProducts)) && ($includeProducts == 1)) {
    //GET PRODUCTS DATA
    $class = 'msOrderProduct';
    $leftJoin = [
        'Product' => [
            'class' => 'msProductData',
            'on' => ' `msOrderProduct`.`product_id` = `Product`.`id`'
        ],
    ];
    $where = [
        'msOrderProduct.order_id' => $orderId
    ];

    $select = [
        'orderProduct' => $modx->getSelectColumns('msOrderProduct', 'msOrderProduct'),
        'Product' => $modx->getSelectColumns('msProductData', 'Product', 'product.'),
    ];

    if (!empty($includeThumbs)) {
        $thumbs = array_map('trim', explode(',', $includeThumbs));
        foreach ($thumbs as $thumb) {
            if (empty($thumb)) {
                continue;
            }
            $leftJoin[$thumb] = array(
                'class' => 'msProductFile',
                'on' => "`{$thumb}`.product_id = `msOrderProduct`.`product_id` AND `{$thumb}`.rank = 0 AND `{$thumb}`.path LIKE '%/{$thumb}/%'",
            );
            $select[$thumb] = "`{$thumb}`.url as `{$thumb}`";
            $groupby[] = "`{$thumb}`.url";
        }
    }
    $productsParam = array(
        'class' => $class,
        'where' => json_encode($where),
        'leftJoin' => json_encode($leftJoin),
        'select' => json_encode($select),
        'sortby' => 'createdon',
        'sortdir' => 'DESC',
        'groupby' => '`msOrderProduct`.`id`',
        'return' => 'data',
        'limit' => 0,
        'nestedChunkPrefix' => 'orders_products_',
    );

    $pdoFetch->setConfig(array_merge($productsParam, $scriptProperties));
    $pdoFetch->addTime('Query parameters are prepared.');
    $products = $pdoFetch->run();
}

if (!isset($tplOuter)) {
    $tplOuter = '@FILE:chunks/office/orders/office.orders.outer.tpl';
}

if (!isset($tplEmpty)) {
    $tplEmpty = '@FILE:chunks/office/summary/office.summary.orders.empty.tpl';
}

if (!empty($order)){
    $output = $pdoFetch->getChunk($tplOuter,[
        'orders' => $order,
        'products' => $products,
    ]);
} else {
    $output = $pdoFetch->getChunk($tplEmpty);
}

return $output;
