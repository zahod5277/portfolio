<?php

class cloudLoyalty {

    /** @var modX $modx */
    public $modx;

    /** @var config $ms2 */
    public $config;

    /** @var miniShop2 $ms2 */
    public $ms2;

    /**
     * @param modX $modx
     * @param array $config
     */
    function __construct(modX &$modx, array $config = []) {
        $this->modx = & $modx;
        $corePath = MODX_CORE_PATH . 'components/cloudloyalty/';
        $assetsUrl = MODX_ASSETS_URL . 'components/cloudloyalty/';

        $this->config = array_merge([
            'actionUrl' => $assetsUrl . 'action.php',
            'corePath' => $corePath,
            'modelPath' => $corePath . 'model/',
            'processorsPath' => $corePath . 'processors/',
            'assetsUrl' => $assetsUrl,
            'connectorUrl' => $assetsUrl . 'connector.php',
            'jsUrl' => $assetsUrl . 'js/',
            'json_response' => true,
            'cloudloyalty_frontend_url' => $this->modx->getOption('cloudloyalty_frontend_url', $assetsUrl . 'js/web/default.js'),
            'cloudloyalty_api_key' => $this->modx->getOption('cloudloyalty_api_key', ''),
            'cloudloyalty_api_url' => $this->modx->getOption('cloudloyalty_api_url',''),
            'cloudloyalty_api_shop_code' => $this->modx->getOption('cloudloyalty_api_shop_code',''),
            'cloudloyalty_api_shop_name' => $this->modx->getOption('cloudloyalty_api_shop_name', ''),
            'cloudloyalty_loyalty_action' => $this->modx->getOption('cloudloyalty_loyalty_action', 'apply-collect'),
            'cloudloyalty_phone_field' => $this->modx->getOption('cloudloyalty_phone_field', 'phone'),
            'cloudloyalty_apply_input' => $this->modx->getOption('cloudloyalty_apply_input', '[name="app_bonuses"]'),
            'cloudloyalty_apply_bonuses_text' => $this->modx->getOption('cloudloyalty_apply_bonuses_text', '[data-loyalty-cb]'),
            'cloudloyalty_ms2_success_status' => $this->modx->getOption('cloudloyalty_ms2_success_status','5'),
            'cloudloyalty_ms2_cancel_status' => $this->modx->getOption('cloudloyalty_ms2_cancel_status','4,6'),
                ], $config);
        
        $ctx = isset($this->config['ctx']) ? $this->config['ctx'] : $this->modx->context->key;
        
        if (!$this->ms2 = $this->modx->getService('minishop2')) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, '[cloudLoyalty] Requires installed miniShop2.');

            return false;
        }
        $this->ms2->initialize($ctx);

        $this->modx->addPackage('cloudloyalty', $this->config['modelPath']);
        $this->modx->lexicon->load('cloudloyalty:default');
    }

    /**
     * @param array $param
     */
    public function getUser() {

        if (!$this->modx->user->isAuthenticated()) {
            return false;
        }

        $user = $this->modx->getUser();

        $profile = $user->getOne('Profile')->toArray();
        return $profile;
    }

    public function getCart() {
        return $this->ms2->cart->get();
    }

    /**
     * Independent registration of css and js
     *
     * @param string $objectName Name of object to initialize in javascript
     */
    public function loadJs($objectName = 'cloudLoaylty') {
        if ($js = trim($this->config['cloudloyalty_frontend_url'])) {
            if (preg_match('/\.js/i', $js)) {
                $this->modx->regClientScript(str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $js));
            }
        }
        $config = $this->modx->toJSON(array(
            'assetsUrl' => $this->config['assetsUrl'],
            'actionUrl' => str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $this->config['actionUrl']),
            'cl_apply_input' => $this->config['cloudloyalty_apply_input'],
            'cl_apply_bonuses_text' => $this->config['cloudloyalty_apply_bonuses_text']
        ));
        $objectName = trim($objectName);
        $this->modx->regClientScript(
                "<script type=\"text/javascript\">{$objectName}.initialize({$config});</script>", true
        );
    }

    protected function request($message, $func) {
        $messageText = json_encode($message);
        $options = array(
            'http' => array(
                'header' => "Content-Type: application/json\r\n" .
                "Accept:application/json\r\n" .
                "Cache-Control: no-cache\r\n" .
                "X-Processing-Version:1.0\r\n" .
                "X-Processing-Key:{$this->config['cloudloyalty_api_key']}\r\n",
                'method' => 'POST',
                'content' => $messageText
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($this->config['cloudloyalty_api_url'] . $func, false, $context);
        return $result ? json_decode($result) : '';
    }

    //
    public function getClientBalance($phone = false) {
        //Если не передан номер телефона, то пробуем взять текущего пользователя
        //и достать у него из профиля номер телефона
        //или просто отформатируем то, что пришло
        if (!$phone) {
            $user = $this->getUser();
            if (is_array($user)) {
                $phone = $this->phoneFormatter($user[$this->config['cloudloyalty_phone_field']]);
            }
        } else {
            $phone = $this->phoneFormatter($phone);
        }
        //Если номер телефона всё же удалось получить и отформатировать, то
        //запрашиваем баланс пользователя
        if ($phone) {
            $message = (object) array('phoneNumber' => $phone);
            $answer = $this->request($message, 'get-balance');
            $res = [];
            if (!isset($answer->errorCode)) {
                $res['status'] = 'success';
                $res['data'] = intval($answer->client->bonuses);
            } else {
                $res['status'] = 'error';
                $res['data'] = $answer->errorCode;
            }
        } else {
            $res = false;
        }


        return $res;
    }

    public function registerClient($user) {
        $query = [
            'phoneNumber' => $user[$this->config['cloudloyalty_phone_field']],
            'email' => $user['email'],
            'fullName' => $user['fullname'],
            'gender' => $user['gender'] ? $this->genderFormatter($user['gender']) : 0,
            ];
        
        if (isset($user['dob'])){
            $query['birthdate'] = $this->dobFormatter($user['dob']);
        }

        $message = (object) [
            'client' => (object) $query
        ];
        $answer = $this->request($message, 'new-client');

        $res = [];
        if (!isset($answer->errorCode)) {
            $res['status'] = 'sucсess';
            $res['data'] = true;
        } else {
            $res['status'] = 'error';
            $res['data'] = $answer->errorCode;
            $res['query'] = $query;
        }
        return $res;
    }

    // Вызывать при изменении параметров их личного кабинета
    // для доступа извне
    public function updateClient($user) {
//        $this->modx->log(1,'Происходит обновление профиля пользователя: '.print_r($user));
        $query = array(
            'phoneNumber' => $user[$this->config['cloudloyalty_phone_field']],
            'email' => $user['email'],
            'fullName' => $user['fullname'],
            'gender' => $user['gender'] ? $this->genderFormatter($user['gender']) : '',
            'birthdate' => $user['dob'] ? $this->dobFormatter($user['dob']) : ''
        );
        $message = (object) [
                    'phoneNumber' => $user[$this->config['cloudloyalty_phone_field']],
                    'client' => (object) $query
        ];
        $answer = $this->request($message, 'update-client');

        $res = [];
        if (!isset($answer->errorCode)) {
            $res['status'] = 'suсcess';
            $res['data'] = true;
        } else {
            $res['status'] = 'error';
            $res['data'] = $answer->errorCode;
        }
        return $res;
    }

    // Получает товары в Корзине или в Заказе для дальнейшего использования
    public function getProducts($cart) {
        $products = [];
        foreach ($cart as $cartItem) {
            $product = $this->modx->getObject('msProduct', $cartItem['id'])->toArray();
            $item = [
                'name' => $product['pagetitle'],
                'qty' => $cartItem['count'],
                'price' => $cartItem['price'],
                'cost' => $cartItem['price'] * $cartItem['count'],
                'sku' => $product['article'],
                'apply_bonuses' => $product['apply_bonuses'] == 1 ? true : false
            ];
            $products[] = $item;
        }

        return $products;
    }

    // Расчет общей суммы корзины, к которой можно применить бонусы
    public function getApplyingSumm() {

        $summ = 0;
        $products = $this->getProducts($this->getCart());
        foreach ($products as $item) {
            if ($item['apply_bonuses']) {
                $summ += $item['cost'];
            }
        }

        return $summ;
    }

    public function getCartSumm($cart) {
        $summ = 0;
        foreach ($cart as $item) {
            $summ += $item['price'] * $item['count'];
        }
        return $summ;
    }

    // Для доступа извне функции - За покупку вам будет начислено XXX бонусов
    public function calculate($cartSumm = false, $applyingSumm = false, $applyingBonuses = false) {

        if (!$applyingBonuses) {
            $applyingBonuses = $_SESSION['app_bonuses'] ? $_SESSION['app_bonuses'] : 0;
        }

        $user = $this->getUser();

        if (!$user) {
            return;
        }

        if (!$cartSumm) {
            $cart = $this->getCart();
            $cartSumm = $this->getCartSumm($cart);
        }

        if (!$applyingSumm) {
            $applyingSumm = $this->getApplyingSumm();
        }

        if ($applyingSumm > $cartSumm) {
            $applyingSumm = $cartSumm;
        }

        $message = [
            'calculate' => [
                'phoneNumber' => $user[$this->config['cloudloyalty_phone_field']],
                'totalAmount' => $cartSumm,
                'collectingAmount' => $applyingSumm,
                'loyaltyAction' => $this->config['cloudloyalty_loyalty_action'],
                'applyBonuses' => (float) $applyingBonuses
            ]
        ];
        $answer = $this->request($message, 'calculate-purchase');

        $res = [];
        if (!isset($answer->errorCode)) {
            $res['status'] = 'suсcess';
            $res['data'] = $answer->calculation->collectableBonuses;
        } else {
            $res['status'] = 'error';
            $res['data'] = $answer->errorCode;
            $res['query'] = $message;
        }

        return $res;
    }

    public function checkApplyingBonuses($value, $user) {
        $balance = $this->getClientBalance($user[$this->config['cloudloyalty_phone_field']]);
        if ($balance['status'] == 'error') {
            return $this->modx->lexicon('cloudloyalty_form_err_no_client');
        }
        if ($value > $balance['data']) {
            return $this->modx->lexicon('cloudloyalty_form_err_not_enough_money');
        }
        
        $cart = $this->getCart();
        $cartSumm = $this->getCartSumm($cart);
        
        if ($cartSumm < $value){
            return $this->modx->lexicon('cloudloyalty_form_err_not_enough_summ');
        }
        
        return true;
    }

    public function process($action, array $fields = array()) {
        switch ($action) {
            case 'getCalculateBonuses':
                $appBonuses = $this->calculate(false, false, $_REQUEST['appBonuses']);
                if ($appBonuses['status'] != 'error'){
                    $appBonuses['data'] = $this->modx->lexicon('cloudloyalty_will_recieve_ajax', [
                        'balance' => $appBonuses['data']
                    ]);
                }
                return $this->modx->toJSON($appBonuses);
                break;
            default:
                break;
        }
    }

    //Функция подготовки товаров для отправки в CloudLoyalty
    public function getOrderItems($products) {
        $out = [];
        foreach ($products as $item) {
            if ($item['qty'] > 0) {
                $out[] = [
                    'sku' => $item['sku'],
                    'itemTitle' => $item['name'],
                    'itemCount' => intval($item['qty']),
                    'price' => intval($item['price'])
                ];
            }
        }
        return $out;
    }

    public function setOrder($order) {
        $balance = $this->getClientBalance($order['phone']);
        $error = false;
        if (($balance['status'] == 'error') && ($balance['data'] == 3)) {
            if (!$user = $this->modx->getObject('modUser', $order['user_id'])) {
                $error = '[CloudLoaylty]: User ' . $order['user_id'] . ' not found';
            }
            $phone = $this->phoneFormatter($order['phone']);
            $profile = $user->getOne('Profile');
            $profile->set($this->config['cloudloyalty_phone_field'], $phone);
            $profile->save();
            $userCreate = $this->registerClient($profile->toArray());
            if ($userCreate['status'] == 'error') {
                $error = '[CloudLoaylty]: There was an error creating user. Error code: ' . $userCreate['data'];
            }
        }
        if (($balance['status'] == 'error') && ($balance['data'] != 3)) {
            $error = '[CloudLoaylty]: There was an error creating your order. Error code: ' . $balance['data'];
        }
        $products = $this->getProducts($this->getCart());
        $out = $this->getOrderItems($products);

        if (empty($out)) {
            return;
        }
        
        if (!is_bool($error)) {
            $result = [
                'status' => 'error',
                'data' => $error
            ];
            return $result;
        }
        $this->modx->log(modX::LOG_LEVEL_ERROR, '[cloudLoyalty] Ошибок нет, идем дальше, формируем запрос.');
        $applyingBonuses = $order['app_bonuses'] ? $order['app_bonuses'] : 0;
        $summ = $order['cost'];
        $applyingSumm = $this->getApplyingSumm();
        
        $message = [
            'client' => [
                'phoneNumber' => $order['phone']
            ],
            'order' => [
                'id' => $order['num'], // Номер заказа - строка
                'executedAt' => date('c'), //Дата и время заказа
                'shopCode' => $this->config['cloudloyalty_api_shop_code'], // код магазина, дается системой
                'shopName' => $this->config['cloudloyalty_api_shop_name'], // название магазина, дается системой
                'totalAmount' => $summ + $applyingBonuses, // общая сумма чека до вычета бонусов
                'loyalty' => [
                    'action' => $this->config['cloudloyalty_loyalty_action'],
                    'applyBonuses' => $applyingBonuses,
                    'collectingAmount' => $applyingSumm,
                ],
                'items' => $out
            ]
        ];
        //return (json_encode($message));
        $answer = $this->request($message, 'set-order');
        if (!isset($answer->errorCode)) {
            $result = [
                'status' => 'success',
                'data' => $answer,
            ];
        } else {
            $result = [
                'status' => 'error',
                'data' => $answer,
                'query' => json_encode($message)
            ];
        }
        return $result;
    }
    
    public function cancelOrder ($orderNum){
        $message = [
            'orderId' => $orderNum,
            'executedAt' => date('c'), //Дата и время заказа
        ];

        $answer = $this->request($message, 'cancel-order');

        if (!isset($answer->errorCode)) {
            $result = [
                'status' => 'success',
                'data' => $answer,
            ];
        } else {
            $result = [
                'status' => 'error',
                'data' => $answer,
                'query' => json_encode($message)
            ];
        }

        return $result;
    }

    public function applyReturn($orderNum) {

        // **
        // ** Получить список в заказе использованных бонусных купонов
        // ** Получить сумму использованных в заказе бонусов
        // ** Тут надо получить содержимое отменяемого заказа

        if (!$order = $this->modx->getObject('msOrder', ['num' => $orderNum])) {
            return false;
        }

        if (!$clOrder = $this->modx->getObject('cloudLoyaltyItem', ['order_id' => $orderNum])) {
            return false;
        }

        if (!$addr = $this->modx->getObject('msOrderAddress', ['user_id' => $order->get('user_id')])) {
            return false;
        }

        $products = $order->getMany('Products');

        $productArr = [];
        foreach ($products as $product) {
            $productArr[] = [
                'id' => $product->get('product_id'),
                'price' => $product->get('price'),
                'count' => $product->get('count'),
            ];
        }

        $products = $this->getProducts($productArr);
        $out = $this->getOrderItems($products);

        $message = [
            'transaction' => [
                'phoneNumber' => $addr->get('phone'),
                'id' => $orderNum . '-r', // Номер заказа - нужен новый для отмены
                'executedAt' => date('c'), //Дата и время заказа
                'purchaseId' => $orderNum, // Номер заказа
                'shopCode' => $this->config['cloudloyalty_api_shop_code'], // код магазина, дается системой
                'shopName' => $this->config['cloudloyalty_api_shop_name'], // название магазина, дается системой
                'refundAmount' => $order->get('cost') + $clOrder->get('bonuses'), // общая сумма чека до вычета бонусов
                'items' => $out
            ]
        ];

        $answer = $this->request($message, 'apply-return');

        if (!isset($answer->errorCode)) {
            $result = [
                'status' => 'success',
                'data' => $answer,
            ];
        } else {
            $result = [
                'status' => 'error',
                'data' => $answer,
                'query' => json_encode($message)
            ];
        }

        return $result;
    }

    public function confirmOrder($orderNum) {

        $message = (object) [
            'orderId' => $orderNum, //Номер заказа
            'executedAt' => date('c'), //Дата и время заказа 
        ];

        $answer = $this->request($message, 'confirm-order');

        if (!isset($answer->errorCode)) {
            $result = [
                'status' => 'success',
                'data' => $answer,
            ];
        } else {
            $result = [
                'status' => 'error',
                'data' => $answer,
                'query' => json_encode($message)
            ];
        }

        return $result;
    }

    public function createOrderToCL($order) {
        $off = $order['app_bonuses'] ? $order['app_bonuses'] : 0;
        $appBonuses = $this->calculate(false, false, $off);
        if ($appBonuses['status'] != 'error'){
            $credited = $appBonuses['data'];
        } else {
            $credited = 0;
        }
        $note = $this->modx->newObject('cloudLoyaltyItem', [
            'order_id' => $order['num'],
            'off_bonuses' => $off,
            'credited_bonuses' => $credited,
            'active' => 0
        ]);
        $note->save();
        return true;
    }

    public function updateOrderToCL($order, $active = 0) {
        if ($note = $this->modx->getObject('cloudLoyaltyItem', ['order_id' => $order['num']])) {
            $note->fromArray([
                'active' => $active 
            ]);
            $note->save();
        }
        return true;
    }

    public function error($message = '', $text) {
        $response = array(
            'success' => false,
            'message' => $text,
        );
        return $this->config['json_response'] ? $this->modx->toJSON($response) : $response;
    }

    // Форматирование телефонного номера в ту форму, в которой нужно Лояльности
    protected function phoneFormatter($phone) {
        // Заменить 8 на +7
        // Убрать все знаки кроме цифр и +
        $phone = preg_replace('/[^\d\+]+/', '', $phone);
        $phone = preg_replace('/^8([\d]{10})$/', '+7$1', $phone);
        return $phone;
    }

    //Форматирование даты рождения из microtime в формат RFC3339
    public function dobFormatter($dob) {
        $time = DateTime::createFromFormat('U', $dob);
        return $time->format(\DateTime::RFC3339);
    }

    //Форматирование пола пользователя. 
    public function genderFormatter($gender) {
        if ($gender > 2) {
            $gender = 0;
        }
        return $gender;
    }

}
