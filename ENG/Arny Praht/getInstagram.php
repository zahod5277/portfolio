<?php

if (!isset($cache_key)) {
    $cache_key = 'cache.insta';
}

if (!$data = $modx->cacheManager->get($cache_key)) {

    $pdo = $modx->getService('pdoTools');

    $token = $modx->getOption('ap.instagram.token');

    $insta = file_get_contents('https://api.instagram.com/v1/users/self/media/recent/?&access_token=' . $token);
    $body = json_decode($insta);

    if (!isset($tpl)) {
        $tpl = '@FILE:chunks/main/main.insta.ribbon.tpl';
    }

    if (!isset($quality)) {
        $quality = 'standard_resolution';
    }
    $c = count($body->data);
    $i = 1;
    foreach ($body->data as $images) {

        $link = $images->link;
        $alt = strip_tags($images->caption->text);
        $created_time = $images->created_time;

        foreach ($images->images as $key => $value) {
            if ($key == $quality) {
                $image = $value->url;
            }
        }
        if ($c == $i){
            $last = 1;
        } else {
            $last = 0;
        }
        $instagram[] = array(
            'link' => $link,
            'image' => $image, //Все картинки ->thumbnail, ->low_resolution, ->standard_resolution//->width, ->height, ->url
            'likes' => $images->likes->count,
            'comments' => $images->comments->count,
            'alt' => $alt,
            'last' => $last
        );
        $i++;
    }

    $output = $pdo->getChunk($tpl, [
        'items' => $instagram
    ]);
    $modx->cacheManager->set($cache_key, $output, 86400);
} else {
    $output = $data;
}
return $output;
