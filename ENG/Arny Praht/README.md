# ARNY PRAHT

## Awesome [e-commerce site](https://arnypraht.com/) with annual turnover over 3 000 000 $.
### My role on this project is architector & backend-developer. 
### All backend on this site was done by myself :)  

### Developed from 10.10.2017 to 26.04.2019
### Supported till 31.12.2019

This is my largest project. I was developing this site about 18 months.

My team (me, frontend-developer & designer) didn't have enough skills in project management then, so we was almost refused from this site.
But we did it! 

What did it cost? Everything.

> Special for you i did english meme, please enjoy
![Мемес](meme1.jpg)

This e-commerce site has, probably, all functions which product like this can have.

* Discount system (by products, by cart cost, by geo, by dates, by persons, by groups)
* Loyalty bonus system with SMS auth
* Warehouse stocks exchange 
* Delivery system which calculate delivery cost by customer city
* Ru/Eng language system with custom functions (my personal module which incredible fast)
* [Product designer](https://arnypraht.com/en/customize/)
* Personal customer page with referral program.
* Order history
* Favorites products
* Search engine which can search by synonyms or words with mistakes    
* And so much more

Here is some code snippets of this project.
All this code was done with MODX CODESTYLE, so sorry for missing PSR.

- **[cloudloyalty.class](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Arny%20Praht/cloudloyalty.class.php)** Main class of Loyalty Bonus System API CloudLoyalty.ru
- **[designer.class](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Arny%20Praht/designer.class.php)** Main class of "product designer"
- **[createDesignerOrder](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Arny%20Praht/createDesignerOrder.php)** Form hook which create order from "product designer"
- **[getInstagram](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Arny%20Praht/getInstagram.php)** Get Instagram feed php code
- **[getOrderInfo](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Arny%20Praht/getOrderInfo.php)** Get all orders from MODX
- **[instocknotifications.class](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Arny%20Praht/instocknotifications.class.php)** - Main class of "Notify me when this product will be available". 
Supports many type of fields, can count stocks and send email notifications to customers.
- **[syncOrdersStatusFromMoySklad](https://bitbucket.org/zahod5277/portfolio/src/master/ENG/Arny%20Praht/syncOrdersStatusFromMoySklad.php)** CRON script which sync order statuses.
 
