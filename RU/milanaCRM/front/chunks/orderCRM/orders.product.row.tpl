{foreach $products as $product}
<tr>
    <td>
        <div class="pos__cover col-12">
            
            <div>
                <a target="_blank" href="/{$product.product_id|url}">
                    {var $thumb = $product.medium|pThumb:'w=100&h=100&far=T&bg=FFFFFF&q=90'}
                    <img src="{$thumb}" alt="{$product.name}" class="img img-responsive">
                </a>
            </div>

            <div>
                <a target="_blank" href="/{$product.product_id|url}">
                    <div>
                        {$product.name}
                    </div>
                </a>
            </div>
        </div>
    </td>

    <td>
        {$product['product.article']}
    </td>

    <td>
        <p>Цвет: {$product.options.color}</p>
        <p>Размер: {$product.options.size}</p>
    </td>

    <td>
        <div class="price">{$product.cost * 1} <span class="ld-sp-currency">₽</span></div>
    </td>

    <td>
        {'x'~$product.count}
    </td>    

    <td>
        <div class="price">{$product.cost * $product.count} <span class="ld-sp-currency">₽</span></div>
    </td>
</tr>
{/foreach}