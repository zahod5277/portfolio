{var $mgrs = $_modx->runSnippet('@FILE:snippets/orderCRM/getManagersList.php',[])}
{var $managers = ''}
{foreach $mgrs as $mgr}
    {set $managers = $managers ~ '<option value="'~$mgr['id']~'"> '~$mgr['fullname'] ~'</option>'}
{/foreach}
{var $sts = $_modx->runSnippet('@FILE:snippets/orderCRM/getStatuses.php')}
{var $statuses = ''}
{foreach $sts as $s}
    {set $statuses = $statuses ~ '<option value="'~$s['id']~'"> '~$s['name'] ~'</option>'}
{/foreach}
{*----------------------------------------------------------------*}
<form action="#" id="filter_form" method="GET" class="mt-5 mb-5 row">
    <div class="col-12 col-md-3">
        <div class="form-group">
            <label for="date_from">Дата от:</label>
            <input type="date" name="date_from" class="form-control">
        </div>
        <div class="form-group">
            <label for="date_to">Дата до:</label>
            <input type="date" name="date_to" class="form-control">
        </div>
    </div>
    <div class="col-12 col-md-5">
        <div class="form-group">
            <label for="other">Текстовый фильтр</label>
            <input type="text" class="form-control" name="other" placeholder="ID заказа, номер заказа, имя, e-mail, комментарий, город" data-toggle="tooltip" data-placement="top" title="ID заказа, номер заказа, имя, e-mail, комментарий, город">
        </div>
        <div class="form-group mt-5">
            <div class="input-group flex-nowrap">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="limit">Лимит выбора: </span>
                </div>
                <input type="text" name="limit" class="form-control" placeholder="100" aria-label="100" data-toggle="tooltip" data-placement="top" title="Сколько показывать заказов на странице">
            </div>
        </div>
    </div>
    <div class="col-12 col-md-3">
        <div class="form-group">
            <label for="date_from">Статус</label>
            <select class="form-control" name="status" id="status">
                <option value="0" selected="selected">Любой</option>
                {$statuses}
            </select>
        </div>
        <div class="form-group">
        </div>
    </div>
    <div class="col-12">
        <div class="form-group mt-5">
            <button type="submit" data-crm-filter-submit class="btn btn-primary"> <i class="fa fa-search"></i> Искать</button>
            <button type="reset" class="btn btn-danger"><i class="fa fa-ban"></i> Очистить</button>
        </div>
    </div>
</form>