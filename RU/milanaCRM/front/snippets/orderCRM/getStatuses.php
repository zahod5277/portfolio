<?php

$milana = $modx->getService('milanaCRM', 'milanaCRM', MODX_CORE_PATH . 'components/milanacrm/model/');
if (!$milana) {
    return 'Could not load MilanaCRM class!';
}


$is_sudo = $modx->user->isMember('Administrator') ? true : false;
$statuses = $milana->getStatuses($is_sudo);
return $statuses;