<?php

class milanaCRMOfficeItemCreateProcessor extends modObjectCreateProcessor
{
    public $objectType = 'milanaCRMItem';
    public $classKey = 'milanaCRMItem';
    public $languageTopics = ['milanacrm'];
    //public $permission = 'create';


    /**
     * @return bool
     */
    public function beforeSet()
    {
        $name = trim($this->getProperty('name'));
        if (empty($name)) {
            $this->modx->error->addField('name', $this->modx->lexicon('milanacrm_item_err_name'));
        } elseif ($this->modx->getCount($this->classKey, ['name' => $name])) {
            $this->modx->error->addField('name', $this->modx->lexicon('milanacrm_item_err_ae'));
        }

        return parent::beforeSet();
    }

}

return 'milanaCRMOfficeItemCreateProcessor';