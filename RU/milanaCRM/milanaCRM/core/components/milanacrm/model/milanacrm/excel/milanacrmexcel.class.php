<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Settings;
use PhpOffice\PhpSpreadsheet\Shared\File;
use PhpOffice\PhpSpreadsheet\Collection\CellsFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class milanaCRMExcel
{
    /** @var Spreadsheet $writer */
    private $writer = null;
    private $title = '';
    private $date = '';
    public $sheet = null;

    /** @var modX $modx */
    public $modx;

    /**
     * milanaCRMExcel constructor.
     * @param modX $modx
     * @param array $config
     */
    public function __construct(&$modx, $config = array())
    {
        $this->modx =& $modx;
        $this->seek = 1;

    }

    /**
     * @return boolean
     */
    public function createXLSX($data, $file)
    {

        if (!is_array($data)){
            return;
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $cols = count($data[0]);
        $rows = count($data);
        for ($r = 0; $r <= $rows; $r++) {
            for ($c = 0; $c <= $cols; $c++) {
                $cell = $sheet->getCellByColumnAndRow($c+1, $r+1);
                $coords = $cell->getCoordinate();
                $sheet->setCellValue($coords, strval($data[$r][$c]));
            }
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($file);
        return true;
    }

    private function downloadXLSX($file)
    {

    }

}