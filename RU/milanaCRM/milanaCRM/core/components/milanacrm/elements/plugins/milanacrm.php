<?php
/** @var modX $modx */
/** @var array $scriptProperties */
/** @var milanaCRM $milanaCRM */
$milanaCRM = $modx->getService('milanaCRM', 'milanaCRM', MODX_CORE_PATH . 'components/milanacrm/model/', $scriptProperties);
if (!$milanaCRM) {
    return 'Could not load milanaCRM class!';
}

$ms2 = $modx->getService('miniShop2');
$ms2->initialize('web');

switch ($modx->event->name) {
    case 'milanaCRMSetManager':
        if ($order = $modx->getObject('msOrder', $order_id)) {
            if ($manager_id == 0){
                $new_status = 1;
            } else {
                $new_status = $modx->getOption('milanacrm_manager_status_in');
            }
            $ms2->changeOrderStatus($order_id, $new_status);
        }
        break;
}