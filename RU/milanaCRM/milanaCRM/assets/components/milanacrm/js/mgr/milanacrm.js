var milanaCRM = function (config) {
    config = config || {};
    milanaCRM.superclass.constructor.call(this, config);
};
Ext.extend(milanaCRM, Ext.Component, {
    page: {}, window: {}, grid: {}, tree: {}, panel: {}, combo: {}, config: {}, view: {}, utils: {}
});
Ext.reg('milanacrm', milanaCRM);

milanaCRM = new milanaCRM();