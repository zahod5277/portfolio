<?php

define('MODX_API_MODE', true);
require_once __DIR__ . '/../index.php';
require_once __DIR__ . '/vendor/autoload.php';

// Включаем обработку ошибок
/** @var modX $modx */
$modx->getService('error','error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_INFO);
$modx->setLogTarget(XPDO_CLI_MODE ? 'ECHO' : 'HTML');

use app\Order;
use app\Product;
use app\AccessByToken;
use app\AccessByLogin;
use app\Cart;


$modx->getService('dhApi', 'dhApi', MODX_CORE_PATH . 'components/dhapi/model/');
$modx->getService('msBundle', 'msBundle', MODX_CORE_PATH . 'components/msbundle/model/msbundle/');


header("Access-Control-Allow-Orgin: *");
//header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=utf-8");

$app = new \Slim\Slim();

/**
 * version 1
**/
$app->post('/v1/:token/orders', function ($token) {
	try{
		$dhToken = AccessByToken::check('', $token);
		$data = Order::all($dhToken->context, $_POST, $dhToken->partner);
		echo json_encode($data);
	} catch (\Exception $e) {
		echo json_encode([
    		'error' => $e->getMessage(),
    		'errorCode' => $e->getCode(),
	    ]);
	}
});

$app->post('/v1/:token/products', function ($token) {
	try{
		$dhToken = AccessByToken::check('', $token);
		$data = Product::all($dhToken->context, $_POST, $dhToken->partner);
		echo json_encode($data);
	} catch (\Exception $e) {
		echo json_encode([
    		'error' => $e->getMessage(),
    		'errorCode' => $e->getCode(),
	    ]);
	}
});

$app->post('/v1/:token/order', function ($token) use ($modx) {
    
	try{
		$dhToken = AccessByToken::check('', $token);
		$data = Order::get($dhToken->context, $_POST, $dhToken->partner);
		echo json_encode($data);
	} catch (\Exception $e) {
		echo json_encode([
    		'error' => $e->getMessage(),
    		'errorCode' => $e->getCode(),
	    ]);
	}
});

$app->post('/v1/:token/product', function ($token) {
	try{
		$dhToken = AccessByToken::check('', $token);
		$data = Product::get($dhToken->context, $_POST, $dhToken->partner);
		echo json_encode($data);
	} catch (\Exception $e) {
		echo json_encode([
    		'error' => $e->getMessage(),
    		'errorCode' => $e->getCode(),
	    ]);
	}
});


/**
 * version 2
**/
$app->post('/v2/orders', function () {
	try{
		$dhToken = AccessByLogin::check('');
		$data = Order::all($dhToken->context, $_POST, $dhToken->partner);
		echo json_encode($data);
	} catch (\Exception $e) {
		echo json_encode([
    		'error' => $e->getMessage(),
    		'errorCode' => $e->getCode(),
	    ]);
	}
});

$app->post('/v2/products', function () {
	try{
		$dhToken = AccessByLogin::check('');
		$data = Product::all($dhToken->context, $_POST, $dhToken->partner);
		echo json_encode($data);
	} catch (\Exception $e) {
		echo json_encode([
    		'error' => $e->getMessage(),
    		'errorCode' => $e->getCode(),
	    ]);
	}
});

$app->post('/v2/order', function () {
	try{
		$dhToken = AccessByLogin::check('');
		$data = Order::get($dhToken->context, $_POST, $dhToken->partner);
		echo json_encode($data);
	} catch (\Exception $e) {
		echo json_encode([
    		'error' => $e->getMessage(),
    		'errorCode' => $e->getCode(),
	    ]);
	}
});

$app->post('/v2/product', function () {
	try{
		$dhToken = AccessByLogin::check('');
		$data = Product::get($dhToken->context, $_POST, $dhToken->partner);
		echo json_encode($data);
	} catch (\Exception $e) {
		echo json_encode([
    		'error' => $e->getMessage(),
    		'errorCode' => $e->getCode(),
	    ]);
	}
});


$app->get('/cart-id/:id', function ($id) {
	global $modx;
	ini_set('error_reporting', 0);
    ini_set('display_errors', 0);

	try{
		$url = Cart::get($id);
		$modx->sendRedirect($url);
	} catch (\Exception $e) {
		echo json_encode([
    		'error' => $e->getMessage(),
    		'errorCode' => $e->getCode(),
	    ]);
	}
});

$app->get('/cart-hash/:hash', function ($hash) {
	global $modx;
	ini_set('error_reporting', 0);
    ini_set('display_errors', 0);

	try{
		$url = Cart::get($hash, true);
		$modx->sendRedirect($url);
	} catch (\Exception $e) {
		echo json_encode([
    		'error' => $e->getMessage(),
    		'errorCode' => $e->getCode(),
	    ]);
	}
});

$app->get('/cart-id/:id/products', function ($id) {
	global $modx;

	try{
		echo json_encode(Cart::products($id));
	} catch (\Exception $e) {
		echo json_encode([
    		'error' => $e->getMessage(),
    		'errorCode' => $e->getCode(),
	    ]);
	}
});

$app->get('/cart-hash/:hash/products', function ($hash) {
	global $modx;

	try{
		echo json_encode(Cart::products($hash, true));
	} catch (\Exception $e) {
		echo json_encode([
    		'error' => $e->getMessage(),
    		'errorCode' => $e->getCode(),
	    ]);
	}
});

$app->get('/cart-save/:codes', function ($codes) {
	try{
		echo json_encode(Cart::save($codes));
	} catch (\Exception $e) {
		echo json_encode([
    		'error' => $e->getMessage(),
    		'errorCode' => $e->getCode(),
	    ]);
	}
});


$app->run();