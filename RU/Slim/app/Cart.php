<?php

namespace app;

class Cart
{
    
    public static function getContext()
    {
		global $modx;
        $setting = $modx->getObject('modContextSetting', [
	        'key'   =>  'http_host',
	        'value' =>  $_SERVER['HTTP_HOST'],
        ]);
        
        if (!$setting){
            throw new \Exception("context not found");
        }
        return $setting->context_key;
    }

	public static function save($codes)
	{
		global $modx;
		
		$codes = explode('?', $codes)[0];
	    //$codes = [];
		$partner = isset($_GET['partner']) ? $_GET['partner'] : '';
		$cart = array_diff(explode(',', $codes), ['']);
		$codes = [];
		foreach ($cart as $pos){
		    $pos = array_map('trim', explode(':', $pos));
		    $codes[$pos[0]] = isset($pos[1]) ? $pos[1] : 1;
		}
        
        if (empty($codes)){
            throw new \Exception("codes not found");
        }
		
		
		$ctx = static::getContext();
		$ids = [];
        $c = $modx->newQuery('msProduct')
            ->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id')
            ->innerJoin('modTemplateVarResource', 'Code', array('msProduct.id = Code.contentid', 'Code.tmplvarid' => 8))
            ->where(array(
                    'msProduct.deleted' => 0,
                    'msProduct.published' => 1,
                    'msProduct.context_key' => $ctx,
                    'Code.value:IN' => array_keys($codes),
            ))
            ->groupby('msProduct.id')
            ->select('msProduct.id, Code.value as code');
        if ($c->prepare() && $c->stmt->execute()) {
            while ($row = $c->stmt->fetch(\PDO::FETCH_ASSOC)){
                $ids[$row['code']] = $row['id'];
            }
        }
        foreach ($codes as $code => $count){
            if (isset($ids[$code])){
                $newCart[] = [
                    'id'        =>  intval($ids[$code]),
                    'count'     =>  intval($count),
                    'options'   =>    [],
                ];
            }
        }
        
        if (empty($newCart)){
            throw new \Exception("products not found");
        }
		
		$dhCart = $modx->newObject('dhApiCart');
		$dhCart->set('context', $ctx);
		$dhCart->set('partner', $partner);
		$dhCart->set('cart', $newCart);
		$dhCart->set('hash', md5(time() . $ctx . $partner . json_encode($newCart)));
		$dhCart->save();

		return [
			'id'    => $dhCart->id,
			'hash'  => $dhCart->get('hash'),
		];
	}
    
	public static function get($id, $byHash = false)
	{
		global $modx;
		
		$where = [
		    ($byHash ? 'hash' : 'id') => $id
	    ];
        
        if (!$dhCart = $modx->getObject('dhApiCart', $where)){
            throw new \Exception("cart not found");
        }
        
        $modx->switchContext($dhCart->context);
        
        $_SESSION['minishop2']['openedCart'] = true;
        $_SESSION['minishop2']['dhApiCart'] = $id;
        $modx->setPlaceholder('minishop2_openedCart','opened');
        $miniShop2 = $modx->getService('miniShop2');
        $miniShop2->initialize($modx->context->key);
        $miniShop2->cart->clean();
        
        foreach ($dhCart->get('cart') as $item){
            $miniShop2->cart->add($item['id'], $item['count'], $item['options']);
        }
        if ($dhCart->partner && $partner = $modx->getObject('dhApiPartner', array('name' => $dhCart->partner))){
            $miniShop2->order->add('partner', $partner->id);
        }
        unset($_SESSION['minishop2']['openedCart']);
        
        $setting = $modx->getObject('modContextSetting', [
	        'context_key'   =>  static::getContext(),
	        'key'           =>  'site_url',
        ]);
        
        $_SESSION['minishop2']['dhApiCart'] = $id;
		

		return '/cart.html';
	}
    
	public static function products($id, $byHash = false)
	{
		global $modx;
		
		$where = [
		    ($byHash ? 'hash' : 'id') => $id
	    ];
        
        if (!$dhCart = $modx->getObject('dhApiCart', $where)){
            throw new \Exception("cart not found");
        }
        
        $ids = [];
        foreach ($dhCart->get('cart') as $item){
            $ids[] = $item['id'];
        }
        
        if (empty($ids)) return [];
        
        $q = $modx->newQuery('msProduct')
			->innerJoin('msProductData', 'Data', 'msProduct.id = Data.id')
			->innerJoin('modTemplateVarResource', 'Code', array('msProduct.id = Code.contentid', 'Code.tmplvarid' => 8))
			->where([
			    'msProduct.id:IN' => $ids,
		    ])
			->groupby('msProduct.id')
			->select('msProduct.id, Code.value as code');
			
		$products = [];
		if ($q->prepare() && $q->stmt->execute()){
		    while ($row = $q->stmt->fetch(\PDO::FETCH_ASSOC)){
		        $products[$row['id']] = $row['code'];
		    }
		}
		$all = [];
        foreach ($dhCart->get('cart') as $item){
            if (isset($products[$item['id']])){
                $all[$products[$item['id']]] = $item['count'];
            }
        }
		

		return $all;
	}

}