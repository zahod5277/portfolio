<?php

namespace app;

use app\Product;

class Order
{

	protected static function prepare(\msOrder $order, $full = false)
	{
        global $modx;
        $modx->getService('superWarranty','superWarranty', MODX_CORE_PATH . 'components/superwarranty/model/');
		$output = $order->toArray();
		if ($full){
			$delivery = $order->getOne('Delivery');
			$payment = $order->getOne('Payment');
			$status = $order->getOne('Status');
			$output = array_merge(
				$output,
				$order->getOne('Address')->toArray('addr_'),
				array(
					'addr_email' => $order->getOne('UserProfile')->get('email'),
					'delivery_name' => $delivery->get('name'),
					'payment_name' => $payment->get('name'),
					'status_name' => $status->get('name'),
				)
			);

			$products = array();
			foreach ($order->getMany('Products') as $op){
				
				if ($product = $op->getOne('Product')){
					$tmp = array_merge(
					    [],
						Product::prepare($product, true),
						array(
							'price' => $op->get('price'),
							'weight' => $op->get('weight'),
							'count' => $op->get('count'),
							'options' => $op->get('options'),
						)
					);
				}
				else{
					$tmp = array(
						'pagetitle' => $op->get('name'),
						'price' => $op->get('price'),
						'weight' => $op->get('weight'),
						'count' => $op->get('count'),
						'options' => $op->get('options'),
					);
				}
				$products[] = $tmp;
			}
			$output['products'] = $products;
		}

		$excludes = array(
			'user_id',
			'status',
			'delivery',
			'payment',
			'address',
			'context',
			'properties',
			'type',
			'partner',
			'erp_id',
			'addr_id',
			'addr_user_id',
			'addr_createdon',
			'addr_updatedon',
		);

		$warranties = $modx->getIterator('superWarrantyItem',[
		    'order_id' => $output['id']
        ]);
        $warranties_arr = [];
		foreach ($warranties as $warranty){
            $warranties_tmp = [];
            $warranties_tmp = [
                'warranty_guid' => $warranty->get('guid'),
                'warranty_id' => $warranty->get('warranty_id'),
                'product_id' => $warranty->get('product_id'),
            ];

            if ($warranty_product = $modx->getObject('msProduct',$warranty->get('product_id'))){
                $warranties_tmp['product_name'] = $warranty_product->get('pagetitle');
                $warranties_tmp['product_sku'] = $warranty_product->get('article');
            }

		    if ($warranty_resource = $modx->getObject('msProduct',$warranty->get('warranty_id'))){
                $warranties_tmp['warranty_name'] = $warranty_resource->get('pagetitle');
		        $warranties_tmp['warranty_sku'] = $warranty_resource->get('article');
            }

		    if ($warranty->get('imei_id') != ''){
                if ($warranty_imei = $modx->getObject('superWarrantyImei',$warranty->get('imei_id'))){
                    $warranties_tmp['product_imei'] = $warranty_imei->get('imei');
                }
            }
            $warranties_arr[] = $warranties_tmp;
        }
		if (!empty($warranties_tmp)){
		    $output['warranties'] = $warranties_arr;
        }

		$output = array_diff_key($output, array_flip($excludes));

		return $output;
	}

	public static function get($ctx = '', $postData = [], $partner = null)
	{
	    global $modx;
	    
		$id = isset($postData['id']) ? intval($postData['id']) : 0;
		$num = isset($postData['num']) ? $postData['num'] : '';
		if (empty($id) && empty($num)) {
			throw new \Exception("Order not found", 404);
		}

		$orders_type = isset($postData['orders_type']) ? (string)$postData['orders_type'] : '';

		if (empty($orders_type) || !in_array($orders_type, array('all', 'api'))){
			$orders_type = 'all';
		}

		$criteria = array(
			'context' => $ctx,
		);
		
		if (!empty($id)){
			$criteria['id'] = $id;
		}
		elseif (!empty($num)){
			$criteria['num'] = $num;
		}
		

		if ($orders_type == 'api' && !is_null($partner)){
			$criteria['partner'] = $partner;
		}
		if (!$order = $modx->getObject('msOrder', $criteria)){
			throw new \Exception("Order not found" . print_r($criteria,1), 404);
		}
		
		return array(
			'object' => static::prepare($order, true),
		);
	}

	public static function all($ctx, $postData = [], $partner = null)
	{
		global $modx;

		$limit = intval(isset($postData['limit']) ? $postData['limit'] : 0);
		$offset = intval(isset($postData['offset']) ? $postData['offset'] : 0);

		if (empty($limit)) $limit = 20;
		elseif ($limit > 50) $limit = 50;

		$sortby = isset($postData['sortby']) ? $postData['sortby'] : '';
		if (!in_array($sortby, array('id', 'num', 'createdon', 'editedon', 'cost', 'cart_cost', 'delivery_cost'))){
			$sortby = 'id';
		}
		$sortdir = isset($postData['sortdir']) ? $postData['sortdir'] : '';
		if (!in_array($sortdir, array('asc', 'desc'))){
			$sortdir = 'asc';
		}

		$orders_type = isset($postData['orders_type']) ? (string) $postData['orders_type'] : '';

		if (empty($orders_type) || !in_array($orders_type, array('all', 'api'))){
			$orders_type = 'all';
		}

		$data = array(
			'results' => array(),
			'total' => 0,
			'limit' => $limit,
			'offset' => $offset,
		);

		$criteria = array(
			'context' => $ctx,
		);
		if ($orders_type == 'api' && !is_null($partner)){
			$criteria['partner'] = $partner;
		}
		$q = $modx->newQuery('msOrder')
			->where($criteria)
			->sortby($sortby, $sortdir);

		$data['total'] = $modx->getCount('msOrder', $q);

		if ($data['total'] > 0){
			$q->limit($limit, $offset);
			foreach ($modx->getIterator('msOrder', $q) as $order){
				$data['results'][] = static::prepare($order);
			}
		}
		
		return $data;
	}
}