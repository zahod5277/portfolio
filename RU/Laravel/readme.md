# Поддержка небольшого сайта на Laravel

Разработка шла летом 2021 года.

На фрилансе в 2021-ом году попался заказчик, которому по непонятным причинам сделали сайт-визитку на Ларе. 
Разумнее было использовать WP или MODX, Битрикс на худой конец, но нет, взяли Лару. 

Заказчик невероятно страдает из-за этого. Админка - Nova, но там сделано буквально две вещи - добавление новостей и добавление видео.

Весь остальной контен (включая баннеры и цены) приходится менять в коде. 

### Задача
1) Разработчик сайта не справился с задачей отобразить детальную страницу новости. При попытке открыть страничку конкретной новости Laravel отдавал 500 ошибку.
Необходимо было сделать отображение детальной новости
2) На всех страницах сайта отображались одинаковые title и description. Необходимо было сделать каждые мета-теги уникальными.

### Решение

Реализовал такой метод для вывода детальной новости
```php
public function news_item(Request $request, $id)
    {
        if (!$news_item = Product::find($id)){
            abort(404);
        }
        $model['data'] = $news_item->getAttributes();
        SEO::setTitle($model['data']['name']);
        if (!empty($model['data']['description'])){
            SEO::setDescription($model['data']['description']);
        } else {
            SEO::setDescription($model['data']['name'] . ' - выступление учеников школы "Студия Рондо"');
        }

        $main_page = '/#sTop';
        $about_us = '/#services';
        $podarok = '/gift';
        $price = '/#pricing';
        $contacts = '#contact-us';
        $news_page = 'vknews';
        $hiddenForms = '';

        return view("news_item", ['data' => $model['data']], compact('main_page', 'news_page', 'about_us', 'podarok', 'price', 'contacts', 'hiddenForms'));
    }
```

Прокинул роут

```php
Route::get('/news/{id}', 'App\Http\Controllers\GadgetController@news_item');
```

И вуа-ля!

Для вывода SEO-тегов был использован компонент SEO (уже не помню, из коробки ли он или нет)

```php
// app/Http/Controllers/GadgetController
use SEO;
```

В каждом методе-обработчике маршрута добавлены параметры title и description, которые устанавливаются с помощью класса SEO
```php
SEO::setTitle($title, false);
SEO::setDescription($description);
```

Каждый роут при его обработке пробрасывает эти параметры в контроллер

```php
return App::make('App\Http\Controllers\GadgetController')->controllerMethodName($title, $description);
```

В layouts, в главном шаблоне master.blade.php добавлен вызов генератора

```php
{!! SEO::generate() !!}
```

**Вот так вот красиво и почти без быдлокода я решил две задачи за три часа работы без знания Laravel** 