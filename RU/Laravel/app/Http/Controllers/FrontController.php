<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function solotony() {
        $main_page = '/#sTop';
        $about_us = '/#services';
        $podarok = '/gift';
        $price = '/#pricing';
        $contacts = '#contact-us';
        $news_page = 'vknews';
        $hiddenForms = 'hidden';

        return view('solotony', compact('main_page', 'news_page', 'about_us', 'podarok', 'price', 'contacts', 'hiddenForms'));
    }

    public function cmsmagazine() {
        return response('c4639f3b000df051ff472e1a11bfb147', 200)
            ->header('Content-Type', 'text/plain');
    }

    public function yandex_webmaster() {
        return view('yandex_webmaster', []);
    }

    public function yandex_connect() {
        return view('yandex_connect', []);
    }

    public function google_search_console() {
        return view('google_search_console', []);
    }

}
