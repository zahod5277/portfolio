<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;
use App\Models\Photo;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ProductsRepositoryEloquent;
//use App\Presenters\ProductPresenter;


use App\Entities\Product;
use App\Entities\Catalog;
//
use Illuminate\Support\Facades\Mail;
use SEO;
//use Cart;
//use Mail;

class GadgetController extends Controller
{

//
    /**
     * @var ProductsRepositoryEloquent
     */
    protected $repository;

    /**
     * @param ProductsRepositoryEloquent $repository
     */
    public function __construct(ProductsRepositoryEloquent $repository)
    {
        //$this->middleware('menu');
        //$this->middleware('web');
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($title, $description)
    {
        SEO::setTitle($title, false);
        SEO::setDescription($description);

        $main_page = '#sTop';
        $about_us = '#services';
        $podarok = '#podarok';
        $price = '#pricing';
        $contacts = '#contact-us';
        $news_page = 'vknews';
        $hiddenForms = '';

        return view("index", compact('main_page', 'news_page', 'about_us', 'podarok', 'price', 'contacts', 'hiddenForms'));
    }

    public function ensemble($title, $description)
    {
        SEO::setTitle($title, false);
        SEO::setDescription($description);

        $main_page = '/#sTop';
        $about_us = '/#services';
        $podarok = '/gift';
        $price = '/#pricing';
        $contacts = '#contact-us';
        $news_page = 'vknews';
        $hiddenForms = 'hidden';
        return view("sectionensemble", compact('main_page', 'news_page', 'about_us', 'podarok', 'price', 'contacts', 'hiddenForms'));
    }

    public function gift($title, $description)
    {
        SEO::setTitle($title, false);
        SEO::setDescription($description);

        $main_page = '/#sTop';
        $about_us = '/#services';
        $podarok = '/gift';
        $price = '/#pricing';
        $contacts = '#contact-us';
        $news_page = 'vknews';
        $hiddenForms = '';
        return view("gift", compact('main_page', 'news_page', 'about_us', 'podarok', 'price', 'contacts', 'hiddenForms'));
    }

    public function contacts($title, $description)
    {
        SEO::setTitle($title, false);
        SEO::setDescription($description);

        $main_page = '/#sTop';
        $about_us = '/#services';
        $podarok = '/gift';
        $price = '/#pricing';
        $contacts = '#contact-us';
        $news_page = 'vknews';
        $hiddenForms = '';
        return view("contacts", compact('main_page', 'news_page', 'about_us', 'podarok', 'price', 'contacts', 'hiddenForms'));
    }

    public function gallery($title, $description)
    {
        SEO::setTitle($title, false);
        SEO::setDescription($description);

        return view("gallery", [
            'main_page' => '/#sTop',
            'about_us' => '/#services',
            'podarok' => '/gift',
            'price' => '/#pricing',
            'contacts' => '#contact-us',
            'news_page' => 'vknews',
            'images' => Photo::query()->get(),
            'hiddenForms' => ''
        ]);
    }

    public function message(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required|max:255'
        ]);

        $name = $request->input('name');
        $phone = $request->input('phone');

        $skype = $request->input('skype');
        $time = $request->input('time');


        Mail::send('message', ['name' => $name, 'phone' => $phone, 'skype' => $skype, 'time' => $time], function ($message) use ($name, $phone, $time, $skype) {
            $message->from('info@rguitar.ru', 'СТУДИЯ РОНДО');
            $message->subject('Заявка с сайта СТУДИЯ РОНДО');
            $message->to('peter.butrimoff@mail.ru');
        });
//manager@rguitar.ru
        $request->session()->flash('message', 'Спасибо за заявку!');

        // return redirect('/#contact-us');
        return redirect('/spasibo');
    }

    public function spasibo()
    {

        $main_page = '/#sTop';
        $about_us = '/#services';
        $podarok = '/gift';
        $price = '/#pricing';
        $contacts = '#contact-us';
        $news_page = 'vknews';
        $hiddenForms = '';

        return view("spasibo", compact('main_page', 'news_page', 'about_us', 'podarok', 'price', 'contacts', 'hiddenForms'));
    }

    public function news($request, $title, $description, $params)
    {
        if (isset($params['page']) && !empty($params['page'] != '')){
            $title = $title.' страница '.$params['page'];
            $description = $description.' страница '.$params['page'];
        }

        SEO::setTitle($title, false);
        SEO::setDescription($description);

        $products = Product::where(function ($query)use($request) {

            //$executions = Input::has('executions') ? Input::get('executions') : [];
            $executions = $request->has('executions') ? $request->get('executions') : [];


            if (isset($executions)) {
                foreach ($executions as $execution) {
                    $query->orWhere('video', '=', $execution);
                }
            }


        })->latest('created_at')->active()->paginate(5);

        $main_page = '/#sTop';
        $about_us = '/#services';
        $podarok = '/gift';
        $price = '/#pricing';
        $contacts = '#contact-us';
        $news_page = 'vknews';
        $hiddenForms = '';
        return view("news", compact('main_page', 'news_page', 'about_us', 'podarok', 'price', 'contacts', 'products', 'title', 'hiddenForms'));
    }


    public function vknews($request, $title, $description)
    {
        SEO::setTitle($title, false);
        SEO::setDescription($description);

        $main_page = '/#sTop';
        $about_us = '/#services';
        $podarok = '/gift';
        $price = '/#pricing';
        $contacts = '#contact-us';
        $news_page = 'vknews';
        $hide_bottom_vk = true;
        $hiddenForms = '';
        return view("vknews", compact('main_page', 'news_page', 'about_us', 'podarok', 'price', 'contacts', 'title', 'hiddenForms', 'hide_bottom_vk'));
    }


    public function video($request, $title, $description)
    {
        SEO::setTitle($title, false);
        SEO::setDescription($description);

        $products = Product::where(function ($query) {
        })->latest('created_at')->active()->paginate(999);

        $main_page = '/#sTop';
        $about_us = '/#services';
        $podarok = '/gift';
        $price = '/#pricing';
        $contacts = '#contact-us';
        $news_page = 'vknews';
        $hiddenForms = '';
        return view("video", compact('main_page', 'news_page', 'about_us', 'podarok', 'price', 'contacts', 'products', 'title', 'hiddenForms'));
    }


    public function news_item(Request $request, $id)
    {
        if (!$news_item = Product::find($id)){
            abort(404);
        }
        $model['data'] = $news_item->getAttributes();
        SEO::setTitle($model['data']['name']);
        if (!empty($model['data']['description'])){
            SEO::setDescription($model['data']['description']);
        } else {
            SEO::setDescription($model['data']['name'] . ' - выступление учеников школы "Студия Рондо"');
        }

        $main_page = '/#sTop';
        $about_us = '/#services';
        $podarok = '/gift';
        $price = '/#pricing';
        $contacts = '#contact-us';
        $news_page = 'vknews';
        $hiddenForms = '';

        return view("news_item", ['data' => $model['data']], compact('main_page', 'news_page', 'about_us', 'podarok', 'price', 'contacts', 'hiddenForms'));
    }

    public function clear_cache(){
        Artisan::call('cache:clear');
        return;
    }

}
