<?php

$MESS['DIH_MODULE_NAME'] = 'Модуль diHouse';
$MESS['DIH_MODULE_DESCRIPTION'] = 'Модуль, расширяющий функционал Битрикса для решения задач компании Дихаус';
$MESS['DIH_MODULE_PARTNER_NAME'] = 'Компания diHouse';
