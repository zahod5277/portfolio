# Модуль оплаты для miniShop2 для сервиса Яндекс.Касса с передачей онлайн-чеков по 54-ФЗ

### Дата разработки 4-5 сентября 2020 г.
### Затраченное время на разработку ~8 часов

## О компоненте

В официальном магазине miniShop2 находится устаревший модуль для оплаты Яндекс.Кассой - он работает по старому протоколу, по которому Яндекс уже не подключает.

Заказчик - компания "ООО ДиХаус", входящая в концерн "Ланит", запросила разработку компонента оплаты с нуля под её нужды.

Компонент был написан за пару дней, на внедрение на сайт заказчика еще ушло несколько дней из-за особенностей бизнес-процессов на сайте и разного рода говнокода.

Компонент универсальный - встанет на любой магазин с miniShop2 и уже готов к работе после установки токена авторизации.

За основу был взят класс с Гитхаба - https://github.com/yandex-money/yandex-checkout-sdk-php


В компоненте всего два основных файла:

- /core/components/yandexcheckout/model/yandexcheckout.class.php

Класс для работы с либой с Гитхаба - выполняет авторизацию, создаёт объект платежа, формирует корзину покупок и чек.

- /assets/components/yandexcheckout/yandexcheckout.php

Файл приёма входящих данных от Яндекса - уведомления об изменении статуса платежа 
