<?php
if (!class_exists('msPaymentInterface')) {
    require_once MODX_CORE_PATH . 'components/minishop2/model/minishop2/mspaymenthandler.class.php';

}

require_once dirname(dirname(__FILE__)) . '/vendor/sdk/lib/autoload.php';

use YandexCheckout\Client;
use YandexCheckout\Request\Payments\CreatePaymentRequest;
use YandexCheckout\Model\ConfirmationType;

class YandexCheckout extends msPaymentHandler implements msPaymentInterface
{
    /**
     * YandexCheckout constructor.
     *
     * @param xPDOObject $object
     * @param array $config
     */
    function __construct(xPDOObject $object, $config = array())
    {
        parent::__construct($object, $config);

        $siteUrl = $this->modx->getOption('site_url');
        $assetsUrl = $this->modx->getOption('assets_url') . 'components/yandexcheckout/';
        $paymentUrl = $siteUrl . substr($assetsUrl, 1) . 'yandexcheckout.php';
        $this->config = array_merge(array(
            'currency' => $this->modx->getOption('yandexcheckout_ms2_payment_currency', null, 'RUB'),
            'vat' => $this->modx->getOption('yandexcheckout_ms2_payment_vat', null, '4'),
            'shopid' => $this->modx->getOption('yandexcheckout_ms2_payment_shopid'),
            'secret' => $this->modx->getOption('yandexcheckout_ms2_payment_secret'),
            'paymentUrl' => $paymentUrl
        ), $config);

        $this->modx->addPackage('yandexcheckout', MODX_CORE_PATH . 'components/yandexcheckout/model/');
        $this->client = new Client();
        $this->client->setAuth($this->config['shopid'], $this->config['secret']);
    }

    /**
     * @param msOrder $order
     *
     * @return array|string
     */
    public function send(msOrder $order)
    {
        if ($order->get('status') > 1) {
            return $this->error('ms2_err_status_wrong');
        }

        $builder = $this->buildPayment($order);

        $request = $builder->build();

        $payment = $this->client->createPayment($request);
        $confirmation = $payment->getConfirmation();
        $payment_link = $confirmation->getConfirmationUrl();

        if (!$this->modx->getObject('yandexcheckoutItem', [
            'order_id' => $order->get('id')
        ])) {
            $checkout_item = $this->modx->newObject('yandexcheckoutItem', [
                'order_id' => $order->get('id'),
                'yandex_checkout_id' => $payment->getId(),
                'payment_link' => $payment_link,
                'status' => $payment->_status,
            ]);
            $checkout_item->save();
        }

        if ($payment->_status == 'pending') {
            return $this->success('', array('redirect' => $payment_link));
        }
    }

    /**
     * @param msOrder $order
     * @param array $params
     *
     * @return bool
     */
    public function receive(msOrder $order, $params = array())
    {
        if ($payment_object = $this->modx->getObject('yandexcheckoutItem', [
            'order_id' => $order->get('id')
        ])) {
            $payment_id = $payment_object->get('yandex_checkout_id');
            $payment = $this->client->getPaymentInfo($payment_id);
            return $payment;
        }
        return true;
    }

    /**
     * @param modObject $products
     *
     * @return array
     */
    public function fillProducts($products)
    {
        $items = [];
        foreach ($products as $item) {
            $name = $item->get('name');
            if (empty($name) && $product = $item->getOne('Product')) {
                $name = $product->get('pagetitle');
            }
            $items[] = [
                "description" => $name,
                "quantity" => $item->get('count'),
                "amount" => [
                    "value" => str_replace(',', '.', $item->get('price')),
                    "currency" => $this->config['currency']
                ],
                "vat_code" => $this->config['vat'],
                "payment_mode" => "full_prepayment",
                "payment_subject" => "commodity"
            ];
        }
        return $items;
    }

    /**
     * Returns a direct link for continue payment process of existing order
     *
     * @param msOrder $order
     *
     * @return string
     */
    public function getPaymentLink(msOrder $order)
    {
        $payment_link = false;
        if ($payment_object = $this->modx->getObject('yandexcheckoutItem', [
            'order_id' => $order->get('id'),
        ])) {
            $payment_link = $payment_object->get('payment_link');
        }
        return $payment_link;
    }

    /**
     * @return CreatePaymentRequest
     */
    public function buildPayment($order)
    {
        $user = $order->getOne('UserProfile');
        $customer = [
            "full_name" => $user->get('fullname') ? $user->get('fullname') : $user->get('email'),
            "phone" => $user->get('phone') ? $this->formatPhone($user->get('phone')) : '',
            "email" => $user->get('email')
        ];

        $products = $order->getMany('Products');
        $items = $this->fillProducts($products);

        $builder = CreatePaymentRequest::builder();
        $builder->setAmount($order->get('cost'))
            ->setCurrency($this->config['currency'])
            ->setDescription('Order №' . $order->get('id'))
            ->setClientIp($_SERVER['REMOTE_ADDR'])
            ->setCapture(true)
            ->setMetadata(array(
                'order_id' => $order->get('id'),
                'cms_name' => 'ya_api_modx_revolution',
            ));

        $confirmation = array(
            'type' => ConfirmationType::REDIRECT,
            'returnUrl' => $this->config['paymentUrl'] . '?order_id=' . $order->get('id'),
        );

        $builder->setConfirmation($confirmation);
        //26e33038-000f-5000-9000-13ad990587d0
        $builder->setReceiptEmail($customer['email']);
        $builder->setReceiptPhone($customer['phone']);
        foreach ($items as $item) {
            $builder->addReceiptItem(
                $item['description'],
                $item['amount']['value'],
                $item['quantity'],
                $item['vat_code'],
                $item['payment_mode'],
                $item['payment_subject']
            );
        }

        if ($order->get('delivery_cost') > 0) {
            $builder->addReceiptItem(
                'Доставка',
                str_replace(',', '.', $order->get('delivery_cost')),
                1,
                $this->config['vat'],
                "full_prepayment",
                "commodity"
            );
        }
        return $builder;
    }

    /**
     * @return string
     */
    public function formatPhone($phone)
    {
        preg_match_all('/(\d)+/', $phone, $arr);
        $formatted_phone = implode('', $arr[0]);
        return $formatted_phone;
    }
}