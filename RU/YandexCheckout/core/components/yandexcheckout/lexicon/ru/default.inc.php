<?php
include_once 'setting.inc.php';

$_lang['yandexcheckout'] = 'yandexcheckout';
$_lang['yandexcheckout_menu_desc'] = 'Пример расширения для разработки.';
$_lang['yandexcheckout_intro_msg'] = 'Вы можете выделять сразу несколько предметов при помощи Shift или Ctrl.';

$_lang['yandexcheckout_items'] = 'Предметы';
$_lang['yandexcheckout_item_id'] = 'Id';
$_lang['yandexcheckout_item_name'] = 'Название';
$_lang['yandexcheckout_item_description'] = 'Описание';
$_lang['yandexcheckout_item_active'] = 'Активно';

$_lang['yandexcheckout_item_create'] = 'Создать предмет';
$_lang['yandexcheckout_item_update'] = 'Изменить Предмет';
$_lang['yandexcheckout_item_enable'] = 'Включить Предмет';
$_lang['yandexcheckout_items_enable'] = 'Включить Предметы';
$_lang['yandexcheckout_item_disable'] = 'Отключить Предмет';
$_lang['yandexcheckout_items_disable'] = 'Отключить Предметы';
$_lang['yandexcheckout_item_remove'] = 'Удалить Предмет';
$_lang['yandexcheckout_items_remove'] = 'Удалить Предметы';
$_lang['yandexcheckout_item_remove_confirm'] = 'Вы уверены, что хотите удалить этот Предмет?';
$_lang['yandexcheckout_items_remove_confirm'] = 'Вы уверены, что хотите удалить эти Предметы?';
$_lang['yandexcheckout_item_active'] = 'Включено';

$_lang['yandexcheckout_item_err_name'] = 'Вы должны указать имя Предмета.';
$_lang['yandexcheckout_item_err_ae'] = 'Предмет с таким именем уже существует.';
$_lang['yandexcheckout_item_err_nf'] = 'Предмет не найден.';
$_lang['yandexcheckout_item_err_ns'] = 'Предмет не указан.';
$_lang['yandexcheckout_item_err_remove'] = 'Ошибка при удалении Предмета.';
$_lang['yandexcheckout_item_err_save'] = 'Ошибка при сохранении Предмета.';

$_lang['yandexcheckout_grid_search'] = 'Поиск';
$_lang['yandexcheckout_grid_actions'] = 'Действия';