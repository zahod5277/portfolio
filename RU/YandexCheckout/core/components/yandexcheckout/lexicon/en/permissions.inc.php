<?php
/**
 * Russian permissions Lexicon Entries for yandexcheckout
 *
 * @package yandexcheckout
 * @subpackage lexicon
 */
$_lang['yandexcheckout_save'] = 'Permission for save/update data.';