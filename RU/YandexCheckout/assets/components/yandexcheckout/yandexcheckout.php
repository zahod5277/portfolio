<?php
define('MODX_API_MODE', true);
/** @noinspection PhpIncludeInspection */
require dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';

/** @var modX $modx */
$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

/** @var miniShop2 $miniShop2 */
$miniShop2 = $modx->getService('miniShop2');
$miniShop2->loadCustomClasses('payment');

if (!class_exists('YandexCheckout')) {
    exit('Error: could not load payment class "YandexCheckout".');
}

//обработка входящих вебхуков
$source = file_get_contents('php://input');
$request_body = json_decode($source, true);

if (isset($_GET['order_id'])) {
    $order_id = $_GET['order_id'];
} elseif (isset($request_body['object']['metadata']['order_id'])) {
    $order_id = $request_body['object']['metadata']['order_id'];
}

if (!isset($order_id)) {
    $modx->log(1, '[YandexCheckout] Не указан ID платежа для receive');
    exit('Access denied');
}
//обработка платежа после ответа кассы
if ($order = $modx->getObject('msOrder', $order_id)) {
    $modx->switchContext($order->get('context'));
    $cancel = $modx->getOption('yandexcheckout_ms2_payment_cancel_id', null, 0);
    $redirect = $modx->makeUrl($cancel);
    $handler = new YandexCheckout($order);
    $payment = $handler->receive($order, []);
    if (!$payment) {
        $modx->sendRedirect($modx->makeUrl(1));
    }
    $status = $payment->getStatus();
    if ($status == 'succeeded') {
        $id = $modx->getOption('yandexcheckout_ms2_payment_success_id');
        $status_id = $modx->getOption('yandexcheckout_ms2_payment_success_status_id', null, 2);
        $miniShop2->changeOrderStatus($order->get('id'), $status_id);
        $redirect = $modx->makeUrl($id);

        if ($payment_object = $modx->getObject('yandexcheckoutItem', [
            'order_id' => $order->get('id')
        ])) {
            $payment_object->set('status', $status);
            $payment_object->save();
        }

    } elseif ($status == 'canceled') {
        $id = $modx->getOption('yandexcheckout_ms2_payment_cancel_id');
        $status_id = $modx->getOption('yandexcheckout_ms2_payment_cancel_status_id', null, 4);
        $miniShop2->changeOrderStatus($order->get('id'), $status_id);
        $redirect = $modx->makeUrl($id, $order->get('context'), [], 'full');

        if ($payment_object = $modx->getObject('yandexcheckoutItem', [
            'order_id' => $order->get('id')
        ])) {
            $payment_object->set('status', $status);
            $payment_object->save();
        }

    } elseif ($status == 'pending') {
        $continue_order = new YandexCheckout($order);
        $response = $continue_order->send($order);
        if ($response['success'] == 1) {
            $redirect = $response['data']['redirect'];
        }
    }
    $modx->sendRedirect($redirect);
}