<?php
/** @var xPDOTransport $transport */
/** @var array $options */
/** @var modX $modx */
if ($transport->xpdo) {
    $modx =& $transport->xpdo;

    $dev = MODX_BASE_PATH . 'Extras/yandexcheckout/';
    /** @var xPDOCacheManager $cache */
    $cache = $modx->getCacheManager();
    if (file_exists($dev) && $cache) {
        if (!is_link($dev . 'assets/components/yandexcheckout')) {
            $cache->deleteTree(
                $dev . 'assets/components/yandexcheckout/',
                ['deleteTop' => true, 'skipDirs' => false, 'extensions' => []]
            );
            symlink(MODX_ASSETS_PATH . 'components/yandexcheckout/', $dev . 'assets/components/yandexcheckout');
        }
        if (!is_link($dev . 'core/components/yandexcheckout')) {
            $cache->deleteTree(
                $dev . 'core/components/yandexcheckout/',
                ['deleteTop' => true, 'skipDirs' => false, 'extensions' => []]
            );
            symlink(MODX_CORE_PATH . 'components/yandexcheckout/', $dev . 'core/components/yandexcheckout');
        }
    }
}

return true;