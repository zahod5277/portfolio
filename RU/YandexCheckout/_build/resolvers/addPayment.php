<?php

if ($transport->xpdo) {
    $modx = & $transport->xpdo;
    switch ($options[xPDOTransport::PACKAGE_ACTION]) {
        case xPDOTransport::ACTION_INSTALL:
        case xPDOTransport::ACTION_UPGRADE:
            if ($miniShop2 = $modx->getService('miniShop2')) {
                $miniShop2->addService('payment', 'YandexCheckout',
                    '{core_path}components/yandexcheckout/model/yandexcheckout.class.php'
                );
            }
            break;

        case xPDOTransport::ACTION_UNINSTALL:
            break;
    }
}