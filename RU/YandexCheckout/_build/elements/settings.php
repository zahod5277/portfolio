<?php

return [
    'ms2_payment_currency' => [
        'xtype' => 'textfield',
        'value' => 'RUB',
        'area' => 'yandexcheckout_main',
    ],
    'ms2_payment_vat' => [
        'xtype' => 'textfield',
        'value' => '4',
        'area' => 'yandexcheckout_main',
    ],
    'ms2_payment_shopid' => [
        'xtype' => 'textfield',
        'value' => '',
        'area' => 'yandexcheckout_main',
    ],
    'ms2_payment_secret' => [
        'xtype' => 'textfield',
        'value' => '',
        'area' => 'yandexcheckout_main',
    ],
    'ms2_payment_success_id' => [
        'xtype' => 'textfield',
        'value' => '',
        'area' => 'yandexcheckout_main',
    ],
    'ms2_payment_success_status_id' => [
        'xtype' => 'textfield',
        'value' => '2',
        'area' => 'yandexcheckout_main',
    ],
    'ms2_payment_cancel_id' => [
        'xtype' => 'textfield',
        'value' => '',
        'area' => 'yandexcheckout_main',
    ],
    'ms2_payment_cancel_status_id' => [
        'xtype' => 'textfield',
        'value' => '4',
        'area' => 'yandexcheckout_main',
    ],
];