<?php

return [
    'yandexcheckoutUserPolicyTemplate' => [
        'description' => 'yandexcheckout policy template description.',
        'template_group' => 1,
        'permissions' => [
            'yandexcheckout_save' => [],
        ]
    ],
];