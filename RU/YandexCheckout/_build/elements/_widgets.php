<?php

return [
    'yandexcheckout' => [
        'description' => '',
        'type' => 'file',
        'content' => '',
        'namespace' => 'yandexcheckout',
        'lexicon' => 'yandexcheckout:dashboards',
        'size' => 'half',
    ],
];