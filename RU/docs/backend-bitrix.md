# Руководство по базовым принципам написания кода на 1С Битрикс

## Определение хорошего кода

1. Код протестирован
   * Написаны юнит-тесты
   * Если нет возможности написать юнит-тесты, необходимо проверить код на различные входные данные - нули, булевы значения, смешанные строки (число+строка, строка+число), дробные числа, неверные параметры - вместо массива подать строку или наоборот, отсутствие переменной и т.п.
   * Проведено функциональное тестирование
     * Функциональное тестирование проводится как разработчиком, так и заказчиком

2. Код откомментирован
   * Избегайте чрезмерного комментирования - код должен описывать сам себя. Если есть какие-то сложные участки или какие-то магические числа - комментировать нужно их.
   * @TODO разрешается и даже приветствуется, если есть какие-то пожелания самого разработчика по улучшению качества кода

### Необязательные требования по комментированию кода:

   * Соблюдается формат PHPDoc. Наглядная статья здесь - [https://elisdn.ru/blog/80/some-reasons-to-learn-phpdoc](https://elisdn.ru/blog/80/some-reasons-to-learn-phpdoc)
   * Описываем входные и выходные данные и их типы

3. Код прошёл ревью
   * Если код не прошёл ревью у тим-лида, он не должен быть допущен до деплоя на боевой сервер


4. Код отформатирован
   * При форматировании соблюдаются стандарты PSR (на худой конец Битрикс).
   * Wordpress-style код НЕ ДОПУСКАЕТСЯ. За это сразу расстрел и распятие!


5. Код задокументирован
   * Описывается постановка задачи
   * Алгоритм работы модуля - описание класса, зависимости, входные и выходные данные
   * Примеры использования


## Правила написания кода на 1С Битрикс

**Базовые принципы работы с Битрикс.**

1. Собственные компоненты располагаются в **local/components** либо в **bitrix/components/**, но никак не в **bitrix/components/bitrix/**
   1. Если продукт не имеет много сайтовости, то собственные компоненты располагаются в **local/**
   2. Если продукт имеет многосайтовость, но компонент решает задачи локального сайта (например, вывод какого-то инфоблока с уникальным условием) - то компонент располагается в **local/**
   3. Если компонент общий, такой как служба доставки или платёжная система, то компонент располагается в **components**
   4. Шаблон .default - не используется! При наличии множества реализаций компонента (например, news:list), непонятно, почему один шаблон дефолтный, а другие - нет. Плюс дефолтные шаблоны тянут за собой множество лишних файлов, которые попадают в проект. Лучше делать именованный шаблон со строго согласованным количеством файлов компонента. Ведь, как правило, всякие lang и desc файлы чаще всего не нужны, так же как и скрипты и стили дефолтного шаблона. Это всё лишний мусор, от него необходимо избавляться. Идеальный шаблон простого компонента - это template.php (или twig) и result_modifier.php. Красота. 
2. Собственные компоненты пишутся в ООП стиле, не в процедурном.
   1. Обязательно использование **class.php** вместо **component.php**
   2. В коде компонента отсутствует HTML код. Пример ниже - плохой код. Среди кода компонента вылезла вёрстка. Вёрстку необходимо передавать в **template.ph**
   3. Используется **D7**
   4. В коде компонентов нет больших закомментированных участков. Разрешается комментировать var_dump переменных, но не более. Пример плохого кода ниже. В середине файла встречается огромный кусок задокументированного кода. Этот мусор будет лежать здесь очень долго.

      ```plaintext
      while($ob = $res->GetNextElement())
      {
          $arFields = $ob->GetFields();
          $arProduct[] = $arFields['ID'];
      }
      
      if(count($arProduct) < $arParams['PAGE_ELEMENT_COUNT'])
      {
          $page_count = intval($arParams['PAGE_ELEMENT_COUNT'] - count($arProduct));
          /*такой код необходимо убирать, он будет лежать годами
          $ordersfilter = array(
              array(
                  'LOGIC' => 'OR',
                  array(
                      '=STATUS_ID' => 'CP',
                      '>=DATE_UPDATE]' => date('d.m.Y', strtotime("-" . $arParams['PERIOD'] . " days")),
                  ),
                  array(
                      '=STATUS_ID' => 'R',
                      '>=DATE_UPDATE]' => date('d.m.Y', strtotime("-" . $arParams['PERIOD'] . " days")),
                  )
              )
          );
          */
      
          $ordersfilter = array(
              '=STATUS_ID' => 'CP',
              '>=DATE_UPDATE]' => date('d.m.Y', strtotime("-" . $arParams['PERIOD'] . " days")),
          );
      
      
      ```
3. **init.php** не содержит сложной логики - всё вынесено в модули и классы
4. Большие участки кода в шаблонах вынесены в отдельные php файлы

   Плохой код - размывается контекст, сложно читать большую простыню

   ```html
     Плохо
     <!DOCTYPE html>
     <html>
     	<head>
     		<meta charset="UTF-8"/>
     		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
     
     		<?$APPLICATION->ShowHead();?>
     		
     		<title><?$APPLICATION->ShowTitle();?></title>
     		
     		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
     
     		<link href="/bitrix/js/main/loader/dist/loader.bundle.min.css?16114221762029" type="text/css"  rel="stylesheet" />
                     <link href="/bitrix/js/socialservices/css/ss.min.css?16114256354686" type="text/css"  rel="stylesheet" />
                     <link href="/bitrix/css/bx_site/learning/tab_panel.css?15954252855070" type="text/css"  rel="stylesheet" />
                     <link href="/bitrix/css/bx_site/service/detail_word.css?15431604632663" type="text/css"  rel="stylesheet" />
                     <link href="/bitrix/css/bx_site/service/learning_partner_rest.css?1522923885293" type="text/css"  rel="stylesheet" />
                     <link href="/bitrix/templates/learning_new_2014/components/bitrix/search.form/.default/style.css?15263820001623" type="text/css"  rel="stylesheet" />
                     <link href="/bitrix/templates/learning_new_2014/components/bitrix/learning.course/.default/style.css?1526382000855" type="text/css"  rel="stylesheet" />
                     <link href="/bitrix/templates/learning_new_2014/components/bitrix/learning.course/.default/bitrix/learning.course.tree/.default/style.css?15263820001139" type="text/css"  rel="stylesheet" />
                     <link href="/bitrix/templates/learning_new_2014/components/bitrix/learning.course/.default/bitrix/learning.test.list/.default/style.css?152638200092" type="text/css"  rel="stylesheet" />
                     <link href="/bitrix/templates/learning_new_2014/components/bitrix/learning.course/.default/bitrix/learning.course.tree/navigation/style.css?1526382000435" type="text/css"  rel="stylesheet" />
                     <link href="/bitrix/templates/.default/styles/magula.css?15263820001945" type="text/css"  data-template-style="true"  rel="stylesheet" />
                     <link href="/bitrix/templates/learning_new/css/images_with_zoom.css?1526382000531" type="text/css"  data-template-style="true"  rel="stylesheet" />
                     <link href="/bitrix/templates/.default/css/learning/common.css?1526382000230" type="text/css"  data-template-style="true"  rel="stylesheet" />
                     <link href="/bitrix/templates/.default/css/learning/service_blocks.css?1526382000960" type="text/css"  data-template-style="true"  rel="stylesheet" />
                     <link href="/bitrix/templates/b24sites_unify/assets/vendor/icon-awesome/css/font-awesome.min.css?152638200030903" type="text/css"  data-template-style="true"  rel="stylesheet" />
                     <link href="/bitrix/templates/.default/css/fontawesome-list-styles.css?16119052421747" type="text/css"  data-template-style="true"  rel="stylesheet" />
                     <link href="/bitrix/templates/.default/components/bitrix/system.auth.form/learning_auth/style.css?16043974328949" type="text/css"  data-template-style="true"  rel="stylesheet" />
                     <link href="/bitrix/panel/main/popup.min.css?145277744920704" type="text/css"  data-template-style="true"  rel="stylesheet" />
                     <link href="/bitrix/templates/.default/components/bitrix/learning.course.list/course_dropdown_list/style.css?15263820004556" type="text/css"  data-template-style="true"  rel="stylesheet" />
                     <link href="/bitrix/components/bx/learning.course.comment.button/templates/.default/style.css?1526382000784" type="text/css"  data-template-style="true"  rel="stylesheet" />
                     <link href="/bitrix/templates/learning_new_2014/styles.css?152638200079" type="text/css"  data-template-style="true"  rel="stylesheet" />
                     <link href="/bitrix/templates/learning_new_2014/template_styles.css?162211549129371" type="text/css"  data-template-style="true"  rel="stylesheet" />
     
     	</head>
   ```

Хороший код - редко используемые и очень однообразные вещи вынесены в отдельный файл


```plaintext
	<?$APPLICATION->ShowHead();?>
	
	<title><?$APPLICATION->ShowTitle();?></title>
	
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />

	<?include_once('include/head.php');?>

</head>
```

5. Правильная организация файлов вложенных страниц

Недостаточно просто сложить все файлы от всех страниц в отдельную папку, как здесь 

`local/templates/site/chunks/inner/where.buy.desc.light.php`
`local/templates/site/chunks/inner/where.buy.desc.middle.php`
`local/templates/site/chunks/inner/about.logo.php`
`local/templates/site/chunks/inner/vacancy.list.php`
`local/templates/site/chunks/inner/main.page.slider.php`


Таким образом папка превращается в мусорку, это кошмар. Если в проекте 5 файлов, это еще куда ни шло, но если это большой проект, то это становится проблемой. Лучше создать дополнительные каталоги. 

`local/templates/site/chunks/where-buy/where.buy.desc.light.php`
`local/templates/site/chunks/where-buy/where.buy.desc.middle.php`
`local/templates/site/chunks/about/about.logo.php`
`local/templates/site/chunks/job/vacancy.list.php`
`local/templates/site/chunks/main/main.page.slider.php`


## Свод простых правил

 1. Разделение логики и представления

    Бизнес-логика в приложении должна отсутствовать в шаблоне файла.

    То есть, любые обращения к БД, расчёты денег или операции, не задействованные напрямую при рендере template.php необходимо выносить в result_modifier.php или в собственный компонент
 2. Разделение логики между компонентами

    Один компонент / класс / файл не должен решать больше одной задачи. Совершенно недопустима ситуация, когда один класс склоняет падежи, работает с банком и делает запрос на почтовую подписку. Это ведет к тому, что весь незначительный функционал будет складываться в этот класс, а с течением времени огромная часть проекта переедет в этот класс. Поддерживать такое невозможно.

    Как решать такие задачи, когда существует функция, которую, казалось бы, некуда отнести?

    Создавать классы-хелперы. Например, есть класс, склоняющий падежи, работающий с числами и выполняющий прочие "довольно абстрактные" вещи. Это может быть класс **base_helper.php**.

    Если есть функция расчёта платежа по кредиту, которая по каким-то причинам не может быть реализована в классе платёжной системы банка, то стоит завести класс **bankname_helper.php**. Вполне может сложиться ситуация, когда в этом хелпере появятся дополнительные функции, уже для расчёта графика платежей, например.
 3. Используйте значимые и произносимые имена переменных

    `$ymdstr = $moment->format('y-m-d'); //плохо. Непонятно, что хранит эта переменная`

    `$currentDate = $moment->format('y-m-d'); //хорошо. Понятно, что это текущая дата`
 4. Аргументы функций (в идеале два или меньше)

    Функцию с большим количеством аргументов сложно тестировать, слишком много различных вариантов поведения
 5. Функции должны делать что-то одно

    Одна функция - это одна функция. Плохо, когда одна функция делает расчёт баланса, регистрацию пользователя и возвращает номер телефона клиента. При изменении функционала это превращается в нечитаемую простыню непонятных действий
 6. Уберите дублирующий код
 7. Не используйте флаги в качестве параметров функций (true/false, black/white, mode = test / prod)
 8. Убирайте мёртвый код

    Он плох так же, как и дублирующий код. Не нужно держать его в кодовой базе. Если что-то не вызывается, избавьтесь от этого! Если что, мёртвый код можно будет достать из истории версий.
 9. If/else vs Switch/case

    If/else - это проверка, switch/case - выбор альтернатив
10. Разный стиль именования переменных, имён функций и полей API

    `$bxName`

    `$BXNAME`

    `$bx_name`

    `$bxname`

    Всё это разные стили именования. Пожалуйста, придерживайтесь одного стиля на протяжении всей кодовой базы.
11. Длина тела функции должна вмешаться в один экран монитора

    Это правило не является на 100% обязательным, как и многие другие, однако если тело функции состоит из 100 и более строк, то стоит пересмотреть код, наверняка что-то можно упросить, вынести в подфункции или вообще выбросить. Короткое тело функции позволяет быстро понять, что происходит и как добавить/исправить функционал.

Полезные ссылки

**ОБЯЗАТЕЛЬНО ДЛЯ ОЗНАКОМЛЕНИЯ**

> [https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=43&LESSON_ID=2815&LESSON_PATH=3913.2815](https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=43&LESSON_ID=2815&LESSON_PATH=3913.2815)

**Правила хорошего кода на PHP.**

> [https://habr.com/ru/company/mailru/blog/336788/](https://habr.com/ru/company/mailru/blog/336788/)

**Болванка для создания собственных компонентов в ООП-стиле**

> [https://gitlab.di-house.ru/b2c/bitrix-component-start-template](https://gitlab.di-house.ru/b2c/bitrix-component-start-template)

**Про форматирование кода, не обязательно к прочтению**

[https://habr.com/ru/company/badoo/blog/232133/](https://habr.com/ru/company/badoo/blog/232133/)