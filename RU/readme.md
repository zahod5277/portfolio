# Редькин Дмитрий
# Team-lead, senior fullstack-developer

#### Коротко
Могу-умею:

* PHP 5/7/8, PHPUnit, Codeception, Slim, Bitrix, MODX, MySQL, Laravel 
* Kafka, Docker, Linux (CentOS, Ubuntu)
* React, TypeScript
* HTML+JS+CSS, Gulp, Pug, SCSS 
* Node.js
* GIT, PHPStorm 
* тимлидство, писательство, коммуникация с бизнесом

-----
#### Подробно
Опыт программирования с 2007 года (Delphi -> HTML+PHP+JS -> Matlab -> Python -> PHP+JS)

* Опыт коммерческой разработки с 2015 года
* Опыт работы в удалённых командах с 2016-ого года
* Создал более 100 сайтов под ключ различной степени сложности, оптимизировал и отрефакторил километры кода, спас нервы и кошельки десяткам клиентов на огромную кучу денег
* Получаю респекты от бизнеса на миллионы $$$ - Nokia, Apple и NVIDIA
* Руководил командой из нескольких разработчиков, вместе с которой мы запустили крутой проект на Битриксе с Кафкой и 1С-кой
* Общаюсь с бизнесом на одном языке

Бóльшая часть разработанных сайтов и красивенькая информация обо мне [на моём личном сайте](http://phpslave.ru/). 
**Похвали меня там!** 

Собственно, ниже кейсы

### Реализованные проекты

#### 2024

**Пет проект на больничном - телеграм бот для VPN**

[Ссылка](https://bitbucket.org/zahod5277/portfolio/src/master/RU/TGBot/)

**Тестовое задание в нидерландскую компанию на Fullstack Developer (VUE + PHP)**

[Ссылка](https://bitbucket.org/zahod5277/portfolio/src/master/RU/Sterc/)

Успешно пройдено с получением оффера

##### 2023

**Orion Senior Software Engineer**

[Тестовое задание](https://bitbucket.org/zahod5277/portfolio/src/master/RU/orion/)

Успешно пройдено с последующим трудоустройством

Остальная работа защищена NDA.

#### 2022

**diHouseExchange** 

[Чуть-чуть похожего кода оттуда](https://bitbucket.org/zahod5277/portfolio/src/master/RU/Api/)

REST API между 1С ERP и сайтами на базе 1C Bitrix. 

Стек: PHP 7.4, Slim 4, Bitrix, PHPUnit, Docker, Kafka

Роль в проекте: архитектор, тимлид, разработчик.

Спроектировал логику работы, разработал JSON API, разработал систему классов, установил и настроил Kafka, научил всю команду писать тесты и пользоваться докером.
Написал документацию, защитил перед бизнесом, запустил в продакшн. 
Как результат - настоящий, полноценный API, выполняющий сотни параллельных операций в секунду (обмен номенклатурой, ценами, стоками и прочей информацией)

**miEco**

Доработки движка 1С Битрикс для сверхбыстрой разработки интернет-магазинов.

Стек: PHP 7.4, 1C Bitrix, JavaScript, Gulp, SCSS, PUG

Роль в проекте: архитектор, тимлид, разработчик

Разработал способы оптимизации разработки сайтов на Битрикс, позволяющие сократить разработку одного интернет-магазина до трёх-пяти рабочих дней.
Серьёзно, даже человек без опыта в Битриксе смог развернуть такой магазин за 5 дней. 

Что было сделано:
* Установлен TWIG в качестве шаблонизатора. Позволил сделать большинство блоков компонентов практически идентичными, только теги расставь!
* Разработана сборка Gulp и регламент разработки фронтенда для подрядчиков, который жестко регламентирует код-стайл, из-за чего упрощается переключение с проекта на проект, тем самым проще в поддержке и доработке.
* Разработан модуль RichContent, своего рода визуальный конструктор страниц без ограничений. Вёрстка программируется в twig-чанки, в админке существует специальный интерфейс и при рендере всё это дело парсится и выводится.   

#### 2021

**Начало работы тимлидом отдела WEB-разработки**

- Создал рабочую инфраструктуру - закупка PHPStorm, настроил тестовые и боевые серверы для работы (CentOS и Ubuntu), заказ необходимого ПО
- Настроил систему быстрой установки сайтов на базе Docker [мой форк BitrixDock здесь. Там даже инструкция для умственно отсталых имеется.](https://github.com/dihouse-redkin/bitrixdock)
- Установил GitLab на сервер компании и настроил командную работу между сотрудниками компании и подрядчиками (с инструкциями и правилами)
- Разработал регламенты разработки front-end и back-end
- Технические собеседования кандидатов на Senior, Middle и Junior разработчиков
- Приём задач от CTO и бизнеса, планирование сроков и ресурсов
- Совещания в Zoom, Битрикс, Cisco Webex, Skype и т.п. в том числе на английском

**Nokia Russia Mobile Shop**

[Официальный магазин Nokia Mobile](https://mobileshop.nokia.ru/)

(закрыт в связи с уходом компании с российского рынка)

Разработка крупного интернет-магазина для известного бренда.

Стек: 1C Битрикс. 

Роль в проекте: разработчик, QA (приём работы подрядчика, тестирование)

Разработка нестандартных модулей:
* Первый в РФ модуль онлайн trade-in
* Рассрочки Сбербанк, Тинькофф, ВсегдаДа
* Экспресс-доставка по геолокации (так и не успели заюзать, т.к. запустили 23 февраля)
* Интеграция с 1С (передача номенклатуры, цен, остатков, заказов)
* Разработка REST API для партнёров (передача информации по заказам в HMD Global Oy)

### 2017 - 2020

### MODX

- [Arny Praht](https://bitbucket.org/zahod5277/portfolio/src/master/RU/Arny%20Praht/). Очень крутой [интернет-магазин](https://arnypraht.com/) с оборотом более 200 млн.руб./год. В Readme файле в папке - подробная информация.
- [Product Ext Warranty](https://bitbucket.org/zahod5277/portfolio/src/master/RU/Product%20Ext%20Warranty/) - реализация функционала "Дополнительная гарантия на устройство". 
    MODX API + 1C УТ API + JWT + AES 128
- [Qustick.ru](https://bitbucket.org/zahod5277/portfolio/src/master/RU/Qustick/) - мой личный pet-project, маркетплейс музыкальных инструментов.     
- [milanaCRM](https://bitbucket.org/zahod5277/portfolio/src/master/RU/milanaCRM/) - микро-CRM система по заказу фабрики трикотажа из Иваново
- [YandexCheckout](https://bitbucket.org/zahod5277/portfolio/src/master/RU/YandexCheckout/) - модуль оплаты Яндекс.Кассой для движка магазина MODX MiniShop2 (инфо по задаче внутри)

### 1C Bitrix

- [Официальный магазин Nokia Mobile](https://mobileshop.nokia.ru/)  
- [BitrixMultiprop](https://bitbucket.org/zahod5277/portfolio/src/master/RU/BitrixMultiprop/) - модуль для 1С Битрикс, представляющий собой класс-обработчик свойств инфоблока
- [docs/backend-bitrix.md](https://bitbucket.org/zahod5277/portfolio/src/master/RU/docs/backend-bitrix.md) - разработанный мною регламент разработки на 1С Битрикс

### Laravel

- [Laravel](https://bitbucket.org/zahod5277/portfolio/src/master/RU/Laravel/) - поддержка маленького сайта на Ларе, дописал пару методов и заюзал компонент SEO

### Slim

- [Slim](https://bitbucket.org/zahod5277/portfolio/src/master/RU/Slim/) - реализация REST API для Nokia и NVIDIA на фреймворке SLIM. API возвращает данные по заказам, товарам и т.п.


### PHP NATIVE

- [sknt-test-case](https://bitbucket.org/zahod5277/portfolio/src/master/RU/sknt-test-case/) - тестовое задание для Skynet (успешно пройдено с дальнейшим трудоустройством)
- [smetter-test-case](https://bitbucket.org/zahod5277/portfolio/src/master/RU/smetter-test-case/) - тестовое задание для Smetter.ru (успешно пройдено)

### Other

- [scripts](https://bitbucket.org/zahod5277/portfolio/src/master/RU/scripts/) - папка с интересными скриптами, которые были выполнены в разное время

### Info

- [docs/testcases](https://bitbucket.org/zahod5277/portfolio/src/master/RU/docs/testcases/) - тестовые задания, разработанные мною для оценки качества знаний прибывающих сотрудников
- [docs/fronted.md](https://bitbucket.org/zahod5277/portfolio/src/master/RU/docs/frontend.md) - разработанный мною регламент разработки фронтенда на HTML + БЭМ + SCSS + jQuery
- [docs/skills.md](https://bitbucket.org/zahod5277/portfolio/src/master/RU/docs/skills.md) - Мои навыки и умения
 


> P.S. Холи щит, сколько же всего я здесь написал!