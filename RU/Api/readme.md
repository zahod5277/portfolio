#REST API модуль для 1С Битрикс

#### Здесь приведена довольно условная структура, просто наброски кода, похожие на рабочий проект, но таковыми не являющимися. 

ApiHandler.php - родительский класс каждой сущности API. Реализует базовые методы - process и response

ApiVendor.php - пример дочерней класса ApiHandler, который работает уже непосредственно с элементами API.

KafkaConsumer.php - пример консмьюмера для Kafka, который потребляет данные и отправляет их в API

Test - примеры тестов для PHPUnit. Очень упрощенно и немножк.  