#!/usr/bin/php
<?php
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define("BX_CAT_CRON", true);
define('NO_AGENT_CHECK', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\Loader::includeModule('api');

$config = new \Api\Config([]);

$broker = $config->options['KAFKA_CONNECTION'];
$topic_name = $config->options['VENDORS_TOPIC'];

if ($broker == '' && $topic_name == ''){
    die('Не указаны параметры подключения!'.PHP_EOL);
}

$conf = new RdKafka\Conf();

$conf->set('group.id', 'group_id');

$rk = new RdKafka\Consumer($conf);
$rk->addBrokers($broker);

$topicConf = new RdKafka\TopicConf();
$topicConf->set('auto.commit.interval.ms', 100);

$topicConf->set('auto.offset.reset', 'smallest');

$topic = $rk->newTopic($topic_name, $topicConf);
$topic->consumeStart(0, RD_KAFKA_OFFSET_STORED);

while (true) {
    $message = $topic->consume(0, 120 * 10000);
    switch ($message->err) {
        case RD_KAFKA_RESP_ERR_NO_ERROR:
            if (isset($message->payload) && $message->payload != '') {
                $data = json_decode($message->payload, 1);

                $site_id = $config->getSiteByShopId($data['shopId']);
                $config->set('IBLOCK_ID', $config->options[$site_id . '__VENDORS_IBLOCK_ID']);

                $vendors = Api\Main::$container->make('Api\Vendors', [
                    'cfg' => $config
                ]);
                $vendors->process($data);

                $topic->offsetStore(0, $message->offset);
                unset($vendors);
            }
            break;
        case RD_KAFKA_RESP_ERR__PARTITION_EOF:
            echo "No more messages; will wait for more\n";
            break;
        case RD_KAFKA_RESP_ERR__TIMED_OUT:
            echo "Timed out\n";
            break;
        default:
            throw new \Exception($message->errstr(), $message->err);
            break;
    }
}