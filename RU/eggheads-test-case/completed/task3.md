Никогда не работал с опцией only_full_group_by, понятия не имею что она делает :) 

Как будет возможность проверить это на БД, которую я смогу подправить, тогда возможно данный запрос изменится.

Я предполагаю, что здесь еще должны быть подзапросы, но у меня всё посчиталось исправно. 

```sql
SELECT users.`name`, users.`phone`, SUM(orders.`subtotal`) AS TOTAL_SUMM, AVG(orders.`subtotal`) AS AVERAGE_BILL, MAX(orders.`created`) AS LAST_ORDER FROM `users` users INNER JOIN `orders` orders ON (users.`id` = orders.user_id) GROUP BY users.id ORDER BY orders.`created` ASC;
```

p.s. Включил ONLY_FULL_GROUP_BY, пришлось немножко переписать запрос: 

```sql
SET GLOBAL sql_mode=(SELECT CONCAT(@@sql_mode, ',ONLY_FULL_GROUP_BY'));
SELECT users.`name`, users.`gender`, SUM(orders.`subtotal`) AS TOTAL_SUMM, AVG(orders.`subtotal`) AS AVERAGE_BILL, MAX(orders.`created`) AS LAST_ORDER FROM `y_users` users INNER JOIN `y_orders` orders ON (users.`id` = orders.user_id) GROUP BY users.id;
```