<?php

//пример файла, который реально работает
//структура таблиц лежит в файле dump.sql

$mysqli = new mysqli("db", "root", "rootroot", "redkin");

$catalog_id = 1;
$query = "SELECT questions.*, users.name as Username, users.gender as Gender FROM `y_questions` questions INNER JOIN `y_users` users ON (questions.user_id = users.id) WHERE catalog_id = {$catalog_id} ORDER BY users.id ASC";
$stmt = $mysqli->prepare($query);

$result = [];
$stmt->execute();

$db_result = $stmt->get_result();
while ($row = $db_result->fetch_assoc()){
    $result[] = [
        'question' => [
            'id' => $row['id'],
            'catalog_id' => $row['catalog_id'],
            'question' => $row['question']
        ],
        'users' => [
            'name' => $row['Username'],
            'gender' => $row['Gender']
        ]
    ];
}

echo '<pre>'.print_r($result, 1).'</pre>';