-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: db
-- Время создания: Дек 11 2022 г., 19:36
-- Версия сервера: 8.0.31
-- Версия PHP: 8.0.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `redkin`
--

-- --------------------------------------------------------

--
-- Структура таблицы `y_orders`
--

CREATE TABLE `y_orders` (
  `id` int NOT NULL,
  `subtotal` int NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `city_id` int NOT NULL,
  `user_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `y_orders`
--

INSERT INTO `y_orders` (`id`, `subtotal`, `created`, `city_id`, `user_id`) VALUES
(1, 1000, '2022-12-11 16:39:31', 1, 1),
(2, 10000, '2022-12-11 16:22:01', 2, 1),
(3, 2000, '2022-12-11 06:39:31', 2, 2),
(4, 2000, '2022-12-11 16:22:16', 2, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `y_questions`
--

CREATE TABLE `y_questions` (
  `id` int NOT NULL,
  `catalog_id` int NOT NULL,
  `user_id` int NOT NULL,
  `question` varchar(255) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `y_questions`
--

INSERT INTO `y_questions` (`id`, `catalog_id`, `user_id`, `question`) VALUES
(1, 1, 1, 'Сколько звёзд на небе?'),
(2, 1, 2, 'Во сколько кукарекают петухи?'),
(3, 1, 3, 'Сколько крошек в хлебе?'),
(4, 1, 2, 'Сколько человек работает в eggheads?');

-- --------------------------------------------------------

--
-- Структура таблицы `y_users`
--

CREATE TABLE `y_users` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `gender` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Дамп данных таблицы `y_users`
--

INSERT INTO `y_users` (`id`, `name`, `gender`) VALUES
(1, 'Васян', 1),
(2, 'Евгения', 2),
(3, 'Дмитрий', 1),
(4, 'Алиса', 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `y_orders`
--
ALTER TABLE `y_orders`
  ADD UNIQUE KEY `id` (`id`);

--
-- Индексы таблицы `y_questions`
--
ALTER TABLE `y_questions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `y_users`
--
ALTER TABLE `y_users`
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `y_orders`
--
ALTER TABLE `y_orders`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `y_questions`
--
ALTER TABLE `y_questions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `y_users`
--
ALTER TABLE `y_users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
