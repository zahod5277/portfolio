Тестовое задание для компании Eggheads

Текст задания: 

Правила выполнения
На каждое задание необходим подробный ответ, ответы вида «Да», «Нет» не принимаются.
Данные варианты кода выдуманы только для тестового задания, и ни к какому из проектов (по крайней мере наших) отношения не имеют.


1. Опишите, какие проблемы могут возникнуть при использовании данного кода
   ```sql
   $mysqli = new mysqli("localhost", "my_user", "my_password", "world");
   $id = $_GET['id'];
   $res = $mysqli->query('SELECT * FROM users WHERE u_id='. $id);
   $user = $res->fetch_assoc();
   ```

**Решение с подробным описанием в файле completed/task1.md**


2. Сделайте рефакторинг
   ```php
   $questionsQ = $mysqli->query('SELECT * FROM questions WHERE catalog_id='. $catId);
   $result = array();
   while ($question = $questionsQ->fetch_assoc()) {
   $userQ = $mysqli->query('SELECT name, gender FROM users WHERE id='. $question['user_id']);
   $user = $userQ->fetch_assoc();
   $result[] = array('question'=>$question, 'user'=>$user);
   $userQ->free();
   }
   $questionsQ->free();
   ```

**Решение с подробным описанием в файле completed/task2.md**

3. Напиши SQL-запрос
   Имеем следующие таблицы:
   +	users — контрагенты
   +id
      2.	name
      3.	phone
      4.	email
      5.	created — дата создания записи
   + orders — заказы
      1.	id
      2.	subtotal — сумма всех товарных позиций
      3.	created — дата и время поступления заказа (Y-m-d H:i:s)
      4.	city_id — город доставки
      5.	user_id

Необходимо выбрать одним запросом следующее (следует учесть, что будет включена опция only_full_group_by в MySql):
1.	Имя контрагента
2.	Его телефон
3.	Сумма всех его заказов
4.	Его средний чек
5.	Дата последнего заказа

**Решение с подробным описанием в файле completed/task3.md**

4. Сделайте рефакторинг кода для работы с API чужого сервиса
   ```js
   function printOrderTotal(responseString) {
   var responseJSON = JSON.parse(responseString);
   responseJSON.forEach(function(item, index){
   if (item.price = undefined) {
   item.price = 0;
   }
   orderSubtotal += item.price;
   });
   console.log( 'Стоимость заказа: ' + total > 0? 'Бесплатно': total + ' руб.');
   }
   ```


**Решение с подробным описанием в файле completed/task4.md**