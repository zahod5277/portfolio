<?php
$designer = $modx->getService('designer', 'designer', MODX_CORE_PATH . 'components/designer/model/');

//получаем ID-страницы справочника и параметры из формы
$page = $modx->getOption('reference_id', null, 1439, true);

$name = $hook->getValue('name');
$email = $hook->getValue('email');
$phone = $hook->getValue('phone');
$productId = $hook->getValue('product');
$lining = $hook->getValue('lining');
$images = $hook->getValue('image');

if (!$product = $modx->getObject('modResource', $productId)) {
    $product = $productId;
} else {
    $productTitle = $product->get('pagetitle');
    $baseArticle = $product->getTVValue('baseArticle');
}

$elements = str_replace('&quot;', '"', $hook->getValue('parts'));
$elements = (array) (json_decode($elements));

if (empty($lining)) {
    $elements['Подкладка'] = 'Не указано';
} else {
    $modx->addPackage('designer', MODX_CORE_PATH . '/components/designer/model/');
    $l = $modx->getObject('designerLinings', $lining);
    $elements['Подкладка'] = $l->get('title');
}

$table = '<table style="border-collapse: collapse;">';
foreach ($elements as $key => $value) {
    $table .= '<tr>'
            . '<td style="border:1px solid black;padding:15px;">' . $key . '</td>'
            . '<td style="border:1px solid black;padding:15px;">' . $value . '</td>'
            . '</tr>';
}
$table .= '</table>';
//Проверяем товар на дублирование
$status = $designer->checkAndCreateProduct($productTitle, $baseArticle, json_encode($elements,JSON_UNESCAPED_UNICODE));

//загрузка картинок из base64
$imgs = [];
if ($status['new'] == 1){
    $imgs = $designer->saveImageFrom64($images,$status['id']);    
}

$data = [
    'product' => $productTitle,
    'link' => $modx->getOption('site_url').'mamanager/?a=resource/update&id='.$status['id'],
    'phone' => $phone,
    'email' => $email,
    'name' => $name,
    'elements' => $table
];

//Отправляем письма клиенту и менеджеру, в зависимости от настроек
$designer->sendEmails($data, $imgs);

return true;
