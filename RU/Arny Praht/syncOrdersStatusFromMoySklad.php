<?php

define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/index.php';

$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');
$modx->error->message = null;

$login = $modx->getOption('sync_moysklad_api_user') . ':' . $modx->getOption('sync_moysklad_api_password');

//массив с временными интервалами проверки статусов заказов. 
//если в первый интервал ничего не произошло, обращаемся к следующуему. 
//Можно было написать красиво и рекурсивно, но я криворукий, 25 лет,
// а до сих пор ни одной рекурсии не написал

$today = date('Y-m-d H:i:s', time() - 5 * 3600);

$week = new DateTime(); // For today/now, don't pass an arg.
$week->modify("-7 day");
$week = $week->format("Y-m-d H:i:s");

$times = [
    $today,
    $week
];

//выборка заказов
$offset = 0;
$limit = 5;

foreach ($times as $time) {
    $datatime = urlencode($time);
    $url = 'https://online.moysklad.ru/api/remap/1.1/entity/customerOrder?updatedFrom=' . $datatime . '&offset=' . $offset . '&limit=' . $limit;
    $orders = getMoySkladRequest($url, $login);
    if (!empty($orders->rows))
        break;
}

if (empty($orders->rows)) {
    die('Вообще ничего нового не произошло');
}


for ($i = 0; $i < count($orders->rows); $i++) {
    $code = $orders->rows[$i]->externalCode;
    $statusLink = $orders->rows[$i]
            ->state
            ->meta->href;
    $moySkladStatus = getMoySkladRequest($statusLink, $login);
    $moySkladStatusName = $moySkladStatus->name;

    if ($order = $modx->getObject('msOrder', $code)) {
        $statusId = $order->get('status');
        $status = $modx->getObject('msOrderStatus', $statusId);
        $statusName = $status->get('name');
        if ($statusName != $moySkladStatusName) {
            $newStatus = $modx->getObject('msOrderStatus', [
                'name' => $moySkladStatusName
            ]);
            $order->set('status', $newStatus->get('id'));
            $order->save();
        }
    }
}

function getMoySkladRequest($url, $login) {

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_USERPWD, $login);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $answer = json_decode(curl_exec($ch));
    return $answer;
}
