<?php

class designer {

    /** @var modX $modx */
    public $modx;

    /** @var config $ms2 */
    public $config;

    /** @var miniShop2 $ms2 */
    public $ms2;

    /**
     * @param modX $modx
     * @param array $config
     */
    function __construct(modX &$modx, array $config = []) {
        $this->modx = & $modx;

        $this->config = array_merge([
            'products_folder' => $this->modx->getOption('designer_products_folder'),
            'template' => $this->modx->getOption('designer_products_template'),
            'mail_subject' => $this->modx->getOption('designer_order_mail_subject'),
            'mail_from' => $this->modx->getOption('designer_mail_from', null, 'reserveMailFrom', $this->modx->getOption('emailsender'), true),
            'mail_to' => $this->modx->getOption('designer_mail_to', null, 'zahod5277@mail.ru'),
            'tv' => $this->modx->getOption('designer_product_options_tv'),
            'mail_to_manager_chunk' => $this->modx->getOption('designer_order_email_to_manager_chunk'),
            'mail_to_client' => $this->modx->getOption('designer_order_mail_to_client', null, 1),
            'mail_to_client_chunk' => $this->modx->getOption('designer_order_email_to_client_chunk'),
            'mail_to_client_subject' => $this->modx->getOption('designer_order_email_to_client_subject'),
                ], $config);

        $ctx = isset($this->config['ctx']) ? $this->config['ctx'] : $this->modx->context->key;

        if (!$this->ms2 = $this->modx->getService('minishop2')) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, '[Designer] Requires installed miniShop2.');

            return false;
        }
        $this->ms2->initialize($ctx);
    }

    public function checkAndCreateProduct($title, $article, $elements) {

        $tv = $this->config['tv'];

        if ($tv = $this->modx->getObject('modTemplateVarResource', ['value' => $elements, 'tmplvarid' => $tv])) {
            $status = [
                'new' => 0,
                'id' => $tv->get('contentid')
            ];
            return $status;
        }

        $productData = [
            'pagetitle' => $title,
            'class_key' => 'msProduct',
            'parent' => $this->config['products_folder'],
            'alias' => microtime(true),
            'template' => $this->config['template'],
            'article' => $article,
            'tvs' => true,
            'tv' . $this->config['tv'] => $elements
        ];
        $this->modx->log(modX::LOG_LEVEL_ERROR, $elements);
        $response = $this->modx->runProcessor('resource/create', $productData);
        //TODO сделать что-то вроде вывода ошибок!
        if ($response->isError()) {
            return false;
        }

        $product = $response->getObject();
        $id = $product['id'];
        $status = [
            'new' => 1,
            'id' => $id
        ];
        return $status;
    }

    public function saveImageFrom64($images, $id) {
        $imgs = [];
        foreach ($images as $image) {

            if (empty($image))
                break;

            $data = substr($image, strpos($image, ","));
            $data = base64_decode($data);

            //супер функция для сохранения картинок в файлы
            $name = MODX_ASSETS_PATH . "images/designer/products/arnypraht-" . microtime(true) . $i . '.png';
            file_put_contents($name, $data);

            $imgs[] = array(
                'data' => $data,
                'filename' => $name,
                'encoding' => "base64",
                'type' => "image/png",
                'file' => $name
            );

            $response = $this->modx->runProcessor('gallery/upload', array(
                'id' => $id,
                'name' => basename($name),
                'file' => $name
                    ), array('processors_path' => MODX_CORE_PATH . '/components/minishop2/processors/mgr/')
            );

            if ($response->isError()) {
                return false;
            }
        }
        return $imgs;
    }

    public function sendEmails($data, $imgs) {
        $to = $this->config['mail_to'];
        $subject = $this->config['mail_subject'];
        $chunk = $this->config['mail_to_manager_chunk'];
        $this->sendEmail($to, $subject, $chunk, $data, $imgs);
        if ($this->config['mail_to_client'] == 1) {
            $to = $data['email'];
            $subject = $this->config['mail_to_client_subject'];
            $chunk = $this->config['mail_to_client_chunk'];
            $this->sendEmail($to, $subject, $chunk, $data, []);
        }
        return true;
    }

    public function sendEmail($to, $subject, $chunk, $data, $imgs) {
        if (!isset($to))
            return false;

        $this->modx->getService('mail', 'mail.modPHPMailer');
        $this->modx->mail->set(modMail::MAIL_FROM, $this->config['mail_from']);
        $this->modx->mail->set(modMail::MAIL_FROM_NAME, $this->modx->getOption('site_name'));

        //тема письма
        $this->modx->mail->set(modMail::MAIL_SUBJECT, $subject);

        $this->modx->mail->address('to', $to);
        $this->modx->mail->set(modMail::MAIL_BODY, $this->modx->getChunk($chunk, [
                    'product' => $data['product'],
                    'phone' => $data['phone'],
                    'email' => $data['email'],
                    'name' => $data['name'],
                    'elements' => $data['elements'] ? $data['elements'] : ''
                ]), $data);

        /* Отправляем */
        $this->modx->mail->setHTML(true);
        if (($imgs != '') || (!empty($imgs))) {
            foreach ($imgs as $img) {
                $this->modx->mail->mailer->AddStringAttachment($img['data'], $img['filename'], $img['encoding'], $img['type']);
            }
        }

        if (!$this->modx->mail->send()) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, 'An error occurred while trying to send the email: ' . $this->modx->mail->mailer->ErrorInfo);
        }
        $this->modx->mail->reset();
        return true;
    }

}
