<?php
header("HTTP/1.0 200 OK");
define('MODX_API_MODE', true);
/** @noinspection PhpIncludeInspection */

require dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/index.php';
/** @var modX $modx */
$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

/** @var miniShop2 $miniShop2 */
$miniShop2 = $modx->getService('miniShop2');

$corePath = MODX_CORE_PATH . 'components/superwarranty/model/';
$sw = $modx->getService('superWarranty','superWarranty',$corePath);

$sw->secondNotification();