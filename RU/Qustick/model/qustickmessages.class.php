<?php

class qustickMessages
{
    /** @var modX $modx */
    public $modx;

    /** @var miniShop2 $ms2 */
    public $ms2;

    /** @var pdoTools $pdo */
    public $pdo;

    /** @var Tickets $tickets */
    public $tickets;

    /** @var int $auth_user */
    public $auth_user = 0;

    /**
     * @param modX $modx
     * @param array $config
     */
    function __construct(modX &$modx, array $config = [])
    {
        $this->modx =& $modx;
        $this->isAjax = false;
        $this->error = false;
        $corePath = MODX_CORE_PATH . 'components/qustick/';
        $assetsUrl = MODX_ASSETS_URL . 'components/qustick/';
        $this->pdo = $this->modx->getService('pdoTools');
        $this->config = array_merge([
            'actionUrl' => $assetsUrl . 'rest.php',
            'corePath' => $corePath,
            'modelPath' => $corePath . 'model/',
            'assetsUrl' => $assetsUrl,
            'cssUrl' => $assetsUrl . 'css/',
            'jsUrl' => $assetsUrl . 'js/',
            'prefix' => $this->modx->getOption(xPDO::OPT_TABLE_PREFIX),
            'qustickMessagesJs' => $assetsUrl . 'js/qustickMessages.js',
            'reportsUrl' => MODX_BASE_PATH . 'reports/',
            'unauthorized_page' => $this->modx->getOption('unauthorized_page'),
            'messages_list_page_id' => $this->modx->getOption('messages_list_page_id')
        ], $config);

        if (!$this->ms2 = $this->modx->getService('minishop2')) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, '[qustickMessages] Requires installed miniShop2.');

            return false;
        }

        if (!$this->tickets = $this->modx->getService('tickets')) {

            $this->modx->log(modX::LOG_LEVEL_ERROR, '[qustickMessages] Requires installed Tickets.');
            return false;
        }

        $this->ms2->initialize('web');
        if ($this->modx->user->isAuthenticated('web')) {
            $this->auth_user = $this->modx->user->toArray();
        }

        $this->modx->addPackage('qustickmessages', $this->config['modelPath']);
    }



    /** @param $user_id int */
    /** @return array */
    public function getMessagesList($user_id)
    {
        $sql = "SELECT * FROM `{$this->config['prefix']}tickets_threads` WHERE `name` LIKE 'message-{$user_id}-%' OR `name` LIKE 'message-%-{$user_id}'";
        $results = $this->modx->query($sql);
        $list = $results->fetchAll(PDO::FETCH_ASSOC);
        return $list;
    }


    /** @param string $path */
    /** @return mixed */
    public function checkRights($path)
    {
        $auth_page_url = $this->modx->makeUrl($this->config['unauthorized_page']);
        $path = array_filter($path);
        if ($path[0] == 'messages') {
            switch (count($path)) {
                case 1:
                    if ($this->modx->user->isAuthenticated('web')) {
                        return true;
                    } else {
                        return false;
                    }
                    break;
                case 2:
                    $users_ids = explode('-', $path[1]);
                    $users_ids = [$users_ids[1], $users_ids[2]];
                    if ($this->modx->user->isAuthenticated('web') && in_array($this->auth_user['id'], $users_ids) && $users_ids[0] < $users_ids[1]) {
                        return true;
                    } else {
                        $this->modx->sendRedirect($auth_page_url);
                    }
                    break;
                default:
                    $this->modx->sendRedirect($auth_page_url);
                    break;
            }
        }
    }

    /** @param $path string url_path */
    /** @param $user_id int */
    /** @return int */
    public function getUserMessageCompanionByAlias($path, $user_id)
    {
        $path = array_filter($path);
        $users_ids = explode('-', $path[1]);
        $users_ids = [$users_ids[1], $users_ids[2]];

        $diff = array_diff($users_ids, [$user_id]);


        if (!empty($diff) && count($diff) == 1) {
            $companion_id = $diff[array_key_first($diff)];
        } else {
            $companion_id = 0;
        }
        return $companion_id;
    }

    /** @param $comment array  comment object */
    /** @return string companion email */
    public function getUserMessageCompanionByThread($comment)
    {
        $comment_author = $comment['email'];
        $sql = "SELECT DISTINCT `email` FROM `{$this->config['prefix']}tickets_comments` WHERE `thread` = {$comment['thread']} AND `email` != '{$comment_author}'";
        $this->modx->log(1, print_r($sql, 1));
        $results = $this->modx->query($sql);
        $users_ids = $results->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($users_ids)) {
            $email = $users_ids[0]['email'];
        } else {
            $email = false;
        }
        return $email;
    }

    /** @param $to int user_id modUser */
    /** @return string */
    public function buildMessagesLink($to)
    {
        $ids = [$this->auth_user['id'], $to];
        sort($ids);
        return "message-{$ids[0]}-{$ids[1]}";
    }

    public function loadJs($objectName = 'qustickMessages')
    {
        if ($js = trim($this->config['qustickMessagesJs'])) {
            if (preg_match('/\.js/i', $js)) {
                $this->modx->regClientScript(str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $js));
            }
        }
        $config = $this->modx->toJSON(array(
            'assetsUrl' => $this->config['assetsUrl'],
            'actionUrl' => str_replace('[[+assetsUrl]]', $this->config['assetsUrl'], $this->config['actionUrl'])
        ));
        $objectName = trim($objectName);
        $this->modx->regClientScript(
            "<script type=\"text/javascript\">{$objectName}.initialize({$config});</script>", true
        );
    }
}