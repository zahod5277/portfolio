<?php

namespace Qustick\Utils;

class aliasGenerator
{
    /** @var modX $modx */
    public $modx;
    /** @var miniShop2 $ms2*/
    public $ms2;
    /** @var array $config */
    public $categories = [
        13 => 'Гитара',
        14 => 'Электрогитара',
        15 => 'Бас-гитара',
        16 => 'Акустическая гитара',
        17 => 'Акустическая бас-гитара',
        20 => 'Укулеле',
        18 => 'Народный инструмент',
        26 => 'Для гитары',
        60 => 'Примочка',
        89 => 'Усилитель',
        205 => 'Педалборд',
        206 => '',
        272 => 'Струны',
        19 => 'Клавишный инструмент',
        21 => 'Синтезатор',
        22 => '',
        23 => '',
        24 => 'Midi-клавиатура',
        25 => 'Акс',
        27 => 'Ударный инструмент',
        28 => 'Акустические ударные',
        29 => 'Цифровые ударные',
        30 => 'Тарелки',
        31 => 'Барабанные палочки',
        32 => 'Стойка',
        33 => 'Стул',
        34 => 'Тренировочный пэд',
        35 => 'Комплектующие',
        36 => 'Аксесс',
        37 => 'Перкуссионный инструмент',
        90 => 'Духовой инструмент',
        111 => 'Флейта',
        112 => 'Блокфлейта',
        113 => 'Саксофон',
        114 => 'Кларнет',
        115 => 'Труба',
        116 => 'Тромбон',
        117 => 'Духовой инструмент',
        118 => 'Электронный духовой инструмент',
        119 => 'Аксессуар для духового инструмента',
        91 => '',
        92 => 'Студийные мониторы/наушники',
        93 => 'Звуковая карта',
        94 => 'Устройство обработки звука',
        95 => 'Акустика для концертов',
        96 => 'Сабвуфер',
        97 => 'DJ оборудование',
        98 => 'Усилитель мощности',
        99 => 'Микшерный пульт',
        101 => 'Микрофон',
        100 => 'Аксесс',
        120 => 'Аксессуар для микрофона',
        126 => '',
        200 => 'Динамик',
        122 => 'Аудиотехника',
        123 => 'Акустическая система',
        124 => 'Виниловый проигрыватель',
        125 => 'Портативная акустика',
        102 => 'Наушники и гарнитуры',
        103 => 'Смычковый инструмент',
        105 => 'Скрипка',
        106 => 'Альт',
        107 => 'Виолончель',
        108 => 'Контрабас',
        109 => 'Смычок',
        110 => 'Аксессуар для смычковых инструментов',
        127 => 'Винил',
        121 => 'Народный/этнический инструмент',
        104 => '',
    ];

    private $ar_processor_path;

    /**
     * @param modX $modx
     * @param array $config
     */
    public function __construct(\modX $modx)
    {
        $this->modx = &$modx;
        $model_path = $this->modx->getOption('autoredirector_core_path', null, $this->modx->getOption('core_path') . 'components/autoredirector/') . 'model/';
        $this->modx->addPackage('autoredirector', $model_path);
        $this->ar_processor_path = array('processors_path' => $this->modx->getOption('autoredirector_core_path', null, $this->modx->getOption('core_path') . 'components/autoredirector/') . 'processors/');

        if (!$this->ms2 = $this->modx->getService('minishop2')) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, '[qustickMessages] Requires installed miniShop2.');
            return false;
        }
        $this->ms2->initialize('web');
    }

    /**
     * @param int $product_id
     * @return mixed
     */
    public function generateAlias($product_id)
    {
        $resource = $this->modx->getObject('msProduct', $product_id);
        $title = $resource->get('pagetitle').'-'.$resource->get('id');
        $res_parent = $resource->get('parent');

        if (isset($this->categories[$res_parent])){
            $title = $this->categories[$res_parent] . ' ' . $title;
        }

        $alias = $resource->cleanAlias($title);
        $alias = preg_replace("/(?![.=$'€%-])\p{P}/u", "", $alias);

        $old_uri = $resource->get('uri');
        $path = explode('/', $old_uri);

        if ($path[count($path)-1] == $alias){
            return 'УРЛЫ СХОДЯТСЯ ДЛЯ ' .$product_id;
        }

        if ($this->createRedirect($product_id, $old_uri, $alias)) {
            $this->modx->log(1, 'ALIAS SET TO '.$alias);
            $resource->set('alias', $alias);
            $resource->save();
        } else {
            return 'Ошибка создания редиректа';
        }
        return 'Success for '.$product_id;
    }

    /**
     * @param int $res_id
     * @param str $old_uri
     * @return bool
     */
    private function createRedirect($res_id, $old_uri)
    {
        $arRule = array('uri' => $old_uri, 'res_id' => $res_id);

        if (!$this->modx->getObject('arRule', $arRule)) {
            $response_ar = $this->modx->runProcessor('mgr/item/create', $arRule, $this->ar_processor_path);
            if ($response_ar->isError()) {
                $res = false;
            } else {
                $res = true;
            }
        }
        return $res;
    }
}