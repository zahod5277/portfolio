<?php

require_once MODX_CORE_PATH . 'model/modx/modprocessor.class.php';
require_once MODX_CORE_PATH . 'model/modx/processors/resource/create.class.php';
//$this->log(1, 'qustickMessageBoardCreateProcessor FILE IS INIT!!!');

class qustickMessageBoardCreateProcessor extends modObjectCreateProcessor
{
    public $permission = '';
    public $object;
    public $classKey = 'modResource';
    /**
     * @return bool
     */
    public function checkPermissions()
    {
        return true;
    }

}

return 'qustickMessageBoardCreateProcessor';
