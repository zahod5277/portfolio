<?php
if (!class_exists('qustickMultiPluginHandler')) {
    require_once MODX_CORE_PATH . 'components/qustick/pluginshandler/qustickmultipluginhandler.class.php';
}

/**
 * Class qustickUsersPluginHandler
 */
class qustickUsersPluginHandler extends qustickMultiPluginHandler
{
    public $groups = [
        'shop' => 'Shop',
        'users' => 'Users',
    ];

    public function OnPageNotFound()
    {
        $alias = $this->modx->context->getOption('request_param_alias', 'q');
        if (!isset($_REQUEST[$alias])) {
            return false;
        }

        $request = $_REQUEST[$alias];
        $tmp = explode('/', $request);
        // Ссылка подходит под заданный формат: user/username
        if ($tmp[0] == 'users') {
        //die(print_r($tmp,1));
        // Здесь будем определять, какую страницу надо показывать
        // Дальше проверяем наличие запрошенного пользователя
            $name = $tmp[1];
            if ($user = $this->modx->getObject('modUser', array('username' => $name))) {
                $this->modx->sendForward(62);
            }
        }

        // Ссылка подходит под заданный формат messages/message-#-##
        if ($tmp[0] == 'messages'){
            $users_ids = explode('-', $tmp[1]);
            $users_ids = [$users_ids[1], $users_ids[2]];
            if ($page_id = $this->checkMessagesUrl($tmp[1], $users_ids)){
                $this->modx->sendForward($page_id);
            }
        }

    }

    public function OnUserFormSave(){
        $profile = $this->user->getOne('Profile');
        if (!empty($_POST['city'])){
            $profile->set('city',$_POST['city']);
        }

        if (!empty($_POST['region'])){
            $profile->set('state',$_POST['region']);
        }

        if ($this->mode == 'new') {
            // Здесь мы добавляем выбранную группу
            $this->user->joinGroup($this->groups[$_POST['group']]);
            $username = $this->translitUrl($profile->get('fullname').'-'.$this->user->get('id'));
            $this->user->set('username', $username);
            $this->user->save();
        }

        $profile->save();
        return true;
    }

    protected function translitUrl($text)
    {
        $options = array(); 
        $translit = $this->modx->getOption('friendly_alias_translit', $options, 'russian');
        $translitClass = $this->modx->getOption('friendly_alias_translit_class', $options, 'translit.modTransliterate');
        $translitClassPath = $this->modx->getOption('friendly_alias_translit_class_path', $options, $this->modx->getOption('core_path', $options, MODX_CORE_PATH) . 'components/');
        $this->modx->getService('translit', $translitClass, $translitClassPath, $options);
        $url = $this->modx->translit->translate($text, $translit);
        $url = str_replace(' ','-',$url);
        $user_url = filter_var($url,FILTER_SANITIZE_URL);
        return $user_url;
    }

    public function onBeforeUserFormSave(){
        $p = $this->user->getOne('Profile');
        if ( $this->mode == 'new' ) {
            $exist = 0;
            $phone = preg_replace('/[^\d\+]+/', '', $p->get('mobilephone'));
            $phone = preg_replace('/^8([\d]{10})$/', '+7$1', $phone);
            if ( $phone != '' ) {
                if ( strlen($phone) > 10 ) {
                    $phone = substr($phone, 1);
                }
                if ( $u = $this->modx->getObject('modUserProfile', ['mobilephone:LIKE' => '%' . $phone . '%', 'internalKey:!=' => 0]) ) {
                    $exist = 1;
                }
                if ( $exist ) {
                    $this->user->set('mobilephone', '');
                    $p->set('mobilephone', '');
                    $this->modx->event->output('Пользователь с таким номером телефона уже зарегистрирован!');
                }
            }
        }
        if ($this->mode == 'upd'){
            $username = $this->user->get('username');
            if ($user = $this->modx->getObject('modUser',[
                'username' => $username,
                'id:!=' => $this->user->id
            ])){
                $this->modx->event->output('Пользователь с таким логином '.$username.' уже существует!');
            }
        }
    }

    /** @param string $url */
    /** @return mixed $page_id */
    private function checkMessagesUrl($url, $users_ids){
        $page_id = false;
        $url_pattern = '/\bmessage-\b\d{1,9}-\d{1,9}/';
        $matches = [];
        //Если адрес соответствует паттерну
        if (preg_match($url_pattern, $url, $matches) == true){
            //если страница для сообщений еще не создана
            if (!$page = $this->modx->getObject('modResource', ['alias' => $url])){
                //если первый ID юзера меньше, чем второй (условие единообразности УРЛов)
                //проверяем наличие юзеров в базе, и если ок, то создаём такую страницу сообщений
                if ($users_ids[0] < $users_ids[1] && $this->checkUsersExists($users_ids)){
                    $page_id = $this->createMessageBoard($url, $users_ids);
                }
            } else {
                $page_id = $page->get('id');
            }

        }
        return $page_id;
    }

    /** @param array $users */
    /** @return bool */
    private function checkUsersExists($users){
        $users_list = $this->getUsers($users);
        if (count($users) === count($users_list)){
            return true;
        }
        return  false;
    }

    /** @param array $users */
    /** @return array $fetch */
    private function getUsers($users){
        $users_lists = implode(',', $users);
        $sql = "SELECT * 
                FROM `{$this->config['prefix']}users` AS `users`
                LEFT JOIN `{$this->config['prefix']}user_attributes` AS `user_profile`
                ON (`users`.`id` = `user_profile`.`internalKey`)                   
                WHERE 
                    `users`.`id` IN ({$users_lists})";
        $results = $this->modx->query($sql);
        $fetch = $results->fetchAll(PDO::FETCH_ASSOC);
        return $fetch;
    }

    /** @param string $alias */
    /** @param array $users */
    /** return mixed */
    private function createMessageBoard($alias, $users){
        $users_list = $this->getUsers($users);

        $page_data = [
            'pagetitle' => 'Диалог пользователей '.$users_list[0]['fullname'].' и '.$users_list[1]['fullname'],
            'class_key' => 'modResource',
            'uri_override' => 0,
            'parent' => 71,
            'alias' => $alias,
            'template' => 15,
            'show_in_tree' => 1,
            'published' => 1,
        ];
        $response = $this->modx->runProcessor('create', $page_data, array('processors_path' => MODX_CORE_PATH . '/components/qustick/processors/qustick/'));

        if ($response->isError()) {
            $this->modx->log(1,'cant save the message page from message! '.$response->getMessage());
            return false;
        }

        $page = $response->getObject();
        return $page['id'];
    }
}
