<?php
if (!class_exists('qustickMultiPluginHandler')) {
    require_once MODX_CORE_PATH . 'components/qustick/pluginshandler/qustickmultipluginhandler.class.php';
}

/**
 * Class qustickInitPluginHandler
 */
class qustickInitPluginHandler extends qustickMultiPluginHandler
{

    public function OnMODXInit()
    {
        $this->modx->loadClass('msOrder');
        $this->modx->map['msOrder']['fields']['seller_id'] = 0;
        $this->modx->map['msOrder']['fieldMeta']['seller_id'] = array(
            'dbtype' => 'int',
            'precision' => 10,
            'attributes' => 'unsigned',
            'phptype' => 'integer',
            'null' => true,
            'default' => 0,
        );
        return true;
    }
}