<?php
if (!class_exists('qustickMultiPluginHandler')) {
    require_once MODX_CORE_PATH . 'components/qustick/pluginshandler/qustickmultipluginhandler.class.php';
}

/**
 * Class qustickCartPluginHandler
 */
class qustickCartPluginHandler extends qustickMultiPluginHandler
{
    /*
     * @return bool
     */
    public function msOnCreateOrder()
    {
        $orders = [];
        $order_id = $this->msOrder->get('id');

        $users_ids = $this->getOrderSellers($order_id);

        $this->notifySellers($users_ids);

        //creating new orders form original order
        if (count($users_ids) > 0) {
            foreach ($users_ids as $index => $user_id) {
                $products = $this->getOrderProductsByUserId($order_id, $user_id['createdby']);
                $products_cost = $this->getProductsCost($products);
                $delivery_cost = $this->msOrder->get('delivery_cost');
                $cart_cost = $products_cost;

                $order_params = [
                    'user_id' => $this->msOrder->get('user_id'),
                    'seller_id' => $user_id['createdby'],
                    'createdon' => $this->msOrder->get('createdon'),
                    'num' => $this->msOrder->get('num') . '-' . $user_id['createdby'],
                    'delivery' => $this->msOrder->get('delivery'),
                    'payment' => $this->msOrder->get('payment'),
                    'cart_cost' => $cart_cost,
                    'weight' => 0,
                    'delivery_cost' => $delivery_cost,
                    'cost' => $cart_cost,
                    'status' => 1,
                    'context' => 'web',
                ];
//                $this->modx->log(1,print_r($order_params,1));

                $order = $this->modx->newObject('msOrder');
                $order->fromArray($order_params);
                $order_address = $this->msOrder->getOne('Address');
                $address = $this->modx->newObject('msOrderAddress');
                $order->addOne($order_address);

                $order_products = [];
                foreach ($products as $product) {
                    $order_product = $this->modx->newObject('msOrderProduct');
                    $order_product->fromArray([
                        'product_id' => $product['id'],
                        'name' => $product['name'],
                        'count' => $product['count'],
                        'price' => $product['price'],
                        'weight' => $product['weight'],
                        'cost' => $product['25000.00'],
                        'options' => [],
                    ]);
                    $order_products[] = $order_product;
                }

                $order->addMany($order_products);

                if ($order->save()) {
                    //$this->modx->log(1, 'ORDERS SUCCESSUFYLYUOY SAVED');
                } else {
                    //$this->modx->log(1, 'ERROR');
                }

            }
        }
        $old_order = $this->modx->getObject('msOrder', $order_id);
        $old_order->set('status', $this->modx->getOption('qustickcrm_tech_status'));
        $old_order->save();

        return true;
    }

    /**
     * @param int $order_id
     * @return array $users_ids
     */
    private function getOrderSellers($order_id)
    {
        $sql = "SELECT DISTINCT 
                `products`.`createdby` 
                FROM `{$this->config['prefix']}site_content` AS `products` 
                LEFT JOIN `{$this->config['prefix']}ms2_order_products` AS `order_products` 
                ON (`products`.`id` = `order_products`.`product_id`) 
                WHERE `order_products`.`order_id` = {$order_id}";
        $results = $this->modx->query($sql);
        $users_ids = $results->fetchAll(PDO::FETCH_ASSOC);
        return $users_ids;
    }

    /**
     * @param int $order_id
     * @param int $user_id
     * @return array
     */
    private function getOrderProductsByUserId($order_id, $user_id)
    {
        $sql = "SELECT * 
                FROM `fzl_site_content` AS `products` 
                    LEFT JOIN `fzl_ms2_order_products` AS `order_products` 
                    ON (`products`.`id` = `order_products`.`product_id`) 
                WHERE 
                    `order_products`.`order_id` = {$order_id} 
                    AND 
                    `products`.`createdby` = {$user_id}";
        $results = $this->modx->query($sql);
        $fetch = $results->fetchAll(PDO::FETCH_ASSOC);

        return $fetch;
    }

    /**
     * @param array $products
     * @return float
     */
    private function getProductsCost($products)
    {
        //$this->modx->log(1, 'PR COST: ' . print_r($products,1));
        $cost = 0;
        foreach ($products as $product) {
            $cost += $product['cost'];
        }
        return $cost;
    }

    /**
     * @param array $users_ids
     * @return bool
     */
    private function notifySellers($users_ids)
    {
        foreach ($users_ids as $user_id) {
            if ($user = $this->modx->getObject('modUserProfile', $user_id['createdby'])) {
                //$this->modx->log(1, 'Отправлено письмо на почту: ' . $user->get('email'));
                $this->ms2->sendEmail($user->get('email'), 'Новый заказ на Кустике', 'У вас новый заказ. Авторизуйтесь в личном кабинете и перейдите в раздел управления заказами для получения подробной информации.');
            }
        }
        return true;
    }
}