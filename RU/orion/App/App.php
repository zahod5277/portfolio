<?php

namespace App;

//load App classes without class autoload
//task condition - no frameworks!
require_once 'Router.php';

class App
{
    public function handleRequest($request)
    {
        if (empty($request['api'])){
            $request['api'] = '/';
        }

        Router::route($request);
    }
}