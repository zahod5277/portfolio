<?php
namespace App;

require_once 'Controllers/Home.php';
require_once 'Controllers/City.php';
require_once 'Controllers/Weather.php';

class Router
{
    static function route($request){
        switch ($request['api']){
            case '/':
                $controller = 'Home';
                break;
            case 'suggest_city':
                $controller = 'City';
                break;
            case 'get_weather':
                $controller = 'Weather';
                break;
        }

        if (isset($controller) && $controller != ''){
            $controller = __NAMESPACE__."\Controllers\\".$controller;
            call_user_func(new $controller($request));
        }
    }
}