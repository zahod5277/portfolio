<?php

namespace App\Controllers;

require_once dirname(dirname(__FILE__)) . '/Controller.php';

class Weather extends Controller
{

    private string $endpoint;

    public function __construct($params)
    {
        $this->endpoint = 'https://api.open-meteo.com/v1/forecast?';
        parent::__construct($params);

    }

    public function __invoke(){
        $url = $this->endpoint
            .'latitude='.$this->params['params']['lat']
            .'&longitude='.$this->params['params']['long']
            .'&daily=precipitation_sum&timezone=Europe/Belgrade&current_weather=true';

        $data = json_decode(file_get_contents($url), 1);

        if (!empty($data)){
            $weather = [
                'city' => $this->params['params']['city'],
                'country' => $this->params['params']['country'],
                'temp' => $data['current_weather']['temperature'],
                'wind' => $data['current_weather']['windspeed'],
                'prec' => $data['daily']['precipitation_sum'][0],
                'prec_unit' => $data['daily_units']['precipitation_sum']
            ];
            return $this->render($weather, 'Weather.html');
        }

        return '';
    }
}