<?php
set_time_limit(500);
header('Content-Type: text/html; charset=utf-8');
define('MODX_API_MODE', true);
require 'index.php';

function redirect($url)
{
    Header("HTTP 302 Found");
    Header("Location: " . $url);
    die();
}

define('APP_ID', 'local.5ef766ac386983.31278xxx'); // take it from Bitrix24 after adding a new application
define('APP_SECRET_CODE', 'KzfzMTi5QXrXZ7f30kFD5dWKDpsHGfD1as85SSWiBJEbtX3xxx'); // take it from Bitrix24 after adding a new application
define('APP_REG_URL', 'http://site.ru/crm/groups.php'); // the same URL you should set when adding a new application in Bitrix24


$domain = isset($_REQUEST['portal']) ? $_REQUEST['portal'] : (isset($_REQUEST['domain']) ? $_REQUEST['domain'] : 'empty');

$btokenRefreshed = null;

$arScope = array('user');

// Авторизация
if (!isset($_REQUEST['code'])){
    requestCode($domain);
}
else {
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.14.2/dist/bootstrap-table.min.css">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.14.2/dist/bootstrap-table.min.js"></script>

    <link rel="stylesheet" href="/styles.css">

    <title>Мечтаево</title>
</head>
<body style="padding: 20px;">
<?
}

$defaultData = [
    'parent' => 1997,
    'template' => 78,
    'context_key' => 'web',
    'show_in_tree' => 0,
    'published' => 1,
];

$arAccessParams = requestAccessToken($_REQUEST['code'], $_REQUEST['server_domain']);

$hide_groups = [];
$hide_res = [];
$publish_groups = [];
$publish_res = [];
$tasks = [];

$tasks_response = executeREST($arAccessParams['client_endpoint'], 'tasks.task.list', array('filter' => ['TITLE' => 'SITE MAP'], 'SELECT' => 'ID'), $arAccessParams['access_token']);
$tasks = $tasks_response['result']['tasks'];
$log = '';
foreach ($tasks as $task) {
    $log .= 'Проверка ID '.$task['id'].' для страницы '.$task['ufAuto449593898901'].'<br>';
    if ($page = $modx->getObject('modTemplateVarResource', [
        'value' => $task['id'],
        'tmplvarid' => 199
    ])) {
        $log .= 'Страница существует, Обновляем данные - видео, координаты.<br><hr>';
        if ($task['status'] == 5) {
            $log .= 'Страница ' . $task['ufAuto449593898901'] . ' убрана с публикации<br><hr>';
            $page->set('published', 0);
            $page->save();
        }
        if ($task['ufAuto449593898901'] == '') {
            $log .= 'Страница ' . $task['ufAuto449593898901'] . ' без названия, пропускаем загрузку<br><hr>';
            continue;
        }
        if ($p = $modx->getObject('modResource',$page->get('contentid'))){
        if ($task['ufAuto334899357352'] != ''){
            $log .= 'Страница ' . $task['ufAuto449593898901'] . ' имеет видео эфир, загружаем его<br><hr>';
            $video = '<iframe src="//ipeye.ru/ipeye_service/api/iframe.php?iframe_player=1&dev='.$task['ufAuto334899357352'].'&autoplay=0&archive=1" width="800" height="600" frameBorder="0" seamless="seamless" allowfullscreen>Ваш браузер не поддерживает фреймы!</iframe>';
            $p->setTVValue('videoobj',$video);
            }
            if ($task['ufAuto294520976557'] != ''){
                $coords = explode(',', $task['ufAuto294520976557']);
                $p->setTVValue('ya_x',trim($coords[1]));
                $p->setTVValue('ya_y',trim($coords[0]));
            }
        }

    } else {
        if ($task['ufAuto294520976557'] == ''){
            $coords = getAddrCoord($task['ufAuto734390799930']);
        } else {
            $c = explode(',', $task['ufAuto294520976557']);
            $coords = [
                    'x' => $c[1],
                    'y' => $c[0]
            ];
        }
        if ($coords['x'] == '' && $coords['y'] == '') {
            $log .= 'Не удалось получить адрес для '.$task['ufAuto449593898901'].'<br><hr>';
            continue;
        }
        $photos = [];
        if (!empty($task['ufTaskWebdavFiles'])) {
            $i = 1;
            foreach ($task['ufTaskWebdavFiles'] as $file) {
                $file_response = executeREST($arAccessParams['client_endpoint'], 'disk.attachedObject.get', array('id' => $file), $arAccessParams['access_token']);
                $file_url = $file_response['result']['DOWNLOAD_URL'];
                $photos[] = download($file_url);
                $i++;
                if ($i > 3) {
                    break;
                }
            }
        }

        $objData = [
            'pagetitle' => str_replace('"','',$task['ufAuto449593898901']),
            'longtitle' => str_replace('"','',$task['ufAuto449593898901']),
            'alias' => 'object-' . $task['id'],
            'tvs' => true,
            'tv161' => str_replace('"','',$task['ufAuto734390799930']),
            'tv199' => $task['id'],
            'tv158' => $task['ufAuto467928686469'],
            'tv159' => $task['ufAuto403751396900'],
            'tv154' => $task['ufAuto152454175340'],
            'tv155' => $task['ufAuto131772407631'],
            'tv156' => $task['ufAuto187144483651'],
            'tv162' => $task['ufAuto334899357352'],
            'tv164' => $coords['x'],
            'tv165' => $coords['y'],
            'tv166' => str_replace('"','',$task['ufAuto449593898901']),
            'tv151' => $photos[0],
            'tv152' => $photos[1],
            'tv150' => $photos[2],
        ];
        $data = array_merge($defaultData, $objData);
        //echo '<pre>'.print_r($data,1).'</pre>';
        $response = createPage($data, $modx);
        if ($response){
            $log .= 'Создана страница ' . $task['ufAuto449593898901'] . '<br><hr>';
        }

    }
}
echo '<pre>' . $log . '</pre>';

$parent = $modx->getObject('modResource',1997);

$parent->setTVValue('updateDate',date("d-m-Y H:i:s"));

function executeHTTPRequest($queryUrl, array $params = array())
{
    $result = array();
    $queryData = http_build_query($params);

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $queryUrl,
        CURLOPT_POSTFIELDS => $queryData,
    ));

//    echo print_r($queryData,1);
//    echo '<hr>';

    $curlResult = curl_exec($curl);
    curl_close($curl);

    if ($curlResult != '') $result = json_decode($curlResult, true);

    return $result;
}

function requestCode($domain)
{
    $url = 'https://site.bitrix24.ru/oauth/authorize/?client_id=' . urlencode(APP_ID);
    redirect($url);
}

function requestAccessToken($code, $server_domain)
{
    $url = 'https://site.bitrix24.ru/oauth/token/?' .
        'grant_type=authorization_code' .
        '&client_id=' . urlencode(APP_ID) .
        '&client_secret=' . urlencode(APP_SECRET_CODE) .
        '&code=' . urlencode($code);
    return executeHTTPRequest($url);
}

function requestDeals($access_token, $server_domain)
{
    $url = 'https://site.bitrix24.ru/rest/task.planner.getlist?start=0&auth=' . $access_token;
    return executeHTTPRequest($url);
}

function executeREST($rest_url, $method, $params, $access_token)
{
    $url = $rest_url . $method . '.json';
    //echo 'URL: '.$url.'<br>';
    return executeHTTPRequest($url, array_merge($params, array("auth" => $access_token)));
}
 
function createPage($data, &$modx)
{
    //$response = $this->modx->runProcessor('resource/create', $productData);
    //$response = $modx->runProcessor('resource/create', $data);
    $response = $modx->runProcessor('create', $data, array('processors_path' => MODX_CORE_PATH . '/components/site/processors/site/'));
    //TODO сделать что-то вроде вывода ошибок!
    if ($response->isError()) {
        echo 'Ошибка: ' . print_r($modx->error->failure($response->getMessage()),1) . '<br>';
        echo '<hr><pre>'.print_r($data,1).'</pre><hr>';
        return false;
    }
    return true;
}

function getAddrCoord($adress)
{
    if ($adress == '') {
        return [];
    }

    $key = "085c1fc1-a63a-48a3-b0a5-90c1e7f5ae6d"; //API ключ для работы с Яндекс картами
    $adress1 = urlencode($adress);
    $url = "http://geocode-maps.yandex.ru/1.x/?geocode=" . $adress1 . "&apikey=" . $key;
    $content = file_get_contents($url); // получаем страницу с координатами
    preg_match('/<pos>(.*?)<\/pos>/', $content, $point); // вырезаем нужные нам координаты
    $coordinaty = explode(' ', trim(strip_tags($point[1])));

    return [
        'x' => $coordinaty[0],
        'y' => $coordinaty[1]
    ];
}

function download($file)
{
    foreach (get_headers($file) as $item) { // получаем заголовок скачиваемого файла
        if (preg_match('~Content-Disposition\:\h*attachment\;\h*filename=\"\K[^\"]+~i', $item, $match)) { // пытаемся найти название файла
            $fileName = urldecode($match[0]);
            break;
        }
    }
    if ($fileName != '') {  // если название файла нашли, копируем файл
        $local = 'files/bitrix/' . $fileName;
        file_put_contents($local, file_get_contents($file));
    }

    return $local;
}


?>
<p><a href="http://sitef.ru/groups.php">Обновить</a> &raquo;</p>
</body>
</html>